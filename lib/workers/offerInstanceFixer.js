// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var config = require('@oakus/hyphen-libs/config').load('./workers/config.json');
var express = require('express');
var http = require('http');
var app = express();
var _ = require('underscore');
var async = require('async');
require('coffee-script/register');  // easy-pg module is in coffescript, so need this
var postgres = require('easy-pg');
var moment = require('moment');
var sql = require('../app_server/models/dataAccess/sql');
var repeatUtils = require( '../app_server/models/repeatUtils');
var genId = require('@oakus/hyphen-libs/genId');

// Express configuration
app.set('port', config.port);

var output = ['started'];
app.get('/status', function( req, res) {
        res.send( output.join( '\n'));
});

function db() {
    return postgres( config.postgresql.connectOptions);
}

newOfferInstances = function( offerId, doc, cb) {
    var offerInstanceRows = [];
    _(doc.sites).each( function( site, siteName) {
        _(repeatUtils.timestampSeries(doc.displayFrom, doc.repeat)).each( function( instanceDisplayFrom) {
            offerInstanceRows.push( [
                offerId,
                siteName,
                instanceDisplayFrom,
                doc.displayUntil? moment.utc( instanceDisplayFrom).add( moment(doc.displayUntil).diff(moment( doc.displayFrom))).toISOString() : null,
                site.location
            ]);
        });
    });

    var transaction = db();
    async.auto({
        'begin': function (cb) {
            transaction.begin( cb);
        },
        'instances': ['begin', function( cb) {
            if( offerInstanceRows.length == 0) return cb();
            async.parallel( _(offerInstanceRows).map( function (row) {
                return function( cb) { transaction.query( sql.newofferInstance, row, cb);}
            }), cb)
        }],
        commit: ['instances', function (cb) {
            transaction.commit( cb);
        }]
    }, function( err, results) {
        if( err && transaction.inTransaction) transaction.rollback();
        cb(err, results.offer);
    });
};

var count = 0;

async.doWhilst(
    function (callback) {
        db().query( 'SELECT offer.* FROM offer LEFT JOIN offerinstance ON id = offerId WHERE offerid IS NULL LIMIT 10000',[], function( err, results) {
            if( err) {
                count = 0;
                return output.push( err.message);
            }
            count = results.rows.length;
            output.push( count);
            async.eachLimit( results.rows, 10, function( row, cb) {
                newOfferInstances( row.id, row.doc, cb);
            }, function( err) {
                if( err) output.push( err.message);
                setTimeout(callback, 500);
            });
        });
    },
    function () { return count; },
    function (err) {
        if( err) output.push( err.message);
        output.push( 'all done.');
    }
);

// Start the server
http.createServer(app).listen(app.get('port'), function() {
    console.log('agent-hyphen-node server listening on port', app.get('port'));
});

