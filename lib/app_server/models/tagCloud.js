// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var _ = require( 'underscore');
var assert = require( 'assert');

var categories = {
    "en-US": {
        "dining": null,
        "shopping": null,
        "service": null
    }
};

var tagsPerCategory = {
    "dining": {
        "en-US": {
            "indian": null,
            "italian": null,
            "japanese": null,
            "american": null
        }
    },
    "shopping": {
        "en-US": {
            "groceries": null,
            "clothes": null,
            "flowers": null
        }
    },
    "service": {
        "en-US": {
            "car": null,
            "cleaning": null
        }
    }
};

// TODO something with the weights
exports.search = function( userId, inputCloud, opts, invokerId, cb) {
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    var result = _(inputCloud).defaults( {categories:{}, tags: {}});

    if( opts.fetch == 'categories') {
        _(result.categories).defaults( categories);
        _(categories).each( function( stringObjs, lang) {
            _(result.categories[lang]).defaults( categories[ lang]);
        })
    }
    else if( opts.fetch == 'tags') {
        _(inputCloud.categories).each( function(catObjs, catLang) {
            _(catObjs).each( function(nulls, catString) {
                _(result.tags).defaults( tagsPerCategory[catString]);
                _(tagsPerCategory[catString]).each( function( stringObjs, tagLang) {
                    _(result.tags[tagLang]).defaults( tagsPerCategory[ catString][catLang]);
                });
            });
        });
    }
    else
        result = inputCloud;

    setImmediate( function() {
        return cb( null, {doc: result});
    });
};