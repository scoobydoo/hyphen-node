// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var _ = require('underscore');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var rsvp = require( './responder').responder;
var offerModel = require( './offer');

// {offer, numberClaimed, totalClaimed, numberRedeemed, totalRedeemed, lastClaimedDate, lastRedeemedDate}

/*
 "offer": { "$ref": "offer#existing"},
 "codes": { "type": "array", "items": {"type": "string"}},
 "numberOfClaims": { "type": "integer", "minimum": 0},
 "totalClaims":  { "type": "integer", "minimum": 0},
 "numberOfRedemptions": { "type": "integer", "minimum": 0},
 "totalRedeemed":  { "type": "integer", "minimum": 0}
 */

exports.getVoucherInfo = function( userId, query, invokerId, cb) {
    cb = rsvp( cb);
    if( userId !== invokerId)
        return cb( errs.notAthorized);

    data.get_Offers_ClaimsSummary_RedemptionsSummary (userId, query, 200, function (err, result) {
        if( err) return cb(err);
        var array = _(result.docs).sortBy( function( doc) { return doc._.lastClaimedDate;});
        var resultObj = [];
        _(array).forEach( function (doc) {
            resultObj.unshift({ // Note the order is reversed here
                offer: doc,
                numberOfClaims: doc._.numberClaimed,
                totalClaimed: doc._.totalClaimed,
                numberOfRedemptions: doc._.numberRedeemed,
                totalRedeemed: doc._.totalRedeemed
            });
            delete doc._.numberClaimed;
            delete doc._.totalClaimed;
            delete doc._.numberRedeemed;
            delete doc._.totalRedeemed;
            delete doc._.lastClaimedDate;
            offerModel.addOfferMetadata( doc);
        });
        cb( err, {docs:resultObj});
    })
};
