// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var rsvp = require( './responder').responder;

exports.newCheckin = function( userId, doc, invokerId, cb) {
    cb = rsvp( cb);
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    data.newCheckin( userId, doc, cb);
};