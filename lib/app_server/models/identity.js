// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var _ = require( 'underscore');
var moment = require('moment');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var async = require( 'async');
var responder = require( './responder');
var rsvp = responder.responder;

function _ifAuthorised( partyId, invokerId, cbNot, cbOk) {
    if( partyId == invokerId)
        setImmediate( cbOk);
    else
        setImmediate( function() {cbNot( errs.notAthorized)});
    /*  // TODO Merchant Identities aren't implemented yet (ever?)
    else {
        data.getMerchant_Role( invokerId, partyId, function( err, role) {
            if (err)
                return cbNot( err);

            if(role !== 'admin')
                return cbNot( errs.notAthorized);
            cbOk();
        })
    }
    */
}

exports.newIdentity = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    _ifAuthorised( doc.partyId, invokerId, cb, function () {
        doc = _(doc).clone();
        doc.identifier = doc.identifier.toLowerCase();
        if( doc.scope == 'mobile')
            doc.identifier = doc.identifier.replace( /[\.\-]/g, ' ');
        data.newIdentity( doc, cb);
    });
};

exports.updateIdentity = function( doc, currentVersion, invokerId, cb) {
    cb = rsvp( cb);
    _ifAuthorised( doc.partyId, invokerId, cb, function () {
        doc = _(doc).clone();
        doc.identifier = doc.identifier.toLowerCase();
        data.updateIdentity( doc, responder.eTagToVersion( currentVersion).db, cb);
    });
};

exports.validateIdentity = function( doc, currentVersion, invokerId, cb) {
    cb = rsvp( cb);
    doc = _(doc).clone();
    doc.validated = doc.$.modified;
    data.updateIdentity( doc, responder.eTagToVersion( currentVersion).db, cb);
};

exports.getIdentity = function( partyId, scope, identifier, invokerId, cb) {
    cb = rsvp( cb);
    async.parallel ({
            authorise: function( cb) {_ifAuthorised( partyId, invokerId, cb, cb);},
            identity: function( cb) { data.get_Identity( partyId, scope, identifier, cb);}
        },
        function( err, results) {
            if( err) return cb( errs.notAthorized);
            cb( null, results.identity);
        });
};

exports.getIdentities = function( partyId, invokerId, cb) {
    cb = rsvp( cb);
    async.parallel ({
            authorise: function( cb) {_ifAuthorised( partyId, invokerId, cb, cb);},
            identities: function( cb) { data.get_Identities( partyId, cb);}
    },
    function( err, results) {
        if( err) return cb(  errs.notAthorized);
        cb( err, results.identities);
    });
};

exports.deleteIdentity = function( partyId, scope, identifier, invokerId, cb) {
    cb = rsvp( cb);
    _ifAuthorised( partyId, invokerId, cb, function () {
        data.deleteIdentity( partyId, scope, identifier, cb);
    })
};
