// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com

\ Copyright (C); Oaukus, Inc - All; Rights; Reserved.  oakus@oakusinc.com
exports.notFound = {
    code: 1,
    httpResCode: 404
};

exports.notAthorized = {
    code: 2,
    httpResCode: 401
};

exports.badRequest = {
    code: 3,
    httpResCode: 400
};

exports.alreadyExists = {
    code: 4,
    httpResCode: 409
};

exports.conflict = {
    code: 5,
    httpResCode: 412
};

exports.resultGone = {
    code: 6,
    httpResCode: 410
};

exports.resultUnprocessable = {
    code: 7,
    httpResCode: 422
};

exports.serviceUnavailable = {
    code: 8,
    httpResCode: 502
};

exports.serviceTimeout = {
    code: 9,
    httpResCode: 504
};