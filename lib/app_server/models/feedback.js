// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var log = require('@oakus/hyphen-libs/glogger');
var data = require( './dataAccess/pgAccess');
var cache = require( './dataAccess/redisAccess');
var errs = require( './errors');
var docUtils = require( '@oakus/hyphen-libs/JSONDocUtils');
var rsvp = require( './responder').responder;
var genId = require( '@oakus/hyphen-libs/genId');
var moment = require( 'moment');
var async = require( 'async');
var _ = require( 'underscore');
var roles = require( './roles');
var counts = require( './userCounts');

exports.newFeedback = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    if( doc.userId !== invokerId)
        return cb( errs.notAthorized);

    roles.getOffer_Roles( doc.offerId, function( e, r) {
        if( e) return cb( e);
        var adminAndPosters = _(r.docs).pluck( 'userId');
        var isUserOffer = !r.docs[ 0].merchantId;

        if( (doc.like || doc.rating) && _(adminAndPosters).contains( invokerId))
            return cb( errs.notAthorized); // Can't like or rate own offers

        var feedbackId = genId.newId();
        doc.comment = docUtils.normaliseWhitespace( doc.comment);
        if( !doc.createdDelta) doc.createdDelta = 0;
        doc.createdAt = moment.utc().add( doc.createdDelta, 'ms').toISOString();
        delete doc.createdDelta;

        data.newFeedback( feedbackId, doc, cb);

        // NOTE cb already called!! Fire and forget from here
        counts.incUserRepScore( invokerId, counts.UserRepAction.LIKE_OFFER);
        if( doc.comment)
            async.each( adminAndPosters, _(cache.unreadComments.increment).partial( _, doc.offerId).bind( cache.unreadComments));
        if( doc.like) {
            async.each( adminAndPosters, _(cache.unreadLikes.increment).partial( _, doc.offerId).bind( cache.unreadLikes));
            counts.incUserRepScore( adminAndPosters[0], counts.UserRepAction.RECEIVE_LIKE); // TODO credit this to the Merchant when we have merchant rep
        }
    })
};