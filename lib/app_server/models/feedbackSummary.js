// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var _ = require('underscore');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var rsvp = require( './responder').responder;
var cache = require( './dataAccess/redisAccess');
var counts = require( './userCounts');

exports.getFeedbackSummary = function( offerId, invokerId, cb) {
    cb = rsvp( cb);
    data.get_Feedback_Names (offerId, 200, function (err, result) {
        if( err) return cb( err);
        var array = result.docs;
        // TODO filter ratings to be relevant to the user (language etc.)
        var comments = _(array).map( function( item) {
            return _(_(item).extend({
                                        user:item._.name,
                                        id: item.$.id
                                    }))
                                    .omit( 'userId', '_', '$')
        });

        var totalLikes = _(comments).reduce( function( memo, val) { return memo + (val.like?val.like:0); }, 0);
        // Filter out none-comments
        comments = _(comments).filter( function( comment) {
            return !!comment.comment;
        });

        // Sort to pre-order tree traversal, or lexical order of paths from root to node (the timestampSeries)
        var commentHash = _.object( _(comments).pluck( 'id'), comments);
        function timestampSeries( comment) {
            return (comment.parentId ? timestampSeries( commentHash[ comment.parentId]) : "") + comment.createdAt
        }
        comments.sort( function( a, b) {
            var pathA = timestampSeries( a);
            var pathB = timestampSeries( b);
            function compare( i) {
                if ( i == pathA.length)
                    return i == pathB.length ? 0 : -1;
                else if ( i == pathB.length)
                    return 1;
                else if ( pathA[i] < pathB[i])
                    return -1;
                else if ( pathA[i] > pathB[i])
                    return 1;
                else return compare( i+1);
            }
            return compare( 0);
        });

        var feedback = {
            totalLikes: totalLikes,
            totalComments: _(comments).reduce( function( memo, val) { return memo + (val.comment?1:0); }, 0),
            usefulComments: comments
        };
        cb( err, {doc:feedback});

        cache.unreadComments.clear( invokerId, offerId); // cb already called.  Fire and forget
        cache.unreadLikes.clear( invokerId, offerId); // cb already called.  Fire and forget
        counts.incUserRepScore( invokerId, counts.UserRepAction.VIEW_OFFER_DETAILS);
    })
};
