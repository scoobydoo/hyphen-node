// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var moment = require( 'moment');
var _ = require( 'underscore');
var rsvp = require( './responder').responder;
var counts = require( './userCounts');

exports.newClaim = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    if( doc.userId !== invokerId)
        return cb( errs.notAthorized);
    // TODO check maxPerCustomer is not exceeded
    data.get_Offer( doc.offerId, function (err, offerResult) {
        if( err) return cb(err);
        doc = _(doc).clone();
        doc.created = moment.utc().toISOString(); // TODO move to db
        data.newClaim( doc, offerResult.doc.maxVouchers, cb);
        counts.incUserRepScore( invokerId, counts.UserRepAction.CLAIM_OFFER); // Fire and forget
    })
};

exports.newRedemption = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    if( doc.userId !== invokerId)
        return cb( errs.notAthorized);
    // TODO Check redeption code.
    // TODO Check user has some claimed vouchers to redeem
    data.newRedemption( doc, cb);
};