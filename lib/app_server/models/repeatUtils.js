// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require( 'assert');
var moment = require( 'moment');
var _ = require( 'underscore');

// TODO this should be an iterator?
exports.timestampSeries = function( firstTimestamp, repeatObj) {
    if( !repeatObj)
        return [firstTimestamp];

    assert( repeatObj.period == 'day'); // Only support this for now

    var until = repeatObj.until ? moment(repeatObj.until) : moment( firstTimestamp).add(1, 'year');
    if( until._i == 'Invalid date') // Cope with some invalid data in db
        until = moment( firstTimestamp).add(1, 'day');
    var occurrences = repeatObj.occurrences ? repeatObj.occurrences + 1 : 10000;

    var every = repeatObj.every ? repeatObj.every : 1;
    var resultSeries = [];

    var time = moment(firstTimestamp).utc(), i = 0;
    do {
        resultSeries.push( time.toISOString());
        time.add(every, 'days'); i++;
    } while( time <= until && i < occurrences);

    return resultSeries;
};