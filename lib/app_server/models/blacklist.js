// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var moment = require('moment');
var _ = require( 'underscore');
var rsvp = require( './responder').responder;

exports.newBlock = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    doc = _(doc).clone();
    doc.created = moment.utc().toISOString();
    if( doc.userId !== invokerId)
        return cb( errs.notAthorized);
    data.newBlock( doc, cb);
};

exports.deleteBlock = function( userId, partyId, invokerId, cb) {
    cb = rsvp( cb);
    var oldBlock = {
        userId: userId,
        partyId: partyId
    };
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    data.deleteBlock( oldBlock, cb);
};