// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var rsvp = require( './responder').responder;

exports.newPosting = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    data.newPosting( doc, cb);
    // TODO need to distinguish errors, not found vs. bad content.  May need to do another read to tell them apart.
};