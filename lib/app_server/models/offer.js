// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var async = require('async');
var assert = require('assert');
var moment = require('moment');
var data = require( './dataAccess/pgAccess');
var cache = require( './dataAccess/redisAccess');
var errs = require( './errors');
var _ = require( 'underscore');
var docUtils = require( '@oakus/hyphen-libs/JSONDocUtils');
var responder = require( './responder');
var genId = require( '@oakus/hyphen-libs/genId');
var login = require( './dataAccess/dynamoAccess');
var config = require( '@oakus/hyphen-libs/config').config();
var counts = require( './userCounts');


exports.addOfferMetadata = function( offer, unreadCounts) {
    if( _(offer).isArray())
        return _(offer).each(_( exports.addOfferMetadata).partial( _, unreadCounts));

    if( !offer.tags)         // TODO remove when no old data in DB
        offer.tags = {};
    if( !offer.categories)   // TODO remove when no old data in DB
        offer.categories = {};

    if( offer._ && offer._.instanceDisplayFrom) { // Instance metadata
        var instanceDisplayFrom = new Date( offer._.instanceDisplayFrom);
        var displayFrom = new Date( offer.displayFrom);
        var redeemFrom = new Date( offer.redeemFrom);
        var instanceRedeemFrom = new Date(instanceDisplayFrom.getTime() + (redeemFrom - displayFrom));
        offer._.instanceRedeemFrom = instanceRedeemFrom.toISOString();
        if( offer.redeemUntil) {
            var redeemUntil = new Date( offer.redeemUntil);
            offer._.instanceRedeemUntil = (new Date(instanceRedeemFrom.getTime() + (redeemUntil - redeemFrom))).toISOString();
        }
    }

    if( unreadCounts) {
        if( !offer._)
            offer._ = {};
        offer._.unreadComments = unreadCounts.comments[offer.$.id];
        if( !offer._.unreadComments) offer._.unreadComments = 0;
        offer._.unreadLikes = unreadCounts.likes[offer.$.id];
        if( !offer._.unreadLikes) offer._.unreadLikes = 0;
    }
};

function rsvp( cb, unreadCounts) {
    return responder.responder( function ( err, dbResults) {
            if(err) return cb( err);
            if( dbResults && dbResults.docs) exports.addOfferMetadata( dbResults.docs, unreadCounts);
            cb( err, dbResults);
        }
    )
}

exports.newOffer = function( doc, invokerId, cb) {
    doc = docUtils.normaliseWhitespace( doc);
    var offerId = genId.newId();
    cb = rsvp( cb, {offerId: {unreadComments:0, unreadLikes: 0}});

    var now = moment.utc();
    var firstAllowedStart = moment(now).subtract( config.model.offer.allowInPast_s, 'seconds');

    var offerStart = doc.displayFrom == null ? now : moment.utc( doc.displayFrom);
    var offerEnd = moment.utc( doc.displayUntil);

    if( offerStart < firstAllowedStart)
        return cb( errs.resultUnprocessable);
    else if ( offerStart < now)
        offerStart = moment(now);
    if( offerEnd < offerStart)
        offerEnd = moment(offerStart).add( offerEnd.diff( moment.utc(doc.displayFrom)));

    doc.displayFrom = offerStart.toISOString();
    doc.displayUntil = offerEnd.toISOString();

    if( doc.redeemFrom !== undefined) {
        var redeemStart = doc.redeemFrom === null ? now : moment.utc( doc.redeemFrom);
        if( redeemStart < offerStart)
            redeemStart = moment( offerStart);
        if( doc.redeemUntil) { // Maybe undefined or null
            var redeemEnd = moment.utc( doc.redeemUntil);
            if( redeemEnd < offerEnd)
                doc.redeemUntil = offerEnd.toISOString();
        }
        doc.redeemFrom = redeemStart.toISOString();
    }

    assert( doc.displayFrom <= doc.displayUntil); // TODO delete me

    if( !doc.tags)  // We don't require the client to set these yet, but maybe in the future
        doc.tags = {};
    if( !doc.categories)
        doc.categories = {};

    delete doc._;               // Read only
    delete doc.$;               // Read only

    if( doc.partyType === 'merchant') {
        data.getMerchant_Role( invokerId, doc.partyId, function( err, role) {
            if (err)
                return cb( err);

            if(role !== 'admin' && role !== 'poster')
                return cb( errs.notAthorized);

            data.newOffer( offerId, doc, cb);
        });
    }
    else {
        data.newOffer( offerId, doc, cb);
    }
    counts.incUserRepScore( invokerId, counts.UserRepAction.POST_OFFER); // Fire and forget
    // TODO Maybe credit to the merchant instead of invoker when we have merchant reputation
};

exports.updateOffer = function( offerId, doc, currentVersion, invokerId, cb) {
    cb = rsvp( cb, {});     // TODO add unreadCounts?
    data.getOffer_Role( invokerId, offerId, function( err, role) {
        if (err)
            return cb( err);

        if(role !== 'admin')
            return cb( errs.notAthorized);

        if( !doc.tags)  // We don't require the client to set these yet, but maybe in the future
            doc.tags = {};
        if( !doc.categories)
            doc.categories = {};

        delete doc._;               // Read only
        delete doc.$;               // Read only

        data.updateOffer( offerId, doc, responder.eTagToVersion( currentVersion).db, cb);
    });
};

exports.getNearByOffers = function( query, invokerId, cb) {
    cb = rsvp( cb);     // TODO add unreadCounts?
    var user = query.user;
    var longitude = parseFloat( query.longitude);
    var latitude = parseFloat( query.latitude);
    var now = moment.utc().toISOString();

    if( !user)
        return cb( errs.badRequest);
    if( user !== invokerId)
        return cb( errs.notAthorized);
    if( isNaN( longitude) || longitude < -180 || longitude > 180)
        return cb( errs.badRequest);
    if( isNaN(latitude) || latitude < -90 || latitude > 90)
        return cb( errs.badRequest);

    data.getNearby_Offers( longitude, latitude, user, now, query.query, query.maxResults, query.iterator, cb);
};

exports.getSuggestedOffersAndUsers = function( startFrom, startUntil, cb) {
    data.getStarting_Offers( startFrom, startUntil, function( err, results) {
        if(err) return cb( err);
        exports.addOfferMetadata( results.docs);
        async.map( results.docs, function( offer, cb) {
            data.getPartyClaimees_UserIds( offer.partyId, function( err, userIds) {
                if(err) return cb( err);
                cb( null, { offer: offer, userIds: _(userIds.rows).pluck( 'userId')});
            });
        }, cb)
    });
};

exports.getPartyOffers = function( partyId, query, invokerId, cb) {
    async.auto({
        "comments": function( cb) { cache.unreadComments.counts( invokerId, cb);},
        "likes": function( cb) { cache.unreadLikes.counts( invokerId, cb);},
        "offers": function( cb) { data.get_Offers( partyId, 200, cb);}
    }, function( e, r) {
        cb = rsvp( cb, {comments: r.comments, likes: r.likes});
        cb( e, r.offers);
    });
};

exports.getOffer = function( offerId, invokerId, cb) {
    data.get_Offer( offerId, rsvp( cb));
};

exports.getUserBlockedOffers = function( userId, query, invokerId, cb) {
    cb = rsvp( cb);     // TODO add unreadCounts?
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    data.getBlocked_Offers( userId, 200, cb); // TODO
};

