// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var moment = require('moment');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var _ = require( 'underscore');
var async = require( 'async');
var docUtils = require( '@oakus/hyphen-libs/JSONDocUtils');
var genId = require( '@oakus/hyphen-libs/genId');
var responder = require( './responder');
var rsvp = responder.responder;

exports.newUser = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    if( invokerId)
        return cb( errs.notAthorized);
    doc = docUtils.normaliseWhitespace( doc);
    var userId = genId.newId();

    delete doc._;               // Read only
    delete doc.$;               // Read only

    data.getNamed_PartyId( doc.name, function( err) {
        if( !err)
            return cb( errs.alreadyExists);
        else if( err.code === errs.notFound.code)
            return data.newUser( userId, doc, function (err, result) {
                if (!err)
                    result.doc._ = {
                        validatedMobileCount: 0,
                        mobileCount: 0,
                        roles: {admin:{},poster:{}}
                    };
                cb( err, result);
            });
        else
            return cb( err);
    });
};

var _gupdate = function( userId, dataFunc, cb) {
    cb = rsvp( cb);
    async.parallel({
        user: function(cb) { dataFunc( cb);},
        identities: function(cb) { data.get_Identities( userId, cb);},
        roles: function(cb) { data.get_MerchantIds_Names_Roles( userId, cb);}
        },
        function( err, results) {
            if( err) return cb( err);
            var userDoc = results.user.doc;
            if( userDoc.deleted) return cb( errs.notFound);

            var ids = _(results.identities.docs).filter( function(id) { return id.scope == 'mobile';});
            userDoc._ = {
                validatedMobileCount: _(ids).filter( function(id) { return !!id.validated;}).length,
                mobileCount: ids.length,
                roles: {admin:{},poster:{}}
            };
            _(results.roles.rows).forEach( function( item) {
                userDoc._.roles[ item.role][ item.merchantid] = item.merchantname;
            });

            cb( err, results.user);
        });
};

exports.updateUser = function( userId, doc, currentVersion, invokerId, cb) {
    cb = rsvp( cb);
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    doc = docUtils.normaliseWhitespace( doc);
    delete doc._;               // Read only
    delete doc.$;               // Read only
    _gupdate( userId, _(data.updateUser).partial( userId, doc, responder.eTagToVersion( currentVersion).db), cb);
};

exports.getUser = function( docId, invokerId, cb) {
    cb = rsvp( cb);
    if( docId !== invokerId)
        return cb( errs.notAthorized);
    _gupdate( docId, _(data.get_User).partial( docId), cb);
};

exports.deleteUser = function( userId, invokerId, cb) {
    cb = rsvp( cb);
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    exports.getUser( userId, invokerId, function (err, result) {
        if (err) return cb( err);
        result.doc.deleted = moment.utc().toISOString();
        result.doc.vicinities = {};
        delete result.doc._;
        delete result.doc.$;
        // TODO delete all the identity docs?
        data.updateUser( userId, result.doc, null, function (err, result) {
            cb( err);
        });
    })
};
