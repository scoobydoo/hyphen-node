// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var moment = require('moment');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var _ = require( 'underscore');
var docUtils = require( '@oakus/hyphen-libs/JSONDocUtils');
var responder = require( './responder');
var rsvp = responder.responder;
var genId = require( '@oakus/hyphen-libs/genId');

exports.newDesire = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    if( doc.userId !== invokerId)
        return cb( errs.notAthorized);
    doc = _(doc).clone();

//    doc = docUtils.normaliseWhitespace( doc); TODO Needed?

    if( doc.from == null)    // TODO more jigging of other timestamps, like offer?
        doc.from = moment.utc();

    var desireId = genId.newId();
    data.newDesire( desireId, doc, cb);
};

exports.updateDesire = function( desireId, doc, currentVersion, invokerId, cb) {
    cb = rsvp( cb);
    if( doc.userId !== invokerId)
        return cb( errs.notAthorized);

//    doc = docUtils.normaliseWhitespace( doc); TODO Needed?
    data.updateDesire( desireId, doc, responder.eTagToVersion( currentVersion).db, cb);
};

exports.readAll = function( userId, query, invokerId, cb) {
    cb = rsvp( cb);
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    data.getUser_Desires( userId, query.maxResults, query.iterator, cb);
};
