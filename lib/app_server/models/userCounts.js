// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var _ = require('underscore');
var async = require('async');
var data = require( './dataAccess/pgAccess');
var cache = require( './dataAccess/redisAccess');
var errs = require( './errors');
var rsvp = require( './responder').responder;

exports.UserRepAction = {
    VIEW_OFFER_DETAILS: 10,
    LIKE_OFFER: 20,
    RECEIVE_LIKE: 20,
    COMMENT_ON_OFFER: 30,
    CLAIM_OFFER: 50,
    SHARE_OFFER: 120,
    POST_OFFER: 200
};

var repLevels = [
    { score:0},
    { score:200},
    { score:800}
];

exports.incUserRepScore = function( userId, userRepActionCredit) {
    assert( userRepActionCredit > 0);
    data.incUserRepScore( userId, userRepActionCredit); // No cb, Fire-and-forget
};

exports.getUserCounts = function( userId, invokerId, cb) {
    cb = rsvp( cb);
    if( userId !== invokerId)
        return cb( errs.notAthorized);

    async.auto({
        comments: function( cb) { cache.unreadComments.counts( userId, cb);},
        likes: function( cb) { cache.unreadLikes.counts( userId, cb);},
        score: function( cb) { data.getUser_repScore( userId, cb);}
    }, function( e, r) {
        if( e) return cb( e);

        var result = {
            unreadComments: _(r.comments).reduce( function( a, b) { return a+b;}),
            unreadLikes: _(r.likes).reduce( function( a, b) { return a+b;}),
            repScore: r.score.repScore,
            repLevels: repLevels
        };
        cb( null, { doc: result});
    });
};
