// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var async = require('async');
var _ = require('underscore');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var rsvp = require( './responder').responder;
var repeatUtils = require( './repeatUtils');

/*
 "maxVouchers": {"type": "integer", "minimum": 0},
 "numberOfClaims": { "type": "integer", "minimum": 0},
 "totalClaims":  { "type": "integer", "minimum": 0},
 "numberOfRedemptions": { "type": "integer", "minimum": 0},
 "totalRedeemed":  { "type": "integer", "minimum": 0},
 "checkedinUnused":  {"type": "integer", "minimum": 0},
 "checkedinOthers":  {"type": "integer", "minimum": 0}
 */

//{offer, numberClaimed, totalClaimed, numberRedeemed, totalRedeemed, lastClaimedDate, lastRedeemedDate}


exports.getClaimsSummary = function( offerId, invokerId, cb) {
    cb = rsvp( cb);
    async.parallel({
        // TODO check invokerIdis of the offer or for the merchant
        authorisation: function( cb) { data.getOffer_Role( invokerId, offerId, cb);},
        offer: function( cb) { data.get_Offer( offerId, cb);},
        summary: function(cb) { data.get_ClaimsSummary_RedemptionsSummary (offerId, cb);}
    }, function ( err, results) {
        if(results.authorisation !== 'admin')
            return cb( errs.notAthorized);
        if( err) return cb(err);

        // Defaults
        var summary = {};
        _(_(results.offer.doc.sites).keys()).each( function( siteName) {
            summary[siteName] = {};
            _(repeatUtils.timestampSeries( results.offer.doc.displayFrom, results.offer.doc.repeat)).each( function (startTime) {
                summary[siteName][startTime] = {
                    numberOfClaims: 0,
                    totalClaimed: 0,
                    numberOfRedemptions: 0,
                    totalRedeemed: 0,
                    checkedinUnused: 0,
                    checkedinOthers: 0
                }
            });
        });

        // overrides from db
        _(results.summary.rows).each( function( obj) {
            summary[obj.instanceSite][obj.instanceDisplayFrom] = {
                numberOfClaims: obj.numberClaimed,
                totalClaimed: obj.totalClaimed,
                numberOfRedemptions: obj.numberRedeemed,
                totalRedeemed: obj.totalRedeemed,
                checkedinUnused: 0, //TODO
                checkedinOthers: 0  //TODO
            }
        });
        cb( err, {doc:summary});
    })
};
