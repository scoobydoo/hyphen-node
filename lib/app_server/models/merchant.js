// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var moment = require('moment');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var docUtils = require( '@oakus/hyphen-libs/JSONDocUtils');
var responder = require( './responder');
var rsvp = responder.responder;
var genId = require( '@oakus/hyphen-libs/genId');

exports.newMerchant = function( doc, invokerId, cb) {
    cb = rsvp( cb);
    doc = docUtils.normaliseWhitespace( doc);
    var merchantId = genId.newId();
    delete doc._;               // Read only
    delete doc.$;               // Read only

    data.getNamed_PartyId( doc.name, function( err) {
        if( !err)
            return cb( errs.alreadyExists);
        else if( err.code === errs.notFound.code) {
            var assocDoc = {merchantId: merchantId, userId: invokerId, role: "admin"};
            return data.newMerchantAndAssociate( merchantId, doc, assocDoc, function( err, newMerchantResult) {
                if (err) return cb( err);
                cb( err, newMerchantResult);
            });
        }
        else
            return cb( err);
    });
};

exports.updateMerchant = function( docId, doc, currentVersion, invokerId, cb) {
    cb = rsvp( cb);
    data.getMerchant_Role( invokerId, docId, function( err, role) {
        if (err)
            return cb( err);

        if(role !== 'admin')
            return cb( errs.notAthorized);

        doc = docUtils.normaliseWhitespace( doc);
        delete doc._;               // Read only
        delete doc.$;               // Read only

        data.updateMerchant( docId, doc, responder.eTagToVersion( currentVersion).db, cb);
    });
};

exports.getMerchant = function( docId, invokerId, cb) {
    data.get_Merchant (docId, rsvp( cb));
};
