// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var _ = require('underscore');
var crypto = require( 'crypto');

var hmacAlgorithm = 'md5';
var passwordBuf = new Buffer( require('@oakus/hyphen-libs/config').config().digestPassword);
var digestLength = 22;

exports.eTagToVersion = function ( eTag) {
    return {
        db: eTag.slice( 0, -digestLength),
        digest: eTag.slice( -digestLength)
    };
};

_versionsToETag = function ( db, digest) {
    return db.toString() + digest;
};

function _digestObject( obj) {
    if( obj === null) return JSON.stringify( obj);
    if( obj.$ && obj.$.eTag != undefined) {        // A versioned document, already been processed
        return obj.$.eTag
    }
    else if( obj.$ && obj.$.version != undefined) {   // A versioned document
        obj.$.eTag = _versionsToETag( _digestObject( obj.$.version), _digestObject( {_: obj._ ? obj._ : 'no de-normalised data'}));
        delete obj.$.version;
        return obj.$.eTag;
    }
    else if( _(obj).isObject()) {                   // An array, un-versioned document, some sub-object in a document
        var string = _(obj).reduce( function( memo, value, key) { return memo + _digestObject( key) + _digestObject( value)}
                                   , (_(obj).isArray()?'A':'O'));
        return crypto.createHmac(hmacAlgorithm, passwordBuf)
                .update( string)
                .digest('base64')
                .replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, '');
    }
    else                                            // Primitive type
        return JSON.stringify( obj);                // No need to compute a digest as the function is never called on a bare primitive
}

exports.responder = function ( cb) {
    return function (err, dbResults) {
        var eTag;
        if( err || dbResults == undefined) return cb( err);
        else if( dbResults.doc)
            eTag = _digestObject( dbResults.doc);
        else if( dbResults.docs)
            eTag = _digestObject( dbResults.docs);
        dbResults.eTag = eTag;
        cb( err, dbResults)
    }
};