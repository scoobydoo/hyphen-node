// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var moment = require( 'moment');
var _ = require( 'underscore');
var dynamoAccess = require( './dataAccess/dynamoAccess');
var errs = require( './errors');

exports.newLogin = function( userId, doc, invokerId, cb) {
    doc = _(doc).clone();
    if( userId !== invokerId)
        return cb( errs.notAthorized);
    doc.time = moment.utc().toISOString();
    var deviceToken = doc.deviceToken;
    delete doc.deviceToken;
    dynamoAccess.upsert( userId, deviceToken, doc, cb)
};

exports.getRecentLogins = function( userIds, cb) {
    dynamoAccess.getMostRecent_Logins( userIds, cb);
};

