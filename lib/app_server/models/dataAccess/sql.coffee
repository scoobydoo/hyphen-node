# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
exports.newUser = """
  INSERT INTO "user" (id, doc, created, modified)
  VALUES ($1, $2, now(), now())
  RETURNING *
"""

exports.newMerchant = """
  INSERT INTO merchant (id, doc, created, modified)
  VALUES ($1, $2, now(), now())
  RETURNING *
"""

exports.newIdentity = """
  INSERT INTO identity (doc, created, modified)
  VALUES ($1, now(), now())
  RETURNING *
"""

exports.newOffer = """
  INSERT INTO offer (id, doc, created, modified)
  VALUES ($1, $2, now(), now())
  RETURNING *
"""

exports.newofferInstance = """
  INSERT INTO "offerinstance" (offerid, offersite, offerstart, offerend, offerlocation)
  VALUES ($1, $2, $3, $4, json_to_geog($5::json))
  RETURNING *
"""

exports.newDesire = """
  INSERT INTO desire (id, doc, created, modified)
  VALUES ($1, $2, now(), now())
  RETURNING *
"""

exports.newAssociate = """
  INSERT INTO "associate" (doc)
  VALUES ($1)
  RETURNING *
"""

exports.newBlock = """
  INSERT INTO "block" (doc)
  VALUES ($1)
  RETURNING *
"""

exports.newClaim = """
  INSERT INTO "claim" (doc)
  VALUES ($1)
  RETURNING *
"""

exports.newRedemption = """
  INSERT INTO "redemption" (doc)
  VALUES ($1)
  RETURNING *
"""

exports.newPosting = """
  INSERT INTO "posting" (doc)
  VALUES ($1)
  RETURNING *
"""

exports.newResponse = """
  INSERT INTO "response" (doc)
  VALUES ($1)
  RETURNING *
"""

exports.newFeedback = """
  INSERT INTO feedback (id, doc, created, modified)
  VALUES ($1, $2, now(), now())
  RETURNING *
"""

exports.updateUser = """
  UPDATE "user" SET
    doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1
  RETURNING *
"""

exports.updateUserCheckVersion = """
  UPDATE "user" SET
    doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1 AND
    version = $3
  RETURNING *
"""

exports.updateIncRepScore = """
  UPDATE "user" SET
    repscore = repscore + $2
  WHERE
    id = $1
  RETURNING repscore AS "repScore"
"""


exports.updateMerchant = """
  UPDATE merchant SET
    doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1
  RETURNING *
"""

exports.updateMerchantCheckVersion = """
  UPDATE merchant SET
     doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1 AND
    version = $3
  RETURNING *
"""


exports.updateIdentity = """
  UPDATE "identity" SET
    doc = $1,
    version = version + 1,
    modified = now()
  WHERE
    id_to_uuid( doc->>'partyId') = id_to_uuid( $1->>'partyId') AND
    doc->>'scope' = $1->>'scope' AND
    translate( doc->>'identifier', ' ', '') = translate( $1->>'identifier', ' ', '')
  RETURNING *
"""

exports.updateIdentityCheckVersion = """
  UPDATE "identity" SET
    doc = $1,
    version = version + 1,
    modified = now()
  WHERE
    id_to_uuid( doc->>'partyId') = id_to_uuid( $1->>'partyId') AND
    doc->>'scope' = $1->>'scope' AND
    translate( doc->>'identifier', ' ', '') = translate( $1->>'identifier', ' ', '') AND
    version = $2
  RETURNING *
"""

exports.updateOffer = """
  UPDATE offer
  SET
    doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1
  RETURNING *
"""

exports.updateOfferCheckVersion = """
  UPDATE offer
  SET
    doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1 AND
    version = $3
  RETURNING *
"""

exports.updateOfferInstanceClaimed = """
  UPDATE "offerinstance"
  SET
    claimed = claimed + $4
  WHERE
    ((claimed + $4) < $5 OR $5 IS NULL) AND
    offerid = $1 AND
    offersite = $2 AND
    offerstart = text_to_tsz( $3)
  RETURNING *
"""

exports.updateDesire = """
  UPDATE desire
  SET
    doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1
  RETURNING *
"""

exports.updateDesireCheckVersion = """
  UPDATE desire
  SET
    doc = $2,
    version = version + 1,
    modified = now()
  WHERE
    id = $1 AND
    version = $3
  RETURNING *
"""

exports.deleteBlock = """
  DELETE FROM block
  WHERE
    id_to_uuid( doc->>'userId') = $1 AND
    id_to_uuid( doc->>'partyId') = $2
  RETURNING *
"""

exports.deleteIdentity = """
  DELETE FROM identity
  WHERE
    id_to_uuid( doc->>'partyId') = $1 AND
    doc->>'scope' = $2 AND
    translate( doc->>'identifier', ' ', '') = translate( $3, ' ', '')
  RETURNING *
"""

exports.deleteOldOffers = """
  DELETE FROM offerinstance WHERE offerend < now() - interval '2 days'
"""

exports.get_User = """
  SELECT *
  FROM "user"
  WHERE id = $1
"""

exports.getUser_repScore = """
  SELECT repscore AS "repScore"
  FROM "user"
  WHERE id = $1
"""

exports.get_Merchant = """
  SELECT *
  FROM merchant
  WHERE id = $1
"""

exports.getNamed_PartyId = """
(
  SELECT id
  FROM merchant
  WHERE lower( doc->>'name') = lower( $1)
)
UNION ALL (
  SELECT id
  FROM "user"
  WHERE lower( doc->>'name') = lower( $1)
)
"""

exports.get_Offer = """
  SELECT *
  FROM offer
  WHERE id = $1
"""

exports.get_OfferInstance = """
  SELECT * FROM "offerinstance"
  WHERE
    offerid = $1 AND
    offersite = $2 AND
    offerstart = text_to_tsz( $3)
"""

exports.get_Identities = """
  SELECT *
  FROM identity
  WHERE id_to_uuid( doc->>'partyId') = $1
"""

exports.get_Identity = """
  SELECT *
  FROM identity
  WHERE
    id_to_uuid( doc->>'partyId') = $1 AND
    doc->>'scope' = $2 AND
    translate( doc->>'identifier', ' ', '') = translate( $3, ' ', '')
"""

exports.getAuthorised_Users = """
SELECT * FROM "user" JOIN (
  SELECT id_to_uuid( b.doc->>'partyId') id FROM identity a JOIN identity b
  ON
    a.doc->>'scope' = b.doc->>'scope' AND
    translate( a.doc->>'identifier', ' ', '') = translate( b.doc->>'identifier', ' ', '')
  WHERE
    (a.doc->>'validated') IS NOT NULL AND
    (b.doc->>'validated') IS NOT NULL AND
    id_to_uuid( a.doc->>'partyId') = $1
) AS ids
ON "user".id = ids.id
"""

exports.get_Offers = """
  SELECT
    offer.*,
    COALESCE( counts.numComments, 0)::integer AS "totalComments",
    COALESCE( counts.numScores, 0)::integer AS "totalLikes"
  FROM
    offer LEFT JOIN (
      SELECT COUNT( doc->>'comment') as numComments, SUM( (doc->>'like') ::int) as numScores
      FROM feedback
      GROUP BY id_to_uuid( doc->>'offerId')
    ) as counts
    ON id_to_uuid( doc->>'offerId') = offer.id
  WHERE id_to_uuid( doc->>'partyId') = $1
  ORDER BY created DESC
"""

exports.get_Desire = """
  SELECT *
  FROM desire
  WHERE id = $1
"""

exports.getUser_Desires = """
  SELECT *
  FROM desire
  WHERE id_to_uuid( doc->>'userId') = $1
"""

exports.getBlocked_Offers = """
SELECT offer.*
FROM offer INNER JOIN (
  SELECT id_to_uuid( offer.doc->>'partyId') AS partyId, MAX( offer.doc->>'created') AS created
  FROM offer JOIN block
  ON
    id_to_uuid( offer.doc->>'partyId') = id_to_uuid( block.doc->>'partyId')
  WHERE
    id_to_uuid( block.doc->>'userId') = $1
  GROUP BY id_to_uuid( offer.doc->>'partyId')
) AS recentoffers
ON
  id_to_uuid( offer.doc->>'partyId') = recentoffers.partyId AND
  offer.doc->>'created' = recentoffers.created
ORDER BY recentoffers.created DESC
"""

# $1: Longitude
# $2: Latiyude
# $3: display time (usually set to now.)
# $4: user id
# $5: Limit
exports.getNearby_Offers = """
  SELECT
    offer.*,
    "offerinstance".offersite AS "instanceSite",
    "offerinstance".offerstart AS "instanceDisplayFrom",
    "offerinstance".offerend AS "instanceDisplayUntil",
    "offerinstance".claimed::integer AS "totalClaimed",
    COALESCE( counts.numComments, 0)::integer AS "totalComments",
    COALESCE( counts.numScores, 0)::integer AS "totalLikes"
  FROM
    offer JOIN "offerinstance" ON offer.id = offerid
    LEFT JOIN (
      SELECT COUNT( doc->>'comment') as numComments, SUM( (doc->>'like') ::int) as numScores
      FROM "feedback"
      GROUP BY id_to_uuid( doc->>'offerId')
    ) as counts
    ON id_to_uuid( doc->>'offerId') = offer.id
  WHERE
    tstzrange( offerstart, offerend) @> text_to_tsz( $3) AND
    id_to_uuid( offer.doc->>'partyId') NOT IN (
      SELECT id_to_uuid( block.doc->>'partyId') FROM block
      WHERE id_to_uuid( block.doc->>'userId') = $4
    )
  ORDER BY st_transform( offerlocation::geometry, 3395) <-> st_transform( ST_SetSRID( st_point( $1, $2),4326), 3395)
  LIMIT $5
"""

exports.getStarting_Offers = """
  SELECT
    offer.*,
    "offerinstance".offersite AS "instanceSite",
    "offerinstance".offerstart AS "instanceDisplayFrom",
    "offerinstance".offerend AS "instanceDisplayUntil",
    "offerinstance".claimed::integer AS "totalClaimed",
    COALESCE( counts.numComments, 0)::integer AS "totalComments",
    COALESCE( counts.numScores, 0)::integer AS "totalLikes"
  FROM
    offer JOIN "offerinstance" ON offer.id = offerid
    LEFT JOIN (
      SELECT COUNT( doc->>'comment') as numComments, SUM( (doc->>'like') ::int) as numScores
      FROM feedback
      GROUP BY id_to_uuid( doc->>'offerId')
    ) as counts
    ON id_to_uuid( doc->>'offerId') = offer.id
  WHERE
    offerstart >= $1 AND
    offerstart < $2
  ORDER BY offerstart
"""

exports.getPartyClaimees_UserIds = """
  SELECT DISTINCT claim.doc->>'userId' AS "userId"
  FROM claim JOIN offer
  ON offer.id = id_to_uuid( claim.doc->>'offerId')
  WHERE id_to_uuid( offer.doc->>'partyId') = $1
"""

exports.get_Feedback_Names = """
  SELECT
    feedback.*,
    "user".doc->>'name' AS name
  FROM
    feedback JOIN "user"
  ON
    id_to_uuid( feedback.doc->>'offerId') = $1 AND
    id_to_uuid( feedback.doc->>'userId') = "user".id
"""

exports.getMerchant_Role = """
  SELECT doc
  FROM associate
  WHERE
    id_to_uuid( doc->>'userId') = $1 AND
    id_to_uuid( doc->>'merchantId') = $2
"""

exports.getOffer_Role = """
  SELECT associate.doc
  FROM associate JOIN offer
  ON
    id_to_uuid( associate.doc->>'userId') = $1 AND
    id_to_uuid( associate.doc->>'merchantId') = id_to_uuid( offer.doc->>'partyId') AND
    offer.id = $2
"""

exports.getOffer_Roles = """
  SELECT COALESCE( associate.doc, offer.doc) AS doc
  FROM offer LEFT JOIN associate
  ON
    id_to_uuid( associate.doc->>'merchantId') = id_to_uuid( offer.doc->>'partyId')
  WHERE
    offer.id = $1
"""

exports.get_MerchantIds_Names_Roles = """
  SELECT
    merchant.id AS merchantId,
    merchant.doc->>'name' AS merchantName,
    associate.doc->>'role' AS role
  FROM associate JOIN merchant
  ON
    id_to_uuid( associate.doc->>'userId') = $1 AND
    merchant.id = id_to_uuid( associate.doc->>'merchantId')
"""

# TODO These casts to integer seem to be needed so int rather than strings are returned for bigints.  Is there an option somewher
# TODO to change the way bigints are handles?
# TODO split into 2 seperate db calls, one for claims and one for redemtions (join in middle tier.)
exports.get_ClaimsSummary_RedemptionsSummary = """
  SELECT
    claimsummary.instanceSite AS "instanceSite",
    claimsummary.instanceDisplayFrom AS "instanceDisplayFrom",
    claimsummary.num::integer AS "numberClaimed",
    claimsummary.total::integer AS "totalClaimed",
    claimsummary.when AS "lastClaimedDate",
    COALESCE( redemptionsummary.num, 0)::integer AS "numberRedeemed",
    COALESCE( redemptionsummary.total, 0)::integer AS "totalRedeemed",
    redemptionsummary.when AS "lastRedeemedDate"
  FROM (
    SELECT
      text_to_tsz( claim.doc->>'instanceDisplayFrom') AS instanceDisplayFrom,
      claim.doc->>'instanceSite' AS instanceSite,
      COUNT( claim) AS num,
      SUM( (claim.doc->>'count')::integer) AS total,
      MAX( text_to_tsz( claim.doc->>'created')) AS when
    FROM claim
    WHERE id_to_uuid( claim.doc->>'offerId') = $1
    GROUP BY instanceSite, instanceDisplayFrom
  ) AS claimsummary LEFT JOIN
  (
    SELECT
      text_to_tsz( redemption.doc->>'instanceDisplayFrom') AS instanceDisplayFrom,
      redemption.doc->>'instanceSite' AS instanceSite,
      COUNT( redemption) AS num,
      SUM( (redemption.doc->>'count')::integer) AS total,
      MAX( text_to_tsz( redemption.doc->>'time')) AS when
    FROM redemption
    WHERE id_to_uuid( redemption.doc->>'offerId') = $1
    GROUP BY instanceSite, instanceDisplayFrom
  ) AS redemptionsummary
  ON
    claimsummary.instanceDisplayFrom = redemptionsummary.instanceDisplayFrom
"""

exports.get_Offers_ClaimsSummary_RedemptionsSummary = """
  SELECT
    offer.* AS offer,
    claimsummary.instanceSite AS "instanceSite",
    claimsummary.instanceDisplayFrom AS "instanceDisplayFrom",
    claimsummary.num::integer AS "numberClaimed",
    claimsummary.total::integer AS "totalClaimed",
    claimsummary.when AS "lastClaimedDate",
    COALESCE( redemptionsummary.num, 0)::integer AS "numberRedeemed",
    COALESCE( redemptionsummary.total, 0)::integer AS "totalRedeemed",
    COALESCE( counts.numComments, 0)::integer AS "totalComments",
    COALESCE( counts.numScores, 0)::integer AS "totalLikes",
    redemptionsummary.when AS "lastRedeemedDate"
  FROM
    offer INNER JOIN (
      SELECT
        text_to_tsz( claim.doc->>'instanceDisplayFrom') AS instanceDisplayFrom,
        claim.doc->>'instanceSite' AS instanceSite,
        id_to_uuid( claim.doc->>'offerId') AS offerId,
        COUNT( claim) AS num,
        SUM( (claim.doc->>'count')::integer) AS total,
        MAX( claim.doc->>'created') AS when
      FROM claim
      WHERE id_to_uuid( claim.doc->>'userId') = $1
      GROUP BY offerId, instanceSite, instanceDisplayFrom
    ) AS claimsummary
    ON
      offer.id = claimsummary.offerId
    LEFT JOIN (
      SELECT
        text_to_tsz( redemption.doc->>'instanceDisplayFrom') AS instanceDisplayFrom,
        redemption.doc->>'instanceSite' AS instanceSite,
        id_to_uuid( redemption.doc->>'offerId') AS offerId,
        COUNT( redemption) AS num,
        SUM( (redemption.doc->>'count')::integer) AS total,
        MAX( redemption.doc->>'time') AS when
      FROM redemption
      WHERE id_to_uuid( redemption.doc->>'userId') = $1
      GROUP BY offerId, instanceSite, instanceDisplayFrom
    ) AS redemptionsummary
    ON
      offer.id = redemptionsummary.offerId
    LEFT JOIN (
      SELECT COUNT( doc->>'comment') as numComments, SUM( (doc->>'like') ::int) as numScores
      FROM feedback
      GROUP BY id_to_uuid( doc->>'offerId')
    ) as counts
    ON id_to_uuid( doc->>'offerId') = offer.id
  WHERE
    offer.id = $2 OR $2 IS NULL
"""