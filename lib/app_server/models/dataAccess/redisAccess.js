// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var _ = require('underscore');
var assert = require('assert');
var async = require('async');
var config = require('@oakus/hyphen-libs/config').config().redis;
var log = require('@oakus/hyphen-libs/glogger');
var genId = require('@oakus/hyphen-libs/genId');
var redis = require("redis");
var client = redis.createClient( config.port, config.host, config.options);

function _key( name, id) {
    return "2keyCounter:" + name + ":" + id;
}

function TwoLevelCounter( name) {
    this.name = name;
}

TwoLevelCounter.prototype.increment = function( userId, offerId, cb) {
    client.hincrby( _key( this.name, userId), offerId, 1, function( e, r) {
        if( e) log.error('redis unreadCount:increment', e);
        cb && cb( e)
    });
};

var HDELDELZR = '\
redis.call("HDEL", KEYS[1], ARGV[1]) \
if redis.call("HLEN", KEYS[1]) == 0 then \
    redis.call("DEL", KEYS[1]) \
return nil \
end';

TwoLevelCounter.prototype.clear = function (userId, offerId, cb) {
    client.eval( HDELDELZR, 1, _key( this.name, userId), offerId, function( e, r) {
        if( e) log.error('redis unreadCount:clear', e);
        cb && cb( e)
    });
};

TwoLevelCounter.prototype.counts = function( userId, cb) {
    var key = _key( this.name, userId);
    client.hgetall( key, function( e, r) {
        if( e) return log.error('redis unreadCount:counts', e);
        if( !r) r = {};
        _(r).forEach( function( v, k) { r[ k] = +v;} ); // Convert strings to ints
        cb( e, r);
    });
    client.expire( key, config.unreadCountsExpireDays*60*30*24, function( err) { if( err) log.error('redis error ', err);});
};

exports.unreadComments = new TwoLevelCounter( "comments");
exports.unreadLikes = new TwoLevelCounter( "likes");
