// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var config = require('@oakus/hyphen-libs/config').config();
var genId = require('@oakus/hyphen-libs/genId');
var errs = require('../errors');
var log = require('@oakus/hyphen-libs/glogger');
var verror = require('verror');
var _ = require('underscore');

var AWS = require('aws-sdk');

AWS.config.update({
    region: config.dynamoDB.region,
    maxRetries: config.dynamoDB.maxRetries,
    httpOptions: {
        timeout: config.dynamoDB.timeout
    }
});

db = new AWS.DynamoDB;

function responder( cb) {
    return function( err, result) {
        if( err) {
            var newErr = new verror.WError( err, 'dynamodb error "%s", table "%s"', err.code, config.dynamoDB.loginTable);
            log.error( 'dynamodb error: ', newErr);
            return cb( _(newErr).extend( errs.serviceUnavailable));
        }
        return cb( null, result);
    }
}

exports.upsert = function( userId, deviceToken, doc, cb) {
    var params = {
        TableName: config.dynamoDB.loginTable,
        Key: {
            'userId': { B: genId.idToUuid( userId)}
        },
        ExpressionAttributeValues: {
            ':doc': { S: JSON.stringify( doc)}
        },
        ExpressionAttributeNames: {
            '#deviceToken': deviceToken
        },
        UpdateExpression: "SET #deviceToken = :doc"
    };
    db.updateItem(params, responder( cb));
};

function _batchGet( keys, docs, msDelay, cb) {
    db.batchGetItem({ RequestItems: keys}, function( err, results) {
        if( err) return cb( err, docs);
        _(results.Responses[config.dynamoDB.loginTable]).each( function( userRecord) {
            docs.push.apply( docs, _(_(userRecord).omit('userId')).map( function( deviceDoc, deviceToken) {
                return _(JSON.parse( deviceDoc.S)).extend({
                    deviceToken: deviceToken,
                    userId: genId.uuidToId( userRecord.userId.B)
                });
            }));
        });
        if( _(results.UnprocessedKeys).keys().length) {
            return setTimeout( function() {
                _batchGet( results.UnprocessedKeys, docs, msDelay*2, cb)
            }, msDelay)
        }
        else
            return cb( null, docs);
    });
}

exports.getMostRecent_Logins = function( userIds, cb) {
    if( userIds.length == 0)
        return setImmediate( function() {cb(null, []);});
    var keys = []; keys[config.dynamoDB.loginTable] = {
        Keys: _(userIds).map( function( id) { return {userId: { B: genId.idToUuid( id)}};})
    };
    _batchGet( keys, [], 50, responder( cb));
};
