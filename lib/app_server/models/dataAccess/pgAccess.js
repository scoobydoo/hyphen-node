// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var log = require('@oakus/hyphen-libs/glogger');
var _ = require('underscore');
var assert = require('assert');
var moment = require('moment');
var async = require('async');
var errors = require('./../errors');
var config = require('@oakus/hyphen-libs/config').config();
var verror = require('verror');
var EE = require('events').EventEmitter;
require('pg-native');
var pg = require('pg').native;
var sql = require('./sql');
var cache = require('./cache');
var unreadCounter = require('./redisAccess');
var genId = require('@oakus/hyphen-libs/genId');
var repeatUtils = require( '../repeatUtils');
var startupMonitor = require('@oakus/hyphen-libs/startupMonitor');
var exec = require('child_process').exec;
var fs = require('fs');

pg.types.setTypeParser(2950, function(val) { // uuid type
    return val === null ? null : genId.uuidToId( val);
});

var dateParser = pg.types.getTypeParser( 1184);
pg.types.setTypeParser(1184, function(val) { // timezonetz type
    return val === null ? null : dateParser( val).toISOString();
});

var timeout_ms = config.postgresql.connectOptions.options.statement_timeout;

var dbClusters = _(( _(config.postgresql.clusters).each( function( connArray) {
    _(connArray).each( function(connectOptions) {
        _(connectOptions).defaults( config.postgresql.connectOptions);
    })
}))).defaults( { "default": [config.postgresql.connectOptions] });

startupMonitor.task( 'createDb', !config.postgresql.createDbOnStartup ? null : function( cb) {
    var superUser = config.postgresql.superUser.user;
    var superUserPass = config.postgresql.superUser.password;
    var dbHost = config.postgresql.connectOptions.host;
    var dbName = config.postgresql.connectOptions.database.toLowerCase();
    var dbUser = config.postgresql.connectOptions.user.toLowerCase();
    var dbPassword = config.postgresql.connectOptions.password;
    var dbTimeout = config.postgresql.connectOptions.options.statement_timeout;
    // '-d postgres' is needed here as on RDS, no matter what master user you set, it makes a 'postgres' db instead of a $user one.
    var cmdString = "psql -c 'create database " + dbName + ";' -U " + superUser + ' -d postgres -h "' + dbHost + '"';
    exec( cmdString, {env:{PGPASSWORD:superUserPass}}, function ( err) {
        if( err && /already exists\n$/.test(err.message))
            return cb( null, "Already Exists");    // swallow, nothing to do
        else if( err)
            return cb( err);
        var filename = __dirname + '/createDb.sql';
        var sql = fs.readFileSync(filename, {encoding:'utf8'});
        sql = sql.replace( new RegExp('\\$user', 'g'), dbUser).replace( new RegExp('\\$password', 'g'), dbPassword).replace( new RegExp('\\$statement_timeout', 'g'), dbTimeout);
        exec( "psql -f - -d " + dbName + " -U " + superUser + ' -h "' + dbHost +'"', {env:{PGPASSWORD:superUserPass}}, cb)
            .stdin.end( sql);
    });
});

function _mapError( cardinality, err, dbResult, fields) {
    var newErr = null;
    if( err) {
        newErr = new verror.WError( err, 'Postgres call error');

        if( err.sqlState === '23505')
            _(newErr).extend( errors.alreadyExists);
        else if( err.sqlState === '22P02' || err.sqlState === '22023') { // malformed uuid string TODO do we need to check all input params to the db?
            _(newErr).extend( errors.notFound);
            log.warn( 'postgresql error: ', _(newErr).defaults( fields));
            if( cardinality === 'optional' || cardinality === 'any' )
                newErr = undefined; // swallow this
        }
        else {
            if( newErr.isTimeout)
                _(newErr).extend( errors.serviceTimeout);
            else
                _(newErr).extend( errors.serviceUnavailable);
            log.error( _(newErr).defaults( fields));
        }
    }
    else if( cardinality !== 'any' && cardinality !== 'optional' && dbResult.rows.length == 0) newErr =  _(new verror.VError( 'Postgres error: No results found.')).extend( errors.notFound);
    else assert( cardinality !== 'one' || dbResult.rows.length == 1, 'Postgres error: Was expecting exactly 1 result, got ' + dbResult.rows.length);

    return newErr;
}

// TODO allow multiple objects (from multiple tables) in each row.  Will need a method of disambiguating names (e.g. offer$id, merchant$id)
function _processResults( dbResults, opts) {
    var underscoreOpt = opts && opts._;
    if( !dbResults) return {rows:[]}; // TODO see pg error err.code === '22P02' above
    var docFieldInfo = _(dbResults.fields).findWhere( {dataTypeID: 114}); // json type
    /*    var metadataFields = !docFieldInfo ? [] : _(dbResults.fields).chain()
     .filter( function( fieldInfo) { return fieldInfo.tableID === docFieldInfo.tableID;})
     .pluck( 'name')
     .without( docFieldInfo.name)
     .value();
     */
    var metadataFields = !docFieldInfo ? [] : ['id','created','modified','version'];
    var deNormalisedFields = !(underscoreOpt && docFieldInfo) ? [] : _(dbResults.fields).chain()
//        .filter( function( fieldInfo) { return fieldInfo.tableID !== docFieldInfo.tableID;})
        .filter( function( fieldInfo) { return !_(metadataFields).include( fieldInfo.name);})
        .pluck( 'name')
        .without( docFieldInfo.name) // TODO delete me if we have tableID again
        .value();
//    var uuidFields = _(_(dbResults.fields).where( {dataTypeID: 2950})).pluck( 'name');  // uuid type
//    var dateFields = _(_(dbResults.fields).where( {dataTypeID: 1184})).pluck( 'name');  // timestamptz type

    if( docFieldInfo)
        _(dbResults.rows).each( function (row) {
            var jsonDoc = row[docFieldInfo.name];

//        _(_(row).pick( uuidFields)).each( function (value, key) {
//            row[ key] = genId.uuidToId( value);
//        });

//        _(_(row).pick( dateFields)).each( function (value, key) {
//            row[ key] = value.toISOString();
//        });

            if( metadataFields.length) {
                jsonDoc.$ = _(row).pick( metadataFields);
                delete jsonDoc.created; // TODO remove this when we have new data
                delete jsonDoc.modified; // TODO remove this when we have new data
                delete jsonDoc.id; // TODO remove this when we have new data
                delete jsonDoc.authSecret; // TODO remove this when we have new data
            }
            if( deNormalisedFields.length)
                jsonDoc._ = _(row).pick( deNormalisedFields);
        });
    return dbResults;
}

function _timeoutFunc( func, cb) {
    var startTime = moment();
    var called = false;
    var myCb = function( e, r) {
        if( called)
            return log.error( 'pg double callback!'); // Only actually seen this when we pause in the debugger, associated with timeouts
        called = true;
        if( moment().diff( startTime) >= timeout_ms) {
            e = new verror.VError( e, 'Postgres call timeout after %d ms.', timeout_ms);
            e.isTimeout = true;
        }
        cb(e, r);
    };
    func( myCb);
}

function Transaction( clusterName) {
    if( !clusterName) clusterName = 'default';
    return {
        connectOptions: dbClusters[clusterName][ _.random( 0, dbClusters[clusterName].length - 1)],
        client: null,
        inTransaction: false,
        doneFunc: null,

        docQuery: function docQuery( queryString, paramArray, cb) {
            this.query( queryString, paramArray, 'one', function( err, dbResults) {
                if (err) return cb && cb(err);
                dbResults = _processResults( dbResults, {_: true});
                cb && cb( null, dbResults.rows[0]);
            });
        },
        docsQuery: function docQuery( queryString, paramArray, cb) {
            this.query( queryString, paramArray, 'any', function( err, dbResults) {
                if( err) return cb && cb( err);
                dbResults = _processResults( dbResults, {_: true});
                cb && cb( null, {
                    docs:_(dbResults.rows).pluck( 'doc'),
                    versions: _(dbResults.rows).pluck( 'version')
                });
            });
        },
        rowsQuery: function docQuery( queryString, paramArray, cb) {
            this.query( queryString, paramArray, 'any', function( err, dbResults) {
                if( err) return cb && cb( err);
                dbResults = _processResults( dbResults);
                cb && cb( err, {rows:dbResults.rows});
            });
        },
        docUpdateQuery: function docQuery( queryString, paramArray, getFunc, cb) { // TODO pass getQueryString + params instead of getFunc
            this.query( queryString, paramArray, 'optional', function( err, dbResults) {
                if( err) return cb && cb( err);
                dbResults = _processResults( dbResults, {_: true});
                if( dbResults.rows.length == 0) // No result from update - either doc was not found, or version mismatch
                    return getFunc( function( err, docResult) {
                        if( err) return cb( err);
                        docResult.error = errors.conflict;
                        return cb && cb( null, docResult);
                    });
                else
                    return cb && cb( null, dbResults.rows[0]);
            });
        },

        query: function query( queryString, paramArray, cardinality, cb) {
            var runningQuery;
            _timeoutFunc( function( cb) {
                if( this.client)
                    runningQuery = this.client.query( queryString, paramArray, cb);
                else {
                    pg.connect( this.connectOptions, function ( err, client, done) {
                        if( client && !EE.listenerCount(client, 'notice')) // if client is reused from pool, cb is already attached
                            client.on('notice', function(msg) {
                                log.info("postgres notice", {message: msg, query: queryString, params: paramArray});
                            });
                        this.doneFunc = done;
                        this.client = client;
                        if( err)
                            cb( err);
                        else
                            runningQuery = client.query( queryString, paramArray, cb);
                    }.bind( this));
                }
            }.bind( this), function( e, r) {
                e = _mapError( cardinality, e, r, {query: queryString, params: paramArray, runningQuery: !!runningQuery, client: !!this.client});

                if(e && this.inTransaction && this.client)
                    this.rollback();

                if( !this.inTransaction && this.doneFunc) {
                    this.client = null;
                    this.doneFunc(); // Release client back to pool
                }

                cb( e, r);
            }.bind( this));
            return this;
        },
        begin: function begin( cb) {
            assert( !this.inTransaction);
            this.inTransaction = true;
            return this.query( 'BEGIN', [], 'optional', cb);
        },
        commit: function commit( cb) {
            assert( this.inTransaction);
            this.query( 'COMMIT', [], 'optional', function( e, r) {
                this.inTransaction = this.client = null;
                this.doneFunc();
                cb(e, r);
            }.bind( this));
            return null;
        },
        rollback: function rollback( cb) {
            assert( this.inTransaction);
            this.client.query( 'ROLLBACK', [], function( e, r) {
                this.inTransaction = this.client = null;
                this.doneFunc( e);
                if( cb) cb(e, r);
            }.bind( this));
            return null;
        }
    };
}

function docQuery( queryString, paramArray, cb) {
    var t = new Transaction();
    return t.docQuery( queryString, paramArray, cb);
}
function docsQuery( queryString, paramArray, cb) {
    var t = new Transaction();
    return t.docsQuery( queryString, paramArray, cb);
}
function rowsQuery( queryString, paramArray, cb) {
    var t = new Transaction();
    return t.rowsQuery( queryString, paramArray, cb);
}
function docUpdateQuery( queryString, paramArray, getFunc, cb) {
    var t = new Transaction();
    return t.docUpdateQuery( queryString, paramArray, getFunc, cb);
}
function begin( cb) {
    var t = new Transaction();
    return t.begin( cb);
}

//Creation
exports.newUser = function( id, doc, cb) {
    docQuery( sql.newUser, [genId.idToUuid( id), doc], cb);
};

exports.newMerchantAndAssociate = function( merchantId, merchant, associate, cb) {
    var transaction = new Transaction();
    async.auto({
        'begin': function (cb) {
            transaction.begin( cb);
        },
        'merchant': ['begin', function( cb) {
            transaction.docQuery( sql.newMerchant, [genId.idToUuid( merchantId), merchant], cb);
        }],
        'associate': ['begin', function( cb) {
            transaction.docQuery( sql.newAssociate, [associate], cb);
        }],
        commit: ['merchant', 'associate', function (cb) {
            transaction.commit( cb);
        }]
    }, function( err, results) {
        cb(err, results ? results.merchant : null);
    });
};

exports.newIdentity = function( doc, cb) {
    docQuery( sql.newIdentity, [doc], cb);
};

exports.newOffer = function( id, doc, cb) {
    var offerId = genId.idToUuid( id);
    var offerInstanceRows = [];
    _(doc.sites).each( function( site, siteName) {
        _(repeatUtils.timestampSeries(doc.displayFrom, doc.repeat)).each( function( instanceDisplayFrom) {
            offerInstanceRows.push( [
                offerId,
                siteName,
                instanceDisplayFrom,
                doc.displayUntil? moment( instanceDisplayFrom).add( moment(doc.displayUntil).diff(moment( doc.displayFrom))).toISOString() : null,
                site.location
            ]);
        });
    });

    var transaction = new Transaction();
    async.auto({
        'begin': function (cb) {
            transaction.begin( cb);
        },
        'offer': ['begin', function( cb) {
            transaction.docQuery( sql.newOffer, [offerId, doc], cb);
        }],
        'instances': ['begin', function( cb) {
            if( offerInstanceRows.length == 0) return cb();
            async.parallel( _(offerInstanceRows).map( function (row) {
                return function( cb) { transaction.docQuery( sql.newofferInstance, row, cb);}
            }), cb)
        }],
        commit: ['instances', 'offer', function (cb) {
            transaction.commit( cb);
        }]
    }, function( err, results) {
        cb(err, results ? results.offer : null);
    });
};

exports.newDesire = function( id, doc, cb) {
    docQuery( sql.newDesire, [genId.idToUuid( id), doc], cb);
};

exports.newAssociate = function( doc, cb) {
    docQuery( sql.newAssociate, [doc], cb);
};

exports.newBlock = function( doc, cb) {
    docQuery( sql.newBlock, [doc], cb);
};

exports.newClaim = function( doc, maxVouchers, cb) {
    var offerUuid = genId.idToUuid( doc.offerId);
    var transaction = new Transaction();
    async.auto( {
        'begin': function (cb) {
            transaction.begin( cb);
        },
        'claimedUpdate': ['begin', function( cb) {
            transaction.docUpdateQuery( sql.updateOfferInstanceClaimed, [offerUuid, doc.instanceSite, doc.instanceDisplayFrom, doc.count, maxVouchers],
                _( get_OfferInstance).partial( doc.offerId, doc.instanceSite, doc.instanceDisplayFrom), cb);
        }],
        'newClaim': ['begin', function( cb) {
            transaction.docQuery( sql.newClaim, [doc], cb);
        }],
        commit: ['claimedUpdate', 'newClaim', function (cb) {
            transaction.commit( cb);
        }]
    }, function( err, results) {
        if( err && err.code !== 5)  // NOT (Conflict, no vouchers left)
            return cb( err);
        var res = {
            doc: {
                offerId: doc.offerId,
                instanceSite: doc.instanceSite,
                instanceDisplayFrom: doc.instanceDisplayFrom,
                count: doc.count,
                totalUnclaimed: maxVouchers - results.claimedUpdate.claimed
            }
        };
        if( results.claimedUpdate.error && results.claimedUpdate.error.code === 5) {  // Conflict, no vouchers left
            res.error = errors.alreadyExists;
            res.doc.count = 0;
        }

        cb(err, res);
    });
};

exports.newRedemption = function( doc, cb) {
    docQuery( sql.newRedemption, [doc], cb);
};

exports.newPosting = function( doc, cb) {
    docQuery( sql.newPosting, [doc], cb);
};

exports.newResponse = function( doc, cb) {
    docQuery( sql.newResponse, [doc], cb);
};

exports.newFeedback = function( id, doc, cb) {
    var ratingId = genId.idToUuid( id);
    docQuery( sql.newFeedback, [ratingId, doc], cb);
};

/*
 exports.newCheckin = function( userId, doc, cb) {
 fs.newCheckin( userId, doc, cb);
 };
 */
// Update

exports.updateUser = function( id, doc, currentVersion, cb) {
    var uuid = genId.idToUuid( id);
    if( currentVersion !== null && currentVersion !== undefined)
        docUpdateQuery( sql.updateUserCheckVersion, [uuid, doc, currentVersion], _(exports.get_User).partial(id), cb);
    else
        docQuery( sql.updateUser, [uuid, doc], cb);
};

exports.incUserRepScore = function( id, delta, cb) {
    docQuery( sql.updateIncRepScore, [ genId.idToUuid( id), delta], cb);
};

exports.updateMerchant = function( id, doc, currentVersion, cb) {
    var uuid = genId.idToUuid( id);
    if( currentVersion !== null && currentVersion !== undefined)
        docUpdateQuery( sql.updateMerchantCheckVersion, [uuid, doc, currentVersion], _(exports.get_Merchant).partial(id), cb);
    else
        docQuery( sql.updateMerchant, [uuid, doc], cb);
};

exports.updateIdentity = function( doc, currentVersion, cb) {
    if( currentVersion !== null && currentVersion !== undefined)
        docUpdateQuery( sql.updateIdentityCheckVersion, [doc, currentVersion], _(exports.get_Identity).partial(doc), cb);
    else
        docQuery( sql.updateIdentity, [doc], cb);
};

exports.updateOffer = function( id, doc, currentVersion, cb) {
    var uuid = genId.idToUuid( id);
    if( currentVersion !== null && currentVersion !== undefined)
        docUpdateQuery( sql.updateOfferCheckVersion, [uuid, doc, currentVersion], _(exports.get_Offer).partial(id), cb);
    else
        docQuery( sql.updateOffer, [uuid, doc], cb);
};

exports.updateDesire = function( id, doc, currentVersion, cb) {
    var uuid = genId.idToUuid( id);
    if( currentVersion !== null && currentVersion !== undefined)
        docUpdateQuery( sql.updateDesireCheckVersion, [uuid, doc, currentVersion], _(get_Desire).partial(id), cb);
    else
        docQuery( sql.updateDesire, [uuid, doc], cb);
};

exports.updateAssociates = function( merchantId, userId_Roles, cb) {
    // doc: [[ userId, role]]
    //fs.updateAssociates( merchantId, userId_Roles, cb);
};

// deletes

exports.deleteBlock = function( doc, cb) {
    docQuery( sql.deleteBlock, [genId.idToUuid( doc.userId), genId.idToUuid( doc.partyId)], cb);
};

exports.deleteIdentity = function( partyId, scope, indentifier, cb) {
    docQuery( sql.deleteIdentity, [ genId.idToUuid( partyId), scope, indentifier], cb);
};

// Queries

exports.get_User = function( userId, cb) {
    // result: user
    docQuery( sql.get_User, [genId.idToUuid(userId)], cb);
};

exports.getUser_repScore = function( userId, cb) {
    docQuery( sql.getUser_repScore, [genId.idToUuid(userId)], cb);
};

exports.get_Merchant = function( merchantId, cb) {
    // result: merchant
    docQuery( sql.get_Merchant, [genId.idToUuid(merchantId)], cb);
};

exports.getNamed_PartyId = function( name, cb) {
    // result: user|merchant
    docQuery( sql.getNamed_PartyId, [name], cb);
};

exports.get_Offer = function( id, cb) {
    // result: offer
    docQuery( sql.get_Offer, [genId.idToUuid( id)], cb);
};

var get_OfferInstance = function( id, site, start,  cb) {
    // result: offer
    docQuery( sql.get_OfferInstance, [genId.idToUuid( id), site, start], cb);
};

var get_Desire = function( id, cb) {
    // result: desire
    docQuery( sql.get_Desire, [genId.idToUuid( id)], cb);
};

exports.getUser_Desires = function( userId, maxResults, iterator, cb) {
    // result: [desire]
    docsQuery( sql.getUser_Desires, [genId.idToUuid( userId)], cb);
};

exports.get_Identities = function( partyId, cb) {
    // result: [identity]
    docsQuery( sql.get_Identities, [genId.idToUuid( partyId)], cb);
};

exports.get_Identity = function( partyId, scope, identifier, cb) {
    // result: identity
    docQuery( sql.get_Identity, [genId.idToUuid( partyId), scope, identifier], cb);
};

exports.getAuthorised_Users = function( userId, cb) {
    // result: [users]
    docsQuery( sql.getAuthorised_Users, [genId.idToUuid( userId)], cb);
};

exports.get_Offers = function( partyId, maxResults, cb) {
    // result: [offer]
    docsQuery( sql.get_Offers, [genId.idToUuid( partyId)], cb);
};

exports.getBlocked_Offers = function( userId, maxResults, cb) {
    // result: [offer]
    docsQuery( sql.getBlocked_Offers, [genId.idToUuid( userId)], cb);
};

function _getNearbyOffersDb( long, lat, time, userId, cb) {
    docsQuery( sql.getNearby_Offers, [long, lat, time, genId.idToUuid( userId), config.postgresql.nearbyOffersLimit], cb);
}

exports.getNearby_Offers = function( long, lat, userId, time, queryStr, maxResults, iterator, cb) {
    // result: [offer]
    if( !maxResults || maxResults > config.postgresql.nearbyOffersLimit) maxResults = config.postgresql.nearbyOffersLimit;
    if( iterator) {
        cache.getArray( iterator, maxResults, function( err, results, newIterator) {
            if( err) return cb( errors.serviceUnavailable);
            if( !results) cb( errors.resultGone);
            else cb( null, {docs:results, iterator: newIterator});
        })
    }
    else {
        _getNearbyOffersDb( long, lat, time, userId, function( err, dbResult) {
            if( err) return cb( err);
            var iterator = cache.newArray( dbResult.docs, maxResults);
            cb( null, {docs:_(dbResult.docs).first( maxResults), iterator: iterator});
        });
    }
};

exports.getStarting_Offers = function( startFrom, startUntil, cb) {
    (new Transaction('startingOffers')).docsQuery( sql.getStarting_Offers, [ startFrom, startUntil], cb);
};

exports.getPartyClaimees_UserIds = function( partyId, cb) {
    (new Transaction('startingOffers')).rowsQuery( sql.getPartyClaimees_UserIds, [ genId.idToUuid( partyId)], cb);
};

exports.get_Feedback_Names = function( offerId, maxResults, cb) {
    // result: [{rating, name}]
    docsQuery( sql.get_Feedback_Names, [genId.idToUuid( offerId)], cb);
};

var _roleCb = function( cb) {
    return function (err, result) {
        if( err && err.code == errors.notFound.code) return cb( null, 'none');
        if( err) return cb( err);
        cb( null, result.doc.role);
    };
};

exports.getMerchant_Role = function( userId, merchantId, cb) {
    // role
    docQuery( sql.getMerchant_Role, [genId.idToUuid( userId), genId.idToUuid(merchantId)], _roleCb( cb));
};

exports.getOffer_Role = function( userId, offerId, cb) {
    // role
    docQuery( sql.getOffer_Role, [genId.idToUuid( userId), genId.idToUuid( offerId)], _roleCb( cb));
};

exports.getOffer_Roles = function( offerId, cb) {
    // [{ merchantId, merchantName, role}]
    docsQuery( sql.getOffer_Roles, [genId.idToUuid( offerId)], cb);
};

exports.get_MerchantIds_Names_Roles = function( userId, cb) {
    // [{ merchantId, merchantName, role}]
    rowsQuery( sql.get_MerchantIds_Names_Roles, [genId.idToUuid( userId)], cb);
};

exports.getAssociated_UserIds_Names_Roles = function( merchantId, cb) {
    // result:  [[ userId, userName, role]]
};

exports.get_ClaimsSummary_RedemptionsSummary = function( offerId, cb) {
    // result: {offer, numberClaimed, totalClaimed, numberRedeemed, totalRedeemed, lastClaimedDate, lastRedeemedDate}
    rowsQuery( sql.get_ClaimsSummary_RedemptionsSummary, [genId.idToUuid( offerId)], cb);
};

exports.get_Offers_ClaimsSummary_RedemptionsSummary = function( userId, queryObj, maxResults, cb) {
    // result: [{offer, numberClaimed, totalClaimed, numberRedeemed, totalRedeemed, lastClaimedDate, lastRedeemedDate}]
    docsQuery( sql.get_Offers_ClaimsSummary_RedemptionsSummary, [genId.idToUuid( userId), queryObj.offer? genId.idToUuid( queryObj.offer):null], cb);
};
