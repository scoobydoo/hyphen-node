// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var _ = require('underscore');
var assert = require('assert');
var async = require('async');
var config = require('@oakus/hyphen-libs/config').config().redis;
var log = require('@oakus/hyphen-libs/glogger');
var genId = require('@oakus/hyphen-libs/genId');
var redis = require("redis");
var client = redis.createClient( config.port, config.host, config.options);

var itParserRegEx = new RegExp( "^(\\d+)\\.(\\d+)\\.(" + genId.idRegexStr() + ')');

exports.newArray = function( values, offset, cb) {
    if( values.length == 0) {
        setImmediate( function() { if(cb) cb( null, 'end')});
        return 'end';
    }

    var key = genId.newId();
    var valueStrings = [key].concat(_(values).map( function( i) { return JSON.stringify(i);}));
    var iterator = offset>=values.length ? 'end' : offset + '.' + values.length + '.' + key;
    client.multi()
        .rpush( valueStrings)
        .expire( key, config.nearbyOffersExpireSec)
        .exec( function( err) {
            if( err) log.error('redis newArray error ', err);
            if( cb) cb( err, iterator);
        });
    return iterator;
};

exports.getArray = function( iterator, maxResults, cb) {
    var start = null, length = null, key = null;
    maxResults = parseInt( maxResults);

    if( iterator == 'end')
        return setImmediate( function() { cb( null, [], 'end')});

    var regMatch = itParserRegEx.exec( iterator);
    if( !regMatch)
        return setImmediate( function() { cb( null, null)});
    if( regMatch) {
        start = parseInt( regMatch[1]);
        length = parseInt( regMatch[2]);
        key = regMatch[3];
    }
    var newIterator = (start+maxResults) >= length ? 'end' : (start+maxResults) + '.' + length + '.' + key;
    var stop = start + maxResults - 1;
    client.lrange( key, start, stop, function (err, result) {
        if( err) log.error('redis getArray error', err);
        if( !result) return cb( err, null);
        client.expire( key, config.nearbyOffersExpireSec, function( err) { if( err) log.error('redis error ', err);});
        result = _(result).map( function(i) {return JSON.parse(i);});
        cb( err, result, newIterator);
    });
};