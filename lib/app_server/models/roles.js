// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var _ = require('underscore');
var data = require( './dataAccess/pgAccess');
var errs = require( './errors');
var rsvp = require( './responder').responder;

exports.getOffer_Roles = function( offerId, cb) {
    cb = rsvp( cb);
    data.getOffer_Roles ( offerId, function( e, r) {
        if( e) return cb( e);
        if( !r.docs.length) return cb( errs.notFound);

        // If no roles for this offer it must be a User offer, which the DB returns.  List the user as an admin
        if( r.docs[0].partyId)
            r.docs = [{
                merchantId: null,
                userId: r.docs[0].partyId,
                role: "admin"
            }];

        cb(e, r);
    });
};
