// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com

module.exports = {
    user: require('./user'),
    offer: require('./offer'),
    roleDocs: require('./roleDocs')
};