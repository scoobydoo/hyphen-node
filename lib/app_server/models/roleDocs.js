// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var moment = require('moment');
var _ = require('underscore');
var data = require( './dataAccess/pgAccess');
var merchantModel = require( './merchant');
var userModel = require('./user');
var errs = require( './errors');
var rsvp = require( './responder').responder;

exports.getRoleDocs = function( userId, invokerId, cb) {
    cb = rsvp( cb);
    data.getAuthorised_Users( userId, function( err, results) { // TODO This should only return doc ids, not docs.
        if( err) return cb(err);
        var oldestUserId;
        var allUser = _(results.docs).filter( function( user) { return (!user.deleted)});
        if( allUser.length == 0) // The user wasn't authorised/ verified
            oldestUserId = userId;
        else {
            if( _(allUser).every( function( user) { return (user.$.id !== invokerId);}))
                return ( cb( errs.notAthorized));
            oldestUserId = _(allUser).min( function( user) { return moment(user.$.created)}).$.id;
        }
        return userModel.getUser( oldestUserId, oldestUserId, function (err, userResult) {
            if( err) return cb(err);
            var user = userResult.doc;
            assert( _(user._.roles.admin).keys().length <= 1);
            assert( _(user._.roles.poster).keys().length == 0); // Only support 1 admin merchant at present

            var resultDoc = {
                user: user,
                adminMerchants: [],
                postMerchants: []
            };

            if( _(user._.roles.admin).keys().length) {
                merchantModel.getMerchant( _(user._.roles.admin).keys()[0], oldestUserId, function (err, merchantResult) {
                    if( err) return cb(err);
                    resultDoc.adminMerchants.push(merchantResult.doc);
                    cb( null, {docs: [resultDoc]});
                });
            }
            else
                cb( null, {docs: [resultDoc]});
        });
    })
};

/*
 exports.getRoles = function( userId, invokerId, cb) {
 if( userId !== invokerId)
 return cb( errs.notAthorized);

 data.get_MerchantIds_Names_Roles( userId, function (err, result) {
 if (err) return cb( err);
 var array = result.rows;
 var result = {admin:{},poster:{}};
 _(array).forEach( function( item) {
 result[ item.role][ item.merchantid] = item.merchantname;
 });
 cb( err, {docs:result});
 })
 };
 */

