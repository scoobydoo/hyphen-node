// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/feedback');
var rsvp = require('./httpResponder');

exports.create = function(req, res) {
    var newFeedback = req.body;
    newFeedback.offerId = req.params.offerid;
    model.newFeedback( newFeedback, req.user.id, rsvp.responder( res));
};