// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/voucher');
var rsvp = require('./httpResponder');

exports.claim = function(req, res) {
    var newClaim = req.body;
    newClaim.userId = req.params.userid;
    model.newClaim( newClaim, req.user.id, rsvp.responder( res));
};

exports.redeem = function(req, res) {
    var newRedemption = req.body;
    newRedemption.userId = req.params.userid;
    model.newRedemption( newRedemption, req.user.id, rsvp.responder( res));
};