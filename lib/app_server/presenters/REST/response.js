// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/response');
var rsvp = require('./httpResponder');

exports.create = function(req, res) {
    var newResponse = req.body;
    newResponse.userId = req.params.userid;
    model.newResponse( newResponse, req.user.id, rsvp.responder( res));
};