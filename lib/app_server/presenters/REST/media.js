// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var genId = require('@oakus/hyphen-libs/genId');
var _ = require('underscore');
var model = require('./../../models/user');
var assert = require('assert');
var log = require('@oakus/hyphen-libs/glogger');
var image = require('imagemagick-native');
var config = require('@oakus/hyphen-libs/config').config();
var AWS = require( config.AwsModulePath);
AWS.config.update({region: config.s3.region, maxRetries: config.s3.maxRetries, httpOptions: {timeout: config.s3.timeout}});
var s3 = new AWS.S3();
var jsonParser = require( '@oakus/hyphen-libs/json-parser').parser( {reqSchema:{}});
var http = require( 'http');
var domain = require( 'domain');

var imageTypeInfo = {
    'JPEG': {
        mimeType: 'image',
        mimeSubType: 'jpeg',
        eTags: {
            'fill' : 'jpg_fill_v1',
            'aspectfill': 'jpg_aspectfill_v1',
            'aspectfit': 'jpg_aspectfit_v1',
            'none': 'jpg_none_v1'
        }
    },
    'PNG': {
        mimeType: 'image',
        mimeSubType: 'png',
        eTags: {
            'fill' : 'png_fill_v1',
            'aspectfill': 'png_aspectfill_v1',
            'aspectfit': 'png_aspectfit_v1',
            'none': 'png_none_v1'
        }
    }
};
var allETags = _(imageTypeInfo).chain()
    .values()
    .pluck('eTags')
    .reduce( function( memo, eTags) {
        return memo.concat(_(eTags).values());
    }, [])
    .value();
var allResizeOptions = _(imageTypeInfo).chain()
    .values()
    .pluck('eTags')
    .reduce( function( memo, eTags) {
        return memo.concat(_(eTags).keys());
    }, [])
    .unique()
    .value();
var mimeToETagMap = _(imageTypeInfo).chain()
    .values()
    .map( function( item) {
        return [item.mimeType + '/' + item.mimeSubType, item.eTags];
    })
    .object()
    .value();


var cacheControlHeader = 'max-age=' + config.mediaCacheMaxAge;  // 28 days
var maxUploadBytes = config.mediaMaxUploadSizeM*1024*1024;

exports.create = function(req, res, dontRequireContentLengthHeader) {
    var aborted = false;

    // Body can point to uri that should be fetched.
    if( req.headers['content-type'] ==='application/json') {
        return jsonParser( req, res, function() {
            http.get( req.body.uri)
                .on('error', function( err) {
                    if( aborted) return;
                    aborted = true;
                    log.error( 'Failed to fetch image from uri', err);
                    res.status( 504).send();
                })
                .on('response', function (incomingMessage) {
                    exports.create( incomingMessage, res, true);
                })
        })
    }

    var bytesNeededToId = config.identifyMediaBufferSizeK*1024;
    var mediaInfo = {id:genId.newId()};
    var first100Bytes = new Buffer(bytesNeededToId);
    var totalBytesSent = 0;
    var declaredLength = parseInt(req.headers['content-length']);

    var s3Stream = null;
    var successCount = 0;
    var errorCount = 0;
    var UploadStartTime = new Date().toISOString();

    if (!dontRequireContentLengthHeader && !declaredLength) {
        log.error( 'No content-length header set for media upload');
        req.pause();
        res.header('Connection', 'close'); // with the Connection: close header set, node will automatically close the socket...
        return res.status(411).send(); // No Content-Length header
    }

    if( declaredLength >= maxUploadBytes){
        log.error( 'Media upload too big');
        req.pause();
        res.header('Connection', 'close'); // with the Connection: close header set, node will automatically close the socket...
        return res.status(413).send(); // Too big
    }

    function canIdentifyAndSend( bufffer, theEnd) {
        assert ( !s3Stream);
        if( !declaredLength) return false;  // If no content-length header, don't identify until we have the whole thing.

        var typeData = null;
        try {
            typeData = image.identify({srcData: bufffer});
        } catch( e) {}
        if( !typeData || !imageTypeInfo[typeData.format] || (theEnd && typeData.error))
            return false;
        mediaInfo.type = imageTypeInfo[typeData.format].mimeType;
        mediaInfo.subtype = imageTypeInfo[typeData.format].mimeSubType;
        mediaInfo.x = typeData.width;
        mediaInfo.y = typeData.height;

        var options = {
            Bucket: config.s3.mediaBucket,
            Key: mediaInfo.id,
            ACL: 'private',
            ContentLength: declaredLength,
            ContentType: mediaInfo.type + '/' + mediaInfo.subtype,
            Metadata: {x: mediaInfo.x.toString(), y: mediaInfo.y.toString()}
        };
        if( theEnd) {
            options.Body = bufffer;
        }
        else {
            req.pause();
            req.unshift( bufffer);          // Put it back on the stream (we're the only listener here, so safe.)
            req.setMaxListeners(15);        // This seems to max out otherwise
            options.Body = req;
        }


        s3Stream = s3.putObject( options)
            .on('error', function (err) {
                if( aborted) return;
                if( errorCount++ === 0) {
                    aborted = true;
                    log.error( 'S3 write error', _(err).defaults( {declaredLength:declaredLength, totalBytesSent:totalBytesSent, UploadStartTime:UploadStartTime }));
//                    req.pause();
//                    res.header('Connection', 'close'); // with the Connection: close header set, node will automatically close the socket...
                    res.status( 504).send();
                }
                else
                    log.warn( 'S3 error double callback #', errorCount);
            })
            .on('success', function (response) {
                if( aborted) return;
                if( successCount++ === 0) {
                    res.send( mediaInfo);
                }
                else
                    log.warn( 'S3 success double callback #', successCount);

            })
            .send();

        if ( !theEnd) req.resume();

        return true;
    }

    req.on('error', function (err) {
        if( aborted) return;
        log.warn( 'Media upload error', err);
        aborted = true;
        s3Stream.abort();
    });
    req.on('data', function ( data) {
        if( req.connection.destroyed) aborted = true;
        if( aborted) return;
        var bufferToSend = {};
        var totalBytesRead = totalBytesSent;
        totalBytesSent += data.length;
        /*
         Express seems to do this for us.
         if(totalBytesSent >= maxUploadBytes) {
         aborted = true;
         log.error( 'Media upload too big');
         req.pause();
         res.header('Connection', 'close'); // with the Connection: close header set, node will automatically close the socket...
         res.send(413); // Too big                     // ... after sending a response
         return;                                       // And finish
         }
         */
        if( totalBytesRead < bytesNeededToId) {             // Haven't already done it
            if (totalBytesSent < bytesNeededToId) {         // Don't have enough yet to identify
                data.copy( first100Bytes, totalBytesRead);
            }
            else {                                          // Have enough data to identify
                if ( totalBytesRead == 0)
                    bufferToSend = data;
                else
                    bufferToSend = Buffer.concat( [first100Bytes.slice(0, totalBytesRead), data], totalBytesSent);
                if( !canIdentifyAndSend( bufferToSend)) {          //  If we didn't identify it
                    bytesNeededToId = totalBytesSent * 2;   //  ... try twice as much data
                    var newBuf = new Buffer( bytesNeededToId);
                    bufferToSend.copy( newBuf);
                    first100Bytes = newBuf;
                }
            }
        }
    });
    req.on('end', function () {
        if( req.connection.destroyed) aborted = true;
        if( aborted) return;
        declaredLength = totalBytesSent;                // In case it wasn't sent, now we know it.
        if( totalBytesSent < bytesNeededToId)    // Must have the whole file, identify with what we have, and send
            if ( !canIdentifyAndSend( first100Bytes.slice(0, totalBytesSent), true))
                res.status( 415).send();
    });
};

exports.read = function(req, res){
    var mediaId = req.params.mediaid;
    var key;
    if( genId.validId( mediaId)) // Old style TODO dlete when remove api support and old-style sample images
        key = mediaId;
    else
        key = config.s3.keyByUser ? '1/u/' + req.user + '/m/' + mediaId : '1/m/' + mediaId;

    var x = req.query.x;
    var y = req.query.y;
    var resize = req.query.resize;

    if((x || y)) {    // we're resizing
        if( !_(allResizeOptions).contains( resize))
            resize = 'fill';
    }
    else
        resize = 'none';

    var eTag = req.header('if-none-match');
    if (_(allETags).contains( eTag)) {
        res.setHeader('Cache-Control', cacheControlHeader);
        res.setHeader('ETag', eTag);
        return res.status( 304).send();
    }

    var s3Options = {
        Bucket: config.s3.mediaBucket,
        Key: key
    };

    s3.getObject( s3Options, function (err, response){
        if( err) {
            log.warn( 'S3 read error', err);
            return res.status( (err.statusCode == 404 || err.statusCode == 403) ? 404 : 504).send(); // We get a 403 if we don't have lists access
        }

        var buffer = response.Body;

        var mediaInfo;
        if( response.Metadata && response.Metadata.mediainfo)
            mediaInfo = JSON.parse( decodeURIComponent( response.Metadata.mediainfo));
        else
            mediaInfo = response.Metadata; // TODO Delete me - here to support old sample data
        res.setHeader('Content-Type', response.ContentType);
        res.setHeader('ETag', mimeToETagMap[response.ContentType][resize]);
        res.setHeader('Cache-Control', cacheControlHeader);

        if( resize !== 'none') {
            if(!x) x = mediaInfo.width;
            if(!y) y = mediaInfo.height;
            try {
                buffer = image.convert( {srcData:buffer, width: x, height: y, resizeStyle: resize});
            }
            catch( err) {
                if( err) {
                    log.error( 's3 bad media', {imagemagickErr: err, s3Options: s3Options});
                    return res.status( 404).send();
                }
            }
        }
        res.send( buffer);
    });
};

exports.getUploadUrl = function( req, res) {
    var getUrlDoc = _(req.body).clone();
    var mediaInfo = getUrlDoc.mediaInfo;

    if( getUrlDoc.length >= maxUploadBytes)
        return res.status( 413).send();

    mediaInfo.id = getUrlDoc.MD5.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, '');

    var params = {
        Bucket: config.s3.mediaBucket,
        Key: config.s3.keyByUser ? '1/m/' + req.user + '/i/' + mediaInfo.id : '1/m/' + mediaInfo.id,
        ContentMD5: getUrlDoc.MD5, // Header must also be set for the PUT to s3
        ACL: 'private', // Encoded in the signedUrl and thus set on the s3 object
        ContentType: mediaInfo.type + '/' + mediaInfo.subtype, // Header must also be set for the PUT to s3
        Metadata: {mediainfo: encodeURIComponent( JSON.stringify( req.body.mediaInfo))}, // Encoded in the signedUrl and thus headers set on the s3 object
        Expires: config.s3.uploadUrlExpire_s
    };

    s3.headObject( {Bucket: params.Bucket, Key: params.Key}, domain.active.bind( function( err, headers) {
        if( err && err.statusCode != 404)  {
            log.error( 'S3 headObject error', err);
            return res.status( 502).send();
        }
        else if (err && err.statusCode == 404
            || headers.ContentLength != params.ContentLength
            || headers.ContentType != params.ContentType
            || !_.isEqual( headers.Metadata, params.Metadata)) {
            getUrlDoc.putUrl = s3.getSignedUrl('putObject', params); // Func seems to be needed otherwise we get _onImmediate errors
            getUrlDoc.putHeaders = {
                'Content-Length' : params.ContentLength,
                'Content-MD5': params.ContentMD5,
                'Content-Type': params.ContentType
            };
        }
        else { // No err, so object exists
            getUrlDoc.putUrl = null;
            getUrlDoc.putHeaders = null;
        }
        res.send( getUrlDoc);
    }));
};