// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var model = require('./../../models/desire');
var rsvp = require('./httpResponder');

var responderOptions = {omitKeys: ['userId']};

exports.create = function(req, res) {
    var doc = req.body;
    doc.userId = req.params.userid;
    model.newDesire( doc, req.user.id, rsvp.responder( res, responderOptions));
};

exports.update = function(req, res) {
    if( req.header('if-match') === null || req.header('if-match') === undefined)
        return res.status( 428).send();
    var doc = req.body;
    doc.userId = req.params.userid;
    model.updateDesire( req.params.desireid, req.body, req.header('if-match'), req.user.id, rsvp.responder( res, responderOptions));
};

exports.readAll = function (req, res){
    model.readAll( req.params.userid, req.query, req.user.id, rsvp.responder( res, responderOptions));
};

