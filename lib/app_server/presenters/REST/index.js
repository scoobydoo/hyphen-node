// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var config = require('@oakus/hyphen-libs/config').config();
var express = require( 'express');

var presenterPath = './';
var jsonMiddleware = require( '@oakus/hyphen-libs/json-parser');
jsonMiddleware.setConfig( config.jsonParserConfig);
jsonMiddleware.loadSchemasSync( require('path').join(__dirname, 'jsonSchemas'));
var jsonParser = jsonMiddleware.parser;

var idAuth = require('@oakus/hyphen-libs/idAuth');
idAuth.setPassword( config.authPassword);
var anonUserAuthenticator = idAuth.passwordAuthenticator( config.anonUserAuthSecretOld.concat( config.anonUserAuthSecret));

var ctrl;

allowOrigin = function() {
    return function( req, res, next) {
        if( config.clients.webPresenter.allowOrigin)
            res.setHeader('Access-Control-Allow-Origin', config.clients.webPresenter.allowOrigin);
        next();
    }
};

module.exports = function (app) {

    var router = express.Router();

    app.use( '/1', router);
    
    ctrl = require(presenterPath + 'associates');
    router.get('/merchant/:merchantid/associates',
            idAuth.enticator(),
            jsonParser({ resSchema: 'associates'}),
            ctrl.read);
    router.put('/merchant/:merchantid/associates',
            idAuth.enticator(),
            jsonParser({ reqSchema: 'associates', resSchema: 'associates'}),
            ctrl.update);

    ctrl = require(presenterPath + 'blacklist');
    router.post('/user/:userid/blacklist/party/:partyid/add',
        idAuth.enticator( ':userid'),
        ctrl.add);
    router.post('/user/:userid/blacklist/party/:partyid/remove',
        idAuth.enticator( ':userid'),
        ctrl.remove);

    ctrl = require(presenterPath + 'checkin');
    router.post('/user/:userid/checkin/',
            idAuth.enticator( ':userid'),
            jsonParser({ reqSchema: 'checkin'}),
            ctrl.create);

    ctrl = require(presenterPath + 'claimsSummary');
    router.get('/offer/:offerid/claims',
            idAuth.enticator(),
            jsonParser({ resSchema: 'claims'}),
            ctrl.read);

    ctrl = require(presenterPath + 'feedbackSummary');
    router.get('/offer/:offerid/feedbackSummary',
            idAuth.enticator(),
            jsonParser({ resSchema: 'feedbackSummary'}),
            ctrl.read);

    ctrl = require(presenterPath + 'media');
    router.post('/party/:partyid/media',
            idAuth.enticator(),
            jsonParser({ resSchema: 'mediaInfo#response'}),
            ctrl.create);
    router.get('/media/:mediaid',
            ctrl.read);
    router.post('/party/:partyid/media/getUploadUrl',
        idAuth.enticator(),
        jsonParser({ reqSchema: 'mediaInfo#getUrl#request', resSchema: 'mediaInfo#getUrl#response'}),
        ctrl.getUploadUrl);

    ctrl = require(presenterPath + 'merchant');
    router.post('/merchant/',
            idAuth.enticator(),
            jsonParser({ reqSchema:'merchant#request', resSchema: 'merchant#response'}),
            ctrl.create);
    router.get('/merchant/:merchantid',
            idAuth.enticator(),
            jsonParser({ resSchema: 'merchant#response'}),
            ctrl.read);
    router.put('/merchant/:merchantid',
            idAuth.enticator(),
            jsonParser({ reqSchema:'merchant#request', resSchema: 'merchant#response'}),
            ctrl.update);

    ctrl = require(presenterPath + 'offer');
    router.post('/offer/',
            idAuth.enticator(),
            jsonParser({ reqSchema:'offer#request', resSchema: 'offer#response'}),
            ctrl.create);
    router.get('/offer/:offerid',
        allowOrigin(),
        jsonParser({ resSchema: 'offer#response'}),
        ctrl.read);
    router.get('/offer/',
        allowOrigin(),
        idAuth.enticator(),
        jsonParser({ resSchema: 'offer#list'}),
        ctrl.readNearByOffers);
    router.get('/party/:partyid/offer/',
            allowOrigin(),
            idAuth.enticator(),
            jsonParser({ resSchema: 'offer#list'}),
            ctrl.readPartyOffers);
    router.get('/user/:userid/blacklist/offer/',
            allowOrigin(),
            idAuth.enticator( ':userid'),
            jsonParser({ resSchema: 'offer#list'}),
            ctrl.readBlockedOffers);
    router.put('/offer/:offerid',
            idAuth.enticator(),
            jsonParser({ reqSchema:'offer#request', resSchema: 'offer#response'}),
            ctrl.update);

    ctrl = require(presenterPath + 'desire');
    router.post('/user/:userid/desire/',
        idAuth.enticator(),
        jsonParser({ reqSchema:'desire#request', resSchema: 'desire#response'}),
        ctrl.create);
    router.get('/user/:userid/desire/',
        idAuth.enticator(),
        jsonParser({ resSchema: 'desire#list'}),
        ctrl.readAll);
    router.put('/user/:userid/desire/:desireid',
        idAuth.enticator(),
        jsonParser({ reqSchema:'desire#request', resSchema: 'desire#response'}),
        ctrl.update);

    ctrl = require(presenterPath + 'posting');
    router.post('/party/:partyid/posting/',
            idAuth.enticator(),
            jsonParser({ reqSchema: 'posting#request'}),
            ctrl.create);

    ctrl = require(presenterPath + 'feedback');
    router.post('/offer/:offerid/feedback/',
            idAuth.enticator(),
            jsonParser({ reqSchema:'feedback#request'}),
            ctrl.create);

    ctrl = require(presenterPath + 'response');
    router.post('/user/:userid/response/',
            idAuth.enticator( ':userid'),
            jsonParser({ reqSchema:'response#request'}),
            ctrl.create);

    ctrl = require(presenterPath + 'roleDocs');
    router.get('/user/:userid/roledocs',
            idAuth.enticator( ':userid'),
            jsonParser({ resSchema: 'roleDocs#list'}),
            ctrl.read);

    ctrl = require(presenterPath + 'user');
    router.post('/user/',
            anonUserAuthenticator,
            jsonParser({ reqSchema:'user#request', resSchema:'user#response'}),
            ctrl.create);
    router.get('/user/:userid',
            idAuth.enticator( ':userid'),
            jsonParser({ resSchema:'user#response'}),
            ctrl.read);
    router.put('/user/:userid',
            idAuth.enticator( ':userid'),
            jsonParser({ reqSchema: 'user#request', resSchema: 'user#response'}),
            ctrl.update);
    router.delete('/user/:userid',
            idAuth.enticator( ':userid'),
            ctrl.del);

    ctrl = require(presenterPath + 'voucher');
    router.post('/user/:userid/voucher/redeem',
            idAuth.enticator( ':userid'),
            jsonParser({ reqSchema: 'voucher#redemption'}),
            ctrl.redeem);
    router.post('/user/:userid/voucher/claim',
            idAuth.enticator( ':userid'),
            jsonParser({ reqSchema: 'voucher#claim'}),
            jsonParser({ resSchema: 'voucher#claim#response'}),
            ctrl.claim);

    ctrl = require(presenterPath + 'voucherInfo');
    router.get('/user/:userid/vouchers',
            allowOrigin(),
            idAuth.enticator( ':userid'),
            jsonParser({ resSchema: 'voucherInfo'}),
            ctrl.read);

    ctrl = require(presenterPath + 'identity');
    router.post('/user/:userid/identity/',
        idAuth.enticator(),
        jsonParser({ reqSchema:'identity', resSchema:'identity'}),
        ctrl.create);
    router.post('/user/:userid/identity/scope/:scope/identifier/:identifier/validate',
        idAuth.enticator(),
        jsonParser({ reqSchema: 'identity#validation', resSchema: 'roleDocs#list'}),
        ctrl.validate);
    router.get('/user/:userid/identity/',
        idAuth.enticator(),
        jsonParser({ resSchema:'identity#list'}),
        ctrl.readAll);
    router.delete('/user/:userid/identity/scope/:scope/identifier/:identifier',
        idAuth.enticator(),
        ctrl.del);

    ctrl = require(presenterPath + 'login');
    router.post('/user/:userid/login/',
        allowOrigin(),
        idAuth.enticator( ':userid'),
        jsonParser({ reqSchema:'login#request'}),
        ctrl.create);

    ctrl = require(presenterPath + 'tagCloud');
    router.search('/user/:userid/tags',
        allowOrigin(),
        idAuth.enticator( ':userid'),
        jsonParser({ reqSchema:'tagCloud', resSchema:'tagCloud'}),
        ctrl.search);
    router.post('/user/:userid/tags',
        allowOrigin(),
        idAuth.enticator( ':userid'),
        jsonParser({ reqSchema:'tagCloud', resSchema:'tagCloud'}),
        ctrl.search);

    ctrl = require(presenterPath + 'userCounts');
    router.get('/user/:userid/counts',
        allowOrigin(),
        idAuth.enticator(),
        jsonParser({ resSchema:'userCounts'}),
        ctrl.read);
};
