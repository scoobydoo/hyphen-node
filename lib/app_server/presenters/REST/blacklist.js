// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/blacklist');
var rsvp = require('./httpResponder');

exports.add = function(req, res) {
    var newBlock = {
        userId: req.params.userid,
        partyId: req.params.partyid
    };
    model.newBlock( newBlock, req.user.id, rsvp.responder( res));
};

exports.remove = function(req, res) {
    model.deleteBlock( req.params.userid, req.params.partyid, req.user.id, rsvp.responder( res));
};
