// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var model = require('./../../models/offer');
var rsvp = require('./httpResponder');

exports.create = function(req, res) {
    var doc = req.body;
    model.newOffer( doc, req.user.id, rsvp.responder( res));
};

exports.update = function(req, res) {
    if( req.header('if-match') === null || req.header('if-match') === undefined)
        return res.status( 428).send(); // TODO middleware to do this check?
    model.updateOffer( req.params.offerid, req.body, req.header('if-match'), req.user.id, rsvp.responder( res));
};

exports.read = function(req, res) {
    model.getOffer( req.params.offerid, req.user ? req.user.id : null, rsvp.responder( res));
};

exports.readNearByOffers = function (req, res){
    req.query.longitude = parseFloat( req.query.longitude);
    req.query.latitude = parseFloat( req.query.latitude);
    model.getNearByOffers( req.query, req.user.id, rsvp.responder( res));
};

exports.readPartyOffers = function (req, res) {
    model.getPartyOffers( req.params.partyid, req.query, req.user.id, rsvp.responder( res));
};

exports.readBlockedOffers = function (req, res) {
    model.getUserBlockedOffers( req.params.userid, req.query, req.user.id, rsvp.responder( res));
};


