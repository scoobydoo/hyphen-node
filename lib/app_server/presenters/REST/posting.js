// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/posting');
var rsvp = require('./httpResponder');

exports.create = function(req, res) {
    var newPost = req.body;
    newPost.partyId = req.params.partyid;
    model.newPosting( newPost, req.user.id, rsvp.responder( res));
};