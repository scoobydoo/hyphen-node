// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/checkin');
var rsvp = require('./httpResponder');

exports.create = function(req, res) {
    model.newCheckin( req.params.userid, req.body, req.user.id, rsvp.responder( res));
};