// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/login');
var rsvp = require('./httpResponder');

exports.create = function(req, res) {
    var newLogin = req.body;
    model.newLogin( req.params.userid, newLogin, req.user.id, rsvp.responder( res));
};