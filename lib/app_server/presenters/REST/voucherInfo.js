// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/voucherInfo');
var rsvp = require('./httpResponder');

exports.read = function(req, res){
    model.getVoucherInfo( req.params.userid, req.query, req.user.id, rsvp.responder( res));
};