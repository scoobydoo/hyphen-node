// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/roleDocs');
var rsvp = require('./httpResponder');
var _ = require('underscore');
var idAuth = require('@oakus/hyphen-libs/idAuth');


exports.responder = function (res) {
    return function( err, results) {
        if( !err)
            _(results.docs).each( function( docs) {
                docs.user.$.authSecret = idAuth.authSecret( docs.user.$.id);
            });
        rsvp.responder( res)( err, results)
    }
};

exports.read = function(req, res){
    model.getRoleDocs( req.params.userid, req.user.id, exports.responder( res));
};