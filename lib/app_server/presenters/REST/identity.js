// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var async = require('async');
var moment = require('moment');
var model = require('./../../models/identity');
var roleDocsModel = require('./../../models/roleDocs');
var rsvp = require('./httpResponder');
var _ = require('underscore');
var config = require('@oakus/hyphen-libs/config').config();
var log = require('@oakus/hyphen-libs/glogger');
var roleDocs = require( './roleDocs');

var sms = new require( 'simple-nexmo')();
sms.init( config.nexmo.key, config.nexmo.secret);

var responderOptions = {omitKeys: ['partyId', 'secretCode', 'codeLastSent']};

function canSendMobileCode( idDoc) {
    var now = moment.utc();

    // Rate limit
    if( idDoc.codeLastSent && now.diff(idDoc.codeLastSent) < config.nexmo.rateLimit_s*1000)
        return false;
    idDoc.codeLastSent = now.toISOString();
    idDoc.resendCodeAt = now.add('ms', config.nexmo.rateLimit_s*1000).toISOString();

    // TTL for the activation code
    if( !idDoc.secretCode || now.diff(idDoc.codeLastSent) > config.nexmo.codeTTL_s*1000)
        idDoc.secretCode = _.random(9).toString() + _.random(9) + _.random(9) + _.random(9);
    return true;
}

// Note this function never passes an error to the callback
function sendMobileCode( number, code, method, cb) {
    if( number.indexOf( config.mobileMagicDontSendSms) === -1) {
        var magicNumber = _(_(config.mobileMagicSendSmsPrefixes).values()).find( function (magicNum) {
            return number.indexOf( magicNum) === 0;
        });
        if( magicNumber) number = magicNumber;
        number = number.replace( /^\+/, '');
        var sendFn;
        if( method == 'voice')
            sendFn = _(sms.sendTTSMessage).partial( config.nexmo.senderId, number, 'Hello. Your Hyphen activation code is.' + code.split('').join());
        else
        //    sendFn = _(sms.sendTextMessage).partial( config.nexmo.senderId, number, 'Your Hyphen activation code is ' + code);
            sendFn = _(sms.send2FACode).partial( number, code);
        async.retry( sendFn, function( err) {
            if (err) log.error('nexmo error', err);
            cb(); // intentionally ignore any errors here
        });
    }
    else cb( null);
}

exports.create = function(req, res){
    var cb = rsvp.responder( res, responderOptions);
    var dbIdDoc = req.body;
    dbIdDoc.partyId = req.params.userid;

    if( dbIdDoc.scope == 'mobile')
        canSendMobileCode( dbIdDoc);

    model.newIdentity( dbIdDoc, req.user.id, function (err, result) {
        if( !err) {                // A new identity for this user
            if( dbIdDoc.scope == 'mobile')
                sendMobileCode( dbIdDoc.identifier, dbIdDoc.secretCode, 'sms', function () {cb (null, result); });
            else
                cb( null, result);
        }
        else if (err.code ===4) {   // Already exists
            _getResendUpdate( dbIdDoc.partyId, dbIdDoc.scope, dbIdDoc.identifier, req, res, cb);
        }
        else                        // Some other error
            cb( err);
    });
};

_getResendUpdate = function(userId, scope, identifier, req, res, cb){
    model.getIdentity( userId, scope, identifier, req.user.id, function (err, result) {
        if( err) return cb( err);
        var idDbDoc = result.doc;

        if( idDbDoc.scope == 'mobile')
            if( !canSendMobileCode( idDbDoc))
                return res.status( 429).send();
// TODO what if there is a version mismatch?
        model.updateIdentity( idDbDoc, result.eTag, req.user.id,  function (err, result) {
            if( err) return cb( err);
            if( idDbDoc.scope == 'mobile')
                sendMobileCode( idDbDoc.identifier, idDbDoc.secretCode, req.params.method, function () {cb (null, result); });
            else cb( null, result);
        });
    });
};

exports.readAll = function(req, res) {
    var cb = rsvp.responder( res, responderOptions);
    model.getIdentities( req.params.userid, req.user.id, cb);
};

exports.validate = function(req, res) {
    var cb = roleDocs.responder( res);
    var code = req.body.code;

    model.getIdentity( req.params.userid, req.params.scope, req.params.identifier, req.user.id, function (err, result) {
        if( err) return cb( err);

        var idDbDoc = result.doc;
        if( !idDbDoc) return res.status( 404).send();

        if( code == idDbDoc.secretCode || code == config.identityMagicValidationCode ) {
// TODO What if versions don't match?
            model.validateIdentity( idDbDoc,  result.eTag, req.user.id, function (err) {
                if( err) return cb( err);
                return roleDocsModel.getRoleDocs( idDbDoc.partyId, req.user.id, cb);
            });
        }
        else
            return res.status( 403).send('Incorrect validation code.');
    });
};

exports.del = function(req, res){
    model.deleteIdentity( req.params.userid, req.params.scope, req.params.identifier, req.user.id, rsvp.responder( res, responderOptions));
};
