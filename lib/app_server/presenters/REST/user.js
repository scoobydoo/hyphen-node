// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var _ = require( 'underscore');
var idAuth = require('@oakus/hyphen-libs/idAuth');
var model = require('./../../models/user');
var rsvp = require('./httpResponder');

var responderOptions = {
    metadata: function _userAuthSecret( user) {
        return {authSecret: idAuth.authSecret( user.$.id)};
    }
};

exports.create = function(req, res){
    var userDoc = req.body;
    model.newUser( userDoc, null, rsvp.responder( res, responderOptions));
};

exports.read = function(req, res) {
    model.getUser( req.params.userid, req.user.id, rsvp.responder( res, _({clientVersion: req.header('if-none-match')}).extend( responderOptions)));
};

exports.update = function(req, res) {
    if( req.header('if-match') === null || req.header('if-match') === undefined)
        return res.status( 428).send();
    model.updateUser( req.params.userid, req.body, req.header('if-match'), req.user.id, rsvp.responder( res, responderOptions));
};

exports.del = function(req, res){
    model.deleteUser( req.params.userid, req.user.id, rsvp.responder( res, responderOptions));
};
