// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var _ = require('underscore');
var assert = require('assert');

// TODO And a proper error mapper option
// TODO Add config object for above.  Move to library folder.  Add config method (for paging header name.)
exports.responder = function ( res, options) {
    var clientVersion, metadata, omitKeys = [];
    if( options) {
        clientVersion = options.clientVersion;
        metadata = options.metadata;
        if( options.omitKeys) omitKeys = options.omitKeys;
    }
    return function (err, result) {
        if( err) {
            assert( err.httpResCode, err);
            return res.status( err.httpResCode).send();
        }
        if (result && (result.doc || result.docs)) {
            if( result.eTag !== null && result.eTag !== undefined) {
                res.setHeader('ETag', result.eTag);
            }
            if( result.iterator !== null && result.iterator !== undefined) {
                res.setHeader('X-Hyphen-Paging-Iterator', result.iterator);
            }

            if( clientVersion !== null && clientVersion !== undefined && result.eTag == clientVersion)
                res.status(304).send();
            else {
                if( result.error)   // Typically on an update version conflict the result doc is the new version that should be returned
                    res.status( result.error.httpResCode);
                if ( result.docs) {
                    result.docs = _(result.docs).map( function( doc) {
                        doc = _(doc).omit( omitKeys);
                        if( metadata)
                            _(doc.$).extend( _(metadata).isFunction()? metadata( doc, result):
                                metadata);
                        return doc;
                    });
                    res.send( result.docs);
                }
                else {
                    result.doc = _(result.doc).omit( omitKeys);
                    if( metadata)
                        _(result.doc.$).extend( _(metadata).isFunction()? metadata( result.doc, result): metadata);
                    res.send( result.doc);
                }
            }
        }
        else
            res.send( {}); // 200 with an empty json doc
    }
};