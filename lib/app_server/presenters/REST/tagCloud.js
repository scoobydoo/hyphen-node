// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/tagCloud');
var rsvp = require('./httpResponder');

exports.search = function(req, res) {
    var input = req.body;
    var opts = req.query;
    model.search( req.params.userid, input, opts, req.user.id, rsvp.responder( res));
};