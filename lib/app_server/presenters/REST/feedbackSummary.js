// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/feedbackSummary');
var rsvp = require('./httpResponder');

exports.read = function(req, res){
    model.getFeedbackSummary( req.params.offerid, req.user.id, rsvp.responder( res));
};
