// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/associates');
var rsvp = require('./httpResponder');

exports.read = function(req, res) {
    model.getAssociates( req.params.merchantid, req.user.id, rsvp.responder( res));
};

exports.update = function(req, res) {
    model.updateAssociates( req.params.merchantid, req.body, req.user.id, rsvp.responder( res));
};
