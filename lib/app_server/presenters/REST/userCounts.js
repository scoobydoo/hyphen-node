// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var model = require('./../../models/userCounts');
var rsvp = require('./httpResponder');

exports.read = function(req, res){
    model.getUserCounts( req.params.userid, req.user.id, rsvp.responder( res));
};