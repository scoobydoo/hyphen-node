// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var assert = require('assert');
var model = require('./../../models/merchant');
var genId = require('@oakus/hyphen-libs/genId');
var rsvp = require('./httpResponder');

exports.create = function(req, res){
    model.newMerchant( req.body, req.user.id, rsvp.responder( res));
};

exports.read = function(req, res){
    model.getMerchant( req.params.merchantid, req.user.id, rsvp.responder( res, {clientVersion: req.header('if-none-match')}));
};

exports.update = function(req, res) {
    if( req.header('if-match') === null || req.header('if-match') === undefined)
        return res.status( 428).send();
    model.updateMerchant( req.params.merchantid, req.body, req.header('if-match'), req.user.id, rsvp.responder( res));
};
