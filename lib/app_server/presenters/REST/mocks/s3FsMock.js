// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var _ = require( 'underscore');
var fs = require( 'fs-extra');
var assert = require('assert');
var async = require('async');
var os = require('os');
var log = require('@oakus/hyphen-libs/glogger');
var querystring = require('querystring');

var tmpDir = os.tmpdir() + '/s3Mock/';
// Ignore any errors here (errors can occur when starting a cluster.)
//try { fs.removeSync( tmpDir);}
//catch ( err) {}
try { fs.mkdirSync( tmpDir);}
catch ( err) {}

exports.config = {};

exports.config.update = function ( config) {
};

exports.S3 = function() {
};

exports.S3.prototype.putObject = function( options) {
    var imageFilename = tmpDir + '/' + options.Key.replace( /\//g, '_');
    var mediaInfoFilename = imageFilename + '_meta.json';
    var data = options.Body;
    delete options.Body;
    var metaInfoFinished = false;
    var dataWriteFinished = false;

    var writeStream = fs.createWriteStream( imageFilename);
    writeStream.send = function() {
        if( Buffer.isBuffer( data))
            writeStream.end( data);
        else
            data.pipe( writeStream);
    };
    writeStream.on( 'finish', function( err) {
        if( metaInfoFinished)
            writeStream.emit( err ? 'error' : 'success');
        else
            dataWriteFinished = true;
    });

    async.retry( 5, function( cb) {
        fs.writeFile( mediaInfoFilename, JSON.stringify( options), {flag:'wx'}, cb);
    }, function( err) {
        if( err) return res.status( 504).send();
        if( dataWriteFinished)
            writeStream.emit( err ? 'error' : 'success');
        else
            metaInfoFinished = true;
    });

    return writeStream;
};

exports.S3.prototype.getObject = function( options, cb) {
    var imageFilename = tmpDir + '/' + options.Key.replace( /\//g, '_');

    this.headObject( options, function( err, result) {
        if( err) return cb( err);
        fs.readFile( imageFilename, function (err, data) {
            assert( !err);
            result.Body = data;
            cb( err, result);
        });
    })
};

exports.S3.prototype.headObject = function( options, cb) {
    var imageFilename = tmpDir + '/' + options.Key.replace( /\//g, '_');
    var mediaInfoFilename = imageFilename + '_meta.json';

    fs.readFile( mediaInfoFilename, 'utf8', function (err, str) {
        if( err) {
            err.statusCode = 404;
            return cb( err);
        }
        cb( null, JSON.parse( str));
    });
};

var route = '/jhdbfuiehf7342fh/s3Mock/putObject';
var port = require('@oakus/hyphen-libs/config').config().port+1;
var app = require('express')();

app.put( route, function( req, res) {
    var options = _.clone(req.query);
    options.Metadata = JSON.parse( decodeURIComponent( options.Metadata));
    var imageFilename = tmpDir + '/' + options.Key.replace( /\//g, '_');
    var mediaInfoFilename = imageFilename + '_meta.json';
    var metaInfoFinished = false;
    var dataWriteFinished = false;

    var writeStream = fs.createWriteStream( imageFilename);
    req.pipe( writeStream);
    writeStream.on( 'finish', function( err) {
        if( metaInfoFinished)
            res.status( 200).send();
        else
            dataWriteFinished = true;
    });

    async.retry( 5, function( cb) {
        fs.writeFile( mediaInfoFilename, JSON.stringify( options), {flag:'w'}, cb);
    }, function( err) {
        if( err) return res.status( 504).send();
        if( dataWriteFinished)
            res.status( 200).send();
        else
            metaInfoFinished = true;
    });
});
app.get('/', function(req, res){
    res.send('s3FSMock upload server running');
});

require('@oakus/hyphen-libs/startupMonitor').task( 'creates3FSMock', [], function(cb) {
    require('http').createServer(app).listen( port, function( err) {
        if( err) throw err;
        log.info('s3FSMock upload server listening on port ' + port);
        cb();
    });
});

exports.S3.prototype.getSignedUrl = function( action, params, cb) {
    assert( action === 'putObject');
    params = _(params).clone();
    params.Metadata = encodeURIComponent( JSON.stringify( params.Metadata));
    var url =  'http://' + os.hostname() + ':' + port
        + route + '?' + querystring.stringify( params);

    if( cb) setImmediate( cb, null, url); // async
    return url; // And sync
};