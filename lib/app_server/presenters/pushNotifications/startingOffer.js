// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var config = require('@oakus/hyphen-libs/config').config().pushNotifications;
var redisConfig = require('@oakus/hyphen-libs/config').config().redis;
var startupMonitor = require('@oakus/hyphen-libs/startupMonitor');
var log = require('@oakus/hyphen-libs/glogger');
var _ = require('underscore');
var assert = require('assert');
var async = require('async');
var moment = require('moment');
var offers = require('../../models/offer');
var logins = require('../../models/login');
var DTimer = require('dtimer').DTimer;
var redis = require('redis');
var pub = redis.createClient( redisConfig.port, redisConfig.host, redisConfig.options);
var sub = redis.createClient( redisConfig.port, redisConfig.host, redisConfig.options);
var nodeId = "offerPostNotificationPeriodic";
var interval_ms = config.postedOfferInterval_s * 1000;
var trail_ms = config.postedOfferTrailBy_ms;
var lock = require('redis-lock')(pub);

var gcm = require( 'node-gcm');
var gcmSender = new gcm.Sender(config.gcm.apiServerKey);

function errorLogger( err) {
    assert( !err, "dtimer error: " + err);
}

var dt = new DTimer(nodeId, pub, sub, {maxEvents: 1});

startupMonitor.task( 'joinStartingOfferTimer', '*', function(cb) {
    lock("offerPostNotificationPeriodic_startInterval_lock", function(done) {
        dt.join( function( err) {
            assert( !err, "dtimer.join error");
            dt.upcoming({limit:16}, function( err, events) {
                assert( !err, "dtimer.upcoming error");
                if( _(events).keys().length > 1) {
                    log.error( 'dtimer has >1 pending event. Deleting some');
                    _(_(events).keys()).forEach( function( evId, i) { if(i!=0) dt.cancel( evId, errorLogger)});
                }
                if( interval_ms && _(events).keys().length == 0) { // No evenets pending,
                    var until = moment.utc().subtract( trail_ms, 'ms').toISOString();
                    dt.post({offersUntil: until}, 0, function( err) {
                        errorLogger( err);
                        done();
                        cb();
                    });
                    log.debug( 'Starting Offers Notification starting interval timer', {until: until});
                }
                else {
                    done();
                    cb();
                }
            });
        });
    });
});

dt.on('event', function (ev) {
    var from = ev.offersUntil;
    var until = moment.utc().subtract( trail_ms, 'ms').toISOString();

    if( interval_ms)    // 0 = don't run
        dt.post( {offersUntil: until}, interval_ms, errorLogger);

    log.debug( 'Searching for StartingOffers', {from: from, until: until});

    offers.getSuggestedOffersAndUsers( from, until, function( err, rows) {
        if (err) return log.error('getSuggestedOffersAndUsers error: ', err);
        async.each( rows, function( row, cb) {
            logins.getRecentLogins( row.userIds, function( err, loginResults) {
                if (err) log.error('getRecentLogins error: ', err); // Note no 'return' here - may get some results even on error.
                if ( !loginResults || loginResults.length == 0) return cb( null);

                var message = new gcm.Message({
                    collapseKey: row.offer.$.id + ':' + row.offer._.instanceSite + ':' + row.offer._.instanceDisplayFrom,
                    delayWhileIdle: false,
                    timeToLive: moment( row.offer._.instanceDisplayUntil).diff( moment(), "seconds"),
                    data: {
                        event: 'newOfferStarting',
                        offer: row.offer
                    }
                });
                log.verbose('GCM sending offers (first '
                    + Math.min(10, loginResults.length)
                    + ' of '
                    + loginResults.length
                    + ' users logged)', { users: _(loginResults).first(10), message: message});
                gcmSender.send(message, _(loginResults).pluck('deviceToken'), 6, function (err, result) {
                    if( err) log.error('GCM send error: ', err);
                    if( result) log.verbose('GCM send result: ', result);
                    // TODO need store which devices have uninstalled the app?
                    cb(null, result);
                });
            });
        }, function( err) {
        })
    })
});
