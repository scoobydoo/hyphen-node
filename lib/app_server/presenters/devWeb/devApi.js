// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var config = require('@oakus/hyphen-libs/config').config();
var express = require( 'express');
var child_process = require( 'child_process');

var secret = config.devRestRoutes.authSecret;
var testConfig = config.devRestRoutes.config;
var nodeBinPath = require('path').dirname( process.execPath);

module.exports = function (app) {
    if( config.devRestRoutes.mount) {
        var router = express.Router();
        app.use( '/dev/' + secret, router);
        router.get('/runTests', function( req, res) {
            res.setTimeout( 10*60*1000);
            req.socket.setNoDelay();
            var opts = {
                env: {
                    'PATH': nodeBinPath, 'PARAM1': testConfig, 'NODE_ENV': 'development'
                },
                cwd: process.cwd(),
                stdio: 'pipe'
            };
            var mocha = child_process.spawn( 'node_modules/mocha/bin/mocha', ['--recursive', '--ui=bdd', '--reporter=spec', '--timeout=120000', '--no-colors' ], opts);
            res.type('text');
            mocha.stdout.setTimeout( 10*60*1000);
            mocha.stdout.setNoDelay();
            mocha.stderr.setTimeout( 10*60*1000);
            mocha.stderr.setNoDelay();
            mocha.stdout.pipe( res);
            mocha.stderr.pipe( res);
        })
    }
};