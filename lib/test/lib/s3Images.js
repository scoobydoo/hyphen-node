// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var _ = require('underscore');
var fs = require('fs');
var AWS = require('aws-sdk');
var s3 = new AWS.S3();
var config = require( '@oakus/hyphen-libs/config').config();
image = require( 'imagemagick-native');

var _localFile = fs.readFileSync('lib/test/RESTapi/docs/654x494_24KB.png');
var localFile = {
    body: _localFile,
    mediaInfo: {
        type:'image',
        subtype:'png',
        x: 654,
        y:494
    },
    length: _localFile.length,
    MD5: require('crypto').createHash('md5').update( _localFile).digest("base64")
};

var files = null;
var loading = false;

//loadFileList( function () {});

function loadFileList( cb) {
    if( files)
        return cb( null, files);// NB no nextTick() here;
    if(loading)                 // Already loading, so wait until loaded.
        return setTimeout( function() { loadFileList(cb);}, 500);
    loading = true;

    var params = {
        Bucket: config.sampleImages.s3Bucket,
        Prefix: config.sampleImages.folder + '/',
        Delimiter: '/'
    };

    var error = s3.listObjects(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            return cb( err);
        }
        else
            files = _(_(data.Contents).pluck('Key')).tail();

        cb( err, files);
    });
}

var cache = {};
exports.loadImage = function( useS3, cb) {
    if( !useS3)
        return setImmediate( cb, null, localFile);
    else {
        loadFileList( function ( err, files) {
            var s3Options = {
                Bucket: config.sampleImages.s3Bucket,
                Key: files[_.random( files.length-1)]
            };

            if( cache[ s3Options.Key])
                setImmediate( cb, null, cache[ s3Options.Key]);
            else {
                s3.getObject( s3Options, function (err, response) {
                    if (err) {
                        console.log(err, err.stack);
                        return cb( err);
                    }
                    var result = {
                        MD5: require('crypto').createHash('md5').update(response.Body).digest("base64"),
                        length: response.Body.length,
                        body: response.Body,
                        mediaInfo: {type:'image'}
                    };
                    var imageFileDetails = image.identify( { srcData: new Buffer( result.body, 'binary')});
                    result.mediaInfo.x = imageFileDetails.width;
                    result.mediaInfo.y = imageFileDetails.height;
                    if ( imageFileDetails.format == 'PNG')
                        result.mediaInfo.subtype = 'png';
                    if ( imageFileDetails.format == 'JPEG')
                        result.mediaInfo.subtype = 'jpeg';

                    cache[s3Options.Key] = _(result).omit( 'body');
                    cb( err, _.clone(result));
                });
            }
        })
    }
};