# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
faker = require( 'faker')()
_ = require( 'underscore')

callFaker = ( module, path, arg) ->
  if( path.length > 1)
    return callFaker( module[ path[ 0]], _(path).tail(), arg)
  else
    result = _(module[ path[0]]).bind( module)( arg)
    if( path[0] == 'latitude' || path[0] == 'longitude')
      return parseFloat(result)
    else if( path[0] == 'phoneNumberFormat')
      return '+' + result.replace( /[\.\-]/g, ' ')
    else if _(result).isString()
      return result.raplace( 'error', 'flaw') ## better for logging
    else
      return result

ipsum = (doc) ->
  if( _(doc).isString() && regexArray = /^\$ipsum.*:(.+)\((.*)\)$/.exec( doc))
    return callFaker( faker, regexArray[1].split( '.'), regexArray[2])
  else if _(doc).isObject()
    newDoc = if _(doc).isArray() then [] else {}
    _(doc).each (value, key) -> newDoc[ ipsum( key)] = ipsum( value)
    return newDoc
  else
    return _(doc).clone()

exports.fakeJson = ipsum