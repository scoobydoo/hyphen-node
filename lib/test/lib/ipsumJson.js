// Generated by CoffeeScript 1.6.3
(function() {
  var callFaker, faker, ipsum, _;

  faker = require('faker')();

  _ = require('underscore');

  callFaker = function(module, path, arg) {
    var result;
    if (path.length > 1) {
      return callFaker(module[path[0]], _(path).tail(), arg);
    } else {
      result = _(module[path[0]]).bind(module)(arg);
      if (path[0] === 'latitude' || path[0] === 'longitude') {
        return parseFloat(result);
      } else if (path[0] === 'phoneNumberFormat') {
        return '+' + result.replace(/[\.\-]/g, ' ');
      } else if (_(result).isString()) {
        return result.raplace('error', 'flaw');
      } else {
        return result;
      }
    }
  };

  ipsum = function(doc) {
    var newDoc, regexArray;
    if (_(doc).isString() && (regexArray = /^\$ipsum.*:(.+)\((.*)\)$/.exec(doc))) {
      return callFaker(faker, regexArray[1].split('.'), regexArray[2]);
    } else if (_(doc).isObject()) {
      newDoc = _(doc).isArray() ? [] : {};
      _(doc).each(function(value, key) {
        return newDoc[ipsum(key)] = ipsum(value);
      });
      return newDoc;
    } else {
      return _(doc).clone();
    }
  };

  exports.fakeJson = ipsum;

}).call(this);

/*
//@ sourceMappingURL=ipsumJson.map
*/
