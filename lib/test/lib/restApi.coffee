# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{ consoleProgress, restApiBaseUrl, anonUserAuthSecret, useS3SampleMediaResources, sampleImages} = require( '@oakus/hyphen-libs/config').config()

_ = require( 'underscore')
async = require( 'async')
ipsum = require( './ipsumJson')
fs = require( 'fs')
https = require('https')
http = require('http')
url = require('url')
moment = require('moment')
request = null
images = require('./s3Images')
image = require( 'imagemagick-native')

exports.setRequest = ( obj) ->
  if !request then request = obj

creationSetConfig = {
  parallelLimit: 1,
  offersFromDate: "now",
  offersRangeMins: 1*24*60,
  offerLengthMinMins: 30,
  offerDisplayLengthMinMins: 5*60,
  maxDailyRepeats: 7,
  maxOfferPerMedia: 1,
  useS3SampleMediaResources: useS3SampleMediaResources,
  latitudeMax: 42,
  latitudeMin: 36,
  longitudeMin: -124,
  longitudeMax: -116  # California + Nevada, roughly.
}

# async map on nested arrays.
asyncNMapLimit = (docOrArray, limit, iterator, done) ->
  if( _(docOrArray).isArray())
    async.mapLimit docOrArray, limit, _(exports.asyncNMap).partial( _, limit, iterator), done
  else
    iterator docOrArray, done

exports.newUser = (config = creationSetConfig) ->
  doc = ipsum.fakeJson( require('./../RESTapi/docs/newUser1'))
  doc.name += _.random( 999999)
  return doc
newUser = exports.newUser

exports.newMerchant = (mediaInfo, config = creationSetConfig) ->
  newMerchant = ipsum.fakeJson( require('./../RESTapi/docs/newMerchant'))
  if mediaInfo?id then newMerchant.logo = mediaInfo
  newMerchant.name += ' ' + _.random( 999999)
  return newMerchant
newMerchant = exports.newMerchant

exports.newOffer = (party, mediaInfoSet, config = creationSetConfig) ->
  newOffer = ipsum.fakeJson( _(require('./../RESTapi/docs/newMerchantOffer')).clone())
  newOffer.partyId = party.$.id
  newOffer.partyType = if( party.lang) then 'user' else 'merchant'
  newOffer.partyName = party.name
  newOffer.media = mediaInfoSet;

  minTotalTime = Math.max( config.offerLengthMinMins, config.offerDisplayLengthMinMins)
  unusedEndMins = _.random( config.offersRangeMins - minTotalTime)
  unusedStartMins = _.random( config.offersRangeMins - unusedEndMins - minTotalTime)
  offerLength = _.random( config.offerLengthMinMins, config.offersRangeMins - ( unusedEndMins + unusedStartMins))
  displayLength = _.random( config.offerDisplayLengthMinMins, config.offersRangeMins - ( unusedEndMins + unusedStartMins))

  fromTime = if config.offersFromDate == "now" then moment.utc() else moment(config.offersFromDate).utc()

  if( config.offersFromDate == "now" && unusedStartMins == 0)
    newOffer.displayFrom = null
  else
    newOffer.displayFrom = moment(fromTime).utc()
                            .add( 'minutes', unusedStartMins)
                            .toISOString()
  newOffer.displayUntil = moment(fromTime).utc()
                  .add( 'minutes', config.offersRangeMins - unusedEndMins)
                  .toISOString()
  newOffer.redeemFrom = moment(fromTime).utc()
                    .add( 'minutes', config.offersRangeMins - unusedEndMins - offerLength)
                    .toISOString()
  newOffer.displayUntil = moment(fromTime).utc()
                  .add( 'minutes', unusedStartMins + displayLength)
                  .toISOString()

  repeats = _.random( 0, config.maxDailyRepeats)
  if( repeats)
    newOffer.repeat.every = _.random( 1, 2)
    newOffer.repeat.occurrences = repeats
  else
    delete newOffer.repeat

  if newOffer.partyType == 'user'
    delete newOffer.maxVouchers
    delete newOffer.maxPerUser
    delete newOffer.repeat

  _(newOffer.sites).each (site) ->
    site.location.longitude = _.random( config.longitudeMin*1000000, config.longitudeMax*1000000)/ 1000000.0
    site.location.latitude = _.random( config.latitudeMin*1000000, config.latitudeMax*1000000)/ 1000000.0

  return newOffer
newOffer = exports.newOffer

postUser = (doc, done) ->
  req = request.post( '/1/user/')
  req._agent = undefined
  req.set( 'X-Hyphen-Log-Level', 'warn')
  .set( 'Authorization', anonUserAuthSecret )
  .set('Accept-Encoding', 'gzip, deflate')
  .set( 'Accept', 'application/json')
  .set('Connection', 'keep-alive')
  .send( doc)
  .expect( 200)
  .end (err, res) ->
    done( err, res?.body, res?.headers)

_postMedia = (authSecret, partyId, file, done) ->
  gerUrlReq = _(file).omit( 'body')

  req = request.post( '/1/party/' + partyId + '/media/getUploadUrl')
  req._agent = undefined
  req.set( 'Authorization', authSecret)
  .set( 'X-Hyphen-Log-Level', 'warn')
  .set('Accept-Encoding', 'gzip, deflate')
  .expect( 'Content-Type', /json/)
  .expect( /"id":".+"/)
  .expect( 200)
  .send( gerUrlReq)
  .end (err, res) ->
    if err then return done( err)
    newMediaInfo = res?.body?.mediaInfo
    newMediaHeaders = res?.headers
    if(err || !res.body.putUrl? || res.body.putUrl == null) then return done( err, newMediaInfo, newMediaHeaders)
    uri = url.parse( res.body.putUrl)
    uri.method = 'PUT'
    req = (if uri.protocol == 'http:' then http else https).request uri, (res) ->
      if( res.statusCode != 200) then err = {statusCode: res.statusCode}
      res.on 'end', ->
        done( null, newMediaInfo, newMediaHeaders)
      res.on 'error', (e) ->
        done(e)
      res.on 'data', ->
    _(res.body.putHeaders).each (value, header) ->
      req.setHeader( header, value)
    req.end file.body, 'binary'

postMedia = (authSecret, partyId, num, config, done) ->
  async.timesSeries num, (i, done) ->
    images.loadImage config.useS3SampleMediaResources, ( err, image) ->
      if err then return done( err)
      _postMedia( authSecret, partyId, image, done)
  , done

postMerchant = (authSecret, doc, done) ->
  req = request.post( '/1/merchant/')
  req._agent = undefined
  req.set( 'X-Hyphen-Log-Level', 'warn')
  .set( 'Authorization', authSecret )
  .set('Accept-Encoding', 'gzip, deflate')
  .set( 'Accept', 'application/json')
  .set('Connection', 'keep-alive')
  .expect( 'Content-Type', /json/)
  .send( doc)
  .expect( 200)
  .end (err, res) ->
    done( err, res?.body, res?.headers)

postOffer = (authSecret, doc, done) ->
  req = request.post( '/1/offer/')
  req._agent = undefined
  req.set( 'X-Hyphen-Log-Level', 'warn')
  .set( 'Authorization', authSecret )
  .set('Accept-Encoding', 'gzip, deflate')
  .set( 'Accept', 'application/json')
  .set('Connection', 'keep-alive')
  .expect( 'Content-Type', /json/)
  .send( doc)
  .expect( 200)
  .end (err, res) ->
    done( err, res?.body, res?.headers)

class DocSet
  config: {}
  constructor: (_config = creationSetConfig) ->
    this.config = _(_.clone(_config)).defaults( creationSetConfig)

  postNewMerchant: (users, merchantsDone) =>
    asyncNMapLimit users, this.config.parallelLimit, (user, itDone) =>
      postMedia user.$.authSecret, user.$.id, 1, this.config, (err, mediaInfo) =>
        if consoleProgress then process.stdout.write( 'i')
        if err then return itDone( err)
        postMerchant( user.$.authSecret, newMerchant( mediaInfo, this.config), itDone)
    , merchantsDone

  postNewOffer: (parties, authSecret = offersDone, offersDone) =>
    if authSecret == offersDone then authSecret = null
    asyncNMapLimit parties, this.config.parallelLimit, (party, partyItDone) =>
      auth = if authSecret then authSecret else party.$.authSecret
      postMedia auth, party.$.id, 1, this.config, ( err, mediaInfoSet) =>
        if err then return partyItDone( err)
        if consoleProgress then process.stdout.write( 'i')
        async.mapLimit [0..._.random( 0, this.config.maxOfferPerMedia-1)], this.config.parallelLimit, (offerSetI, offersetItDone) =>
          postOffer( auth, newOffer( party, mediaInfoSet, this.config), offersetItDone)
          if consoleProgress then process.stdout.write( 'o')
        , ( err, repeatedOfferSet) =>
          postOffer( auth, newOffer( party, mediaInfoSet, this.config), partyItDone)
    , offersDone

  createUsersMerchantsOffers: ( numUser, numMerchants, numOffers, done) =>
    this.users = []
    this.usersHeaders = []
    if numMerchants
      this.merchants = ([] for[0...numUser])
      this.merchantsHeaders = ([] for[0...numUser])
    if numOffers
      this.offers = (([]for[0...numMerchants]) for[0...numUser])
      this.offersHeaders = (([]for[0...numMerchants]) for[0...numUser])
    this.errors = []

    async.mapLimit [0...numUser], this.config.parallelLimit, (userI, cb1) =>
      postUser newUser(this.config), (err, user, headers) =>
        if consoleProgress then process.stdout.write( 'u')
        if( err) then this.errors.push( err); return cb1()
        this.users[ userI] = user
        this.usersHeaders[ userI] = headers

        doOffers = ( party, userI, merchantI, cb3) =>
          async.mapLimit [0...numOffers], this.config.parallelLimit, ( offerI, cb4) =>
            this.postNewOffer party, user.$.authSecret, (err, offer, headers) =>
              if consoleProgress then process.stdout.write( 'o')
              if( err) then this.errors.push( err); return cb4()
              if( merchantI != null)
                this.offers[userI][merchantI][offerI] = offer
                this.offersHeaders[userI][merchantI][offerI] = headers
              else
                this.offers[userI][offerI] = offer
                this.offersHeaders[userI][offerI] = headers
              cb4()
          , cb3

        if( numMerchants)
          async.mapLimit [0...numMerchants], this.config.parallelLimit, ( merchantI, cb2) =>
            this.postNewMerchant user, (err, merchant, headers) =>
              if consoleProgress then process.stdout.write( 'm')
              if( err) then this.errors.push( err); return cb2()
              this.merchants[ userI][merchantI] = merchant
              this.merchantsHeaders[ userI][merchantI] = headers
              doOffers( merchant, userI, merchantI, cb2)
          , cb1
        else
          doOffers( user, userI, null, cb1)

    , () =>
      if consoleProgress then process.stdout.write( '\r')
      done( (if this.errors.length then this.errors else null), this) # TODO Construct a propper Error here and throw it
    return this

  createUsersMerchants: _(DocSet::createUsersMerchantsOffers).partial( _, _, 0, _)
  createUsers: _(DocSet::createUsersMerchantsOffers).partial( _, 0, 0, _)
  createUsersOffers: _(DocSet::createUsersMerchantsOffers).partial( _, 0, _, _)

exports.DocSet = DocSet

# TODO add a few more local files, choose one at random.  Or just use test files we already hacve?
