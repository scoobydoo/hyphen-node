// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
// California + Nevada, roughly.
var latitudeMax = 42;
var latitudeMin = 36;
var longitudeMin = -124;
var longitudeMax = -116;

var _ = require( 'underscore');
var config = require( '@oakus/hyphen-libs/config').config();
var rest = require('../lib/restApi');
var request = require('supertest')( config.restApiRemoteUrl);
rest.setRequest( request);
var async = require('async');

var users = [];
exports.go = function( numUsers, numSearches, cb) { // Create the users first time through
    if( users.length == 0) {
        var userDocs = new rest.DocSet({parallelLimit: 4});
        userDocs.createUsers( numUsers, function( err) {
            if( err) return cb( err);
            users = userDocs.users;
            return cb();
        });
    }
    else {
        async.concat( users, function( user, cb) {
            async.timesSeries( numSearches, function( i, cb) {
                var req = request.get( '/1/offer/?user=' + user.$.id +
                        '&longitude=' + _.random(longitudeMin*100, longitudeMax*100)/100 +
                        '&latitude=' + _.random(latitudeMin*100, latitudeMax*100)/100 +
                        '&maxResults=10');
                    req._agent = undefined;
                    req.set( 'X-Hyphen-Log-Level', 'warn')
                    .set( 'Authorization', user.$.authSecret)
                    .set( 'Accept', 'application/json')
                    .set('Accept-Encoding', 'gzip, deflate')
                    .buffer()
                    .end( function( e, r) {
                        cb( e, r && r.body);
                    });
            }, cb)
        }, cb)
    }
};
