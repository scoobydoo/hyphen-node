// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var configMod = require( '@oakus/hyphen-libs/config');
var config = configMod.config();
config = configMod.load( './lib/test/agent/config', config.testAgentConfig? config.testAgentConfig : null).config();
var http = require('http');
var https = require('https');
var _ = require('underscore');
var async = require('async');
var generateData = require( './generateData');
var useOffers = require( './useOffers');

module.exports = function( expressApp) {
    var app = expressApp ? expressApp : require('express')();

// http configuration
    if( !config.globalmaxSockets)
        http.globalAgent.maxSockets = https.globalAgent.maxSockets = config.agentGlobalAgentMaxSockets;

// AWS configuration
    require('aws-sdk').config.update({region: config.s3.region, maxRetries: config.s3.maxRetries, httpOptions: {timeout: config.s3.timeout}});

// Express configuration
    app.set('port', config.agentPort);

    app.get('/generateData', function( req, res) {
        generateData.go( function() {
            res.send( 'Unit complete');
        })
    });

    app.get('/generateLotsOfData', function( req, resToBrowser) {
        var parallel = req.params.parallel || 1;
        var series = req.params.series || 1;
        resToBrowser.writeHead(200, {'Content-Type': 'text/plain'});

        async.timesSeries( series, function (s, cb) {
            async.times( parallel, function(p, cb) {
                http.get(config.agentUri + '/generateData', function(res) {
                    res.on( 'end', function() {
                        resToBrowser.write("Unit complete, response code: " + res.statusCode + '\n');
                        cb();
                    });
                    res.resume();   // ignore the response data
                });
            }, cb);
        }, function() {
            resToBrowser.end(); // Finished
        })
    });



// Start the server if we didn't get passed an expressApp
    if( !expressApp)
        http.createServer(app).listen(app.get('port'), function() {
            console.log('agent-hyphen-node server listening on port', app.get('port'));
        });

    var output = [];
    app.get('/intervalStatus', function( req, res) {
        res.send( output.join( '<br>'));
    });

    function addOutput( str) {
        if(output.length > 512)
            output = _(output).last(512);
        output.push( Date() + ' ' + str);
    }

    function doPostOffers() {
        generateData.go( config.generateOffers.numUsers, config.generateOffers.numMerchants, config.generateOffers.numOffers, function( e, r) {
            if( e)
                addOutput( 'Generate data error' + e);
            else
                addOutput( 'Generated data: ' + (config.generateOffers.numUsers+1) + ' users, ' +
                    (config.generateOffers.numUsers * config.generateOffers.numMerchants) + ' merchants, ' +
                    (1 + config.generateOffers.numUsers * config.generateOffers.numMerchants * config.generateOffers.numOffers) + ' offers');
            if( config.generateOffers.interval == 0) {
                if( config.generateOffers.delay)
                    setTimeout( doPostOffers, config.generateOffers.delay);
                else
                    doPostOffers();
            }
        })
    }
    async.timesSeries( config.generateOffers.numThreads, function( n, cb) {
        if( config.generateOffers.interval != 0)
            setInterval( doPostOffers, config.generateOffers.interval);
        else
            doPostOffers();
        setTimeout( cb, 5000 / config.generateOffers.numThreads); // Spread out the threads
    });


    function doSearchOffers( workerId) {
        useOffers.go( config.searchOffers.numUsers, config.searchOffers.numSearches, function( e, r) {
            addOutput( 'DoSearchOffers worker #' + workerId + ' complete: ' + (config.searchOffers.numUsers) + ' users, ' +
                (config.searchOffers.numUsers * config.searchOffers.numSearches) + ' searches');
            if( config.searchOffers.interval == 0) {
                if( config.searchOffers.delay)
                    setTimeout( function() { doSearchOffers( workerId);}, config.searchOffers.delay);
                else
                    doSearchOffers( workerId);
            }
        })
    }
    async.timesSeries( config.searchOffers.numThreads, function( n, cb) {
        if( config.searchOffers.interval != 0)
            setInterval( function() { doSearchOffers( n);}, config.searchOffers.interval);
        else
            doSearchOffers( n);
        setTimeout( cb, 5000 / config.searchOffers.numThreads); // Spread out the threads
    });

    console.log( config);
};
