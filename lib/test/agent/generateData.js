// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var config = require( '@oakus/hyphen-libs/config').config();
var rest = require('../lib/restApi');
var request = require('supertest')( config.restApiRemoteUrl);
rest.setRequest( request);
var async = require('async');

exports.go = function( numUsers, numMerchants, numOffers, cb) {
    docs1 = new rest.DocSet({ parallelLimit: 3, useS3SampleMediaResources: config.useS3SampleMediaResources});
    docs2 = new rest.DocSet({ parallelLimit: 3, useS3SampleMediaResources: config.useS3SampleMediaResources});

    async.series( [
        function( cb) { docs1.createUsersOffers((numUsers?numUsers:0), 1, cb);},
        function( cb) { docs2.createUsersMerchantsOffers( numUsers, numMerchants, numOffers, cb);}
    ], cb);
};