// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
require('@oakus/hyphen-libs/config').load('./config.json');
require('./app.js')();
