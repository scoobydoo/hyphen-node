// Generated by CoffeeScript 1.6.3
(function() {
  var anonUserAuthSecret, expect, moment, request, rest, _, _ref, _ref1;

  _ref = require('./testEnv'), expect = _ref.expect, _ = _ref._, request = _ref.request, (_ref1 = _ref.config, anonUserAuthSecret = _ref1.anonUserAuthSecret);

  moment = require('moment');

  rest = require('../lib/restApi');

  describe('Claims document', function() {
    var autherisedUser, login1, newClaim, newOffer, newRedemption, newUser;
    login1 = {
      deviceToken: "APA91bF3CuJeDrcuYdQimbujkLzgtdrankOg4YQjqM0RVSZV49HFdmZYIo5d",
      os: "Android",
      osVersion: "4.4.4",
      app: "Hyphen",
      appVersion: "0.5.0"
    };
    newUser = {};
    autherisedUser = {};
    newOffer = {};
    newRedemption = {};
    newClaim = require('./docs/newClaim1');
    newRedemption = require('./docs/newRedemption1');
    before(function(done) {
      return new rest.DocSet().createUsersMerchantsOffers(1, 1, 1, function(err, docs) {
        newOffer = docs.offers[0][0][0];
        autherisedUser = docs.users[0];
        return done();
      });
    });
    before(function(done) {
      return new rest.DocSet().createUsers(1, function(err, docs) {
        newUser = docs.users[0];
        return done();
      });
    });
    describe('Lifecyle', function() {
      it('Post a login from a valid User returns 200', function(done) {
        return request.post('/1/user/' + newUser.$.id + '/login/').set('Authorization', newUser.$.authSecret).set('Accept', 'application/json').send(login1).expect(200, done);
      });
      it('POST to claim vouchers with valid doc', function(done) {
        newClaim.offerId = newOffer.$.id;
        newClaim.instanceDisplayFrom = newOffer.displayFrom;
        newClaim.instanceSite = _(newOffer.sites).keys()[0];
        newRedemption.offerId = newOffer.$.id;
        newRedemption.instanceDisplayFrom = newOffer.displayFrom;
        newRedemption.instanceSite = _(newOffer.sites).keys()[0];
        newRedemption.time = moment.utc();
        return request.post('/1/user/' + newUser.$.id + '/voucher/claim').set('Authorization', newUser.$.authSecret).set('Accept', 'application/json').send(newClaim).expect(200, done);
      });
      it('POST to redeem vouchers with valid doc', function(done) {
        return request.post('/1/user/' + newUser.$.id + '/voucher/redeem').set('Authorization', newUser.$.authSecret).set('Accept', 'application/json').send(newRedemption).expect(200, done);
      });
      it('GET the ascociated Claims doc', function(done) {
        return request.get('/1/offer/' + newOffer.$.id + '/claims').set('Authorization', autherisedUser.$.authSecret).set('Accept', 'application/json').expect('Content-Type', /json/).expect(200).end(function(err, res) {
          expect(res.body[newClaim.instanceSite][newClaim.instanceDisplayFrom].numberOfClaims).to.equal(1);
          expect(res.body[newClaim.instanceSite][newClaim.instanceDisplayFrom].totalClaimed).to.equal(2);
          return done(err);
        });
      });
      it('POST another other by the merchant', function(done) {
        newOffer.displayFrom = moment.utc().toISOString();
        return request.post('/1/offer/').set('Authorization', autherisedUser.$.authSecret).set('Accept', 'application/json').expect('Content-Type', /json/).send(_(newOffer).omit('_', '$')).expect(200, done);
      });
      return it('Wait', function(done) {
        this.timeout(31 * 1000);
        return setTimeout(done, 1000 * 30);
      });
    });
    return describe('Bad requests', function() {
      return it('GET Claims with invalid offerId', function(done) {
        return request.get('/1/offer/' + "badId" + "/claims").set('Authorization', newUser.$.authSecret).set('Accept', 'application/json').expect(401, done);
      });
    });
  });

}).call(this);

/*
//@ sourceMappingURL=Claims.map
*/
