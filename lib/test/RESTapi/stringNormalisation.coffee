# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
async = require('async')

spaceyUser = {
  "name": "  \t\n\rThomas \t  " + _.random(99999) + '  \r\r',
  "vicinities": {
    "\tHo  me\r": {
      "latitude": 41,
      "longitude": -83.80382839999998
    },
    "Ho   me\r\r": {
      "latitude": 42,
      "longitude": -83.80382839999998
    },
    "\tHo   me\r\r": {
      "latitude": 43,
      "longitude": -43.80382839999998
    }
  },
  "lang": ["es-21"]
}

upperCaseEmail = {
"scope": "email",
"identifier": "SNOOPY" + _.random( 1000000) + "@DOg.cOm"
}

describe 'Whitespace & case normalisation', ->
  user = null
  it 'POST a document with extra whitespace in string values and keys, returns doc with single spaces', (done) ->
    request
    .post( '/1/user/')
    .set( 'Authorization', anonUserAuthSecret )
    .set( 'Accept', 'application/json')
    .set('Connection', 'keep-alive')
    .expect( /"name":"Thomas \d+"/)
    .expect( /"Ho me":{/)
    .expect( /"Ho me 2":{/)
    .expect( /"Ho me 3":{/)
    .send( spaceyUser)
    .expect 200, (err, res) ->
      if err then done( err)
      user = res.body
      expect( user.vicinities['Ho me'].latitude).to.equal( 41)
      expect( user.vicinities['Ho me 2'].latitude).to.equal( 42)
      expect( user.vicinities['Ho me 3'].latitude).to.equal( 43)
      done()
  it 'POST an email identity with upercase chars returns all lower case', (done) ->
    before (done) ->
    request
    .post( '/1/user/' + user.$.id + '/identity/')
    .set( 'Authorization', user.$.authSecret )
    .set( 'Accept', 'application/json')
    .expect( 'Content-Type', /json/)
    .expect( /"identifier":"snoopy\d+@dog.com"/)
    .send( upperCaseEmail)
    .expect( 200, done)