# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
async = require('async')

describe 'User document', ->
  describe 'Lifecyle', ->
    newDoc = updatedUserDoc = newDocEtag = updatedUserDocETag = null
    it 'POST some new User docs', (done) ->
      new rest.DocSet().createUsers 2, done
    it 'POST a blank doc responds with a valid JSON doc', (done) ->
      new rest.DocSet().createUsers 1, (err, res) -> newDoc = res.users[0]; done()
    it 'POST some more new User docs', (done) ->
      new rest.DocSet().createUsers 2, done
    it 'GET with same userId responds with identical and correct json document', (done) ->
      request
      .get( '/1/user/' + newDoc.$.id )
      .set( 'Accept', 'application/json; charset=utf-8')
      .set( 'Authorization', newDoc.$.authSecret )
      .expect( 'Content-Type', /json/)
      .expect( newDoc)
      .expect( 200)
      .end (err, res) ->
        if err then return done( err)
        newDocEtag = res.headers['etag']
        done()
    it 'PUT it back unchanged returns docuemnt with updated modification timestamp', (done) ->
      request
      .put( '/1/user/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newDoc.$.authSecret )
      .set( 'If-Match', newDocEtag)
      .expect( 'Content-Type', /json/)
      .expect (res) ->
        updatedUserDoc = res.body
        updatedUserDocETag = res.headers['etag']
        expect( updatedUserDoc.$.modified).to.be.greaterThan( newDoc.$.modified)
        expect( newDocEtag).to.not.equal( updatedUserDocETag)

      .send( _(newDoc).omit( '$', '_'))
      .expect( 200, done)
    it 'GET it again and it is unchanged', (done) ->
      request
      .get( '/1/user/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newDoc.$.authSecret )
      .expect( 'Content-Type', /json/)
      .expect( updatedUserDoc)
      .expect( 'ETag', updatedUserDocETag)
      .expect( 200, done)
    it 'GET it again with if-none-match set returns 304', (done) ->
      request
      .get( '/1/user/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newDoc.$.authSecret )
      .set( 'If-None-Match', updatedUserDocETag)
      .expect( 'ETag', updatedUserDocETag)
      .expect( 304, done)
    it 'PUT the original again with original etag returns 412', (done) ->
      request
      .put( '/1/user/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newDoc.$.authSecret )
      .set( 'If-Match', newDocEtag)
      .expect( updatedUserDoc)
      .send( _(newDoc).omit( '$', '_'))
      .expect( 412, done)
    it 'DELETE the doc', (done) ->
      request
      .del( '/1/user/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newDoc.$.authSecret )
      .expect( 'Content-Type', /json/)
      .expect( 200, done)
    it 'GET it again returns 404', (done) ->
      request
      .get( '/1/user/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newDoc.$.authSecret )
      .expect( 404, done)

  describe 'Bad requests', ->
    dummyDoc = {
      "$": {
        "id": "badOne"
      }
      "name": "bad",
      "vicinities": {},
      "lang": ["es-21"]
    }

    newDoc = newDoc2 = merchant = null
    before (done) ->
      new rest.DocSet().createUsersMerchants 1,1, (err, res) ->
        newDoc = res.users[0]
        newDoc2 = _(newDoc).omit(['_','$'])
        merchant = res.merchants[0][0]
        done( err)

    it 'GET with invalid userid returns 401', (done) ->
      request
      .get( '/1/user/' + dummyDoc.$.id)
      .set( 'Authorization', newDoc.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    it 'PUT with invalid userid and bad doc returns 401', (done) ->
      request
      .put( '/1/user/' + dummyDoc.$.id)
      .set( 'Authorization', newDoc.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 401, done)
    it 'PUT with invalid userid and good doc returns 401', (done) ->
      request
      .put( '/1/user/' + dummyDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newDoc.$.authSecret )
      .send( _(dummyDoc).omit( '$', '_'))
      .expect( 401, done)
    it 'PUT with valid userid and bad doc returns 400', (done) ->
      request
      .put( '/1/user/' + newDoc.$.id)
      .set( 'Authorization', newDoc.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 400, done)
    it 'POST with same name as existing user returns 409', (done) ->
      request
      .post( '/1/user/')
      .set( 'Authorization', anonUserAuthSecret )
      .set( 'Accept', 'application/json')
      .send(newDoc2)
      .expect( 409, done)
    it 'POST with same name as existing merchant returns 409', (done) ->
      newDoc2.name = merchant.name
      request
      .post( '/1/user/')
      .set( 'Authorization', anonUserAuthSecret )
      .set( 'Accept', 'application/json')
      .send(newDoc2)
      .expect( 409, done)