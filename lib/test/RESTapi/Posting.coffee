# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
moment = require('moment');
rest = require('../lib/restApi')

describe 'Posting document', ->
  newPosting = {}
  newMerchant = {}
  newOffer = {}
  newUser = {}
  before (done) -> new rest.DocSet().createUsersMerchantsOffers 1,1,1,(err,docs) -> newOffer = docs.offers[0][0][0]; done()
  before (done) -> new rest.DocSet().createUsersMerchants 1,1, (err,docs) -> newUser = docs.users[0]; newMerchant = docs.merchants[0][0];done()

  describe 'Lifecyle', ->
    it 'POST responds with a valid JSON doc', (done) ->
      newPosting = require('./docs/newPosting')
      newPosting.time = moment.utc();
      newPosting.offerId = newOffer.$.id
      request
      .post( '/1/party/' + newMerchant.$.id + '/posting/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .send( newPosting)
      .expect( 200, done)

  describe 'Bad requests', ->
    it 'POST with a bad partyId and good doc returns 404'
    it 'POST with a bad partyId and bad doc returns 404', (done) ->
      request
      .post( '/1/party/' + 'BadPartyId' + '/posting/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 400, done)
    it 'POST with a valid partyId and bad doc returns 400', (done) ->
      request
      .post( '/1/party/' + newMerchant.$.id + '/posting/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 400, done)