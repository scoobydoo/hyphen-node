# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
moment = require('moment')

oldIterator = '8.200.KjIRy2FqGiUmIaNiLH3v'
lat = _.random( -89, 89)
lon = _.random( -179, 179)
offerParams = {
  latitudeMax: lat + 0.0001,
  latitudeMin: lat - 0.0001,
  longitudeMin: lon - 0.0001,
  longitudeMax: lon + 0.0001,
  offersFromDate: "now",
  offersRangeMins: 5,
  offerLengthMinMins: 5,
  offerDisplayLengthMinMins: 5
}

describe 'Paging of list results', ->
  this.timeout( 5 * 60 * 1000)
  docs = new rest.DocSet(offerParams)
  user = results = null

  before (done) ->
    docs.createUsersMerchantsOffers 2,2,5, done

  describe 'Nearby Offers Paging', ->
    user = null
    iterator = null
    before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> user = docs.users[0]; done()
    it 'Wait time specified in response', (done) ->
      setTimeout( done, moment( 2000))
    it 'GET offers specifying maxResults=5 returns a list of 5 offers and an iterator', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults=5')
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 'X-Hyphen-Paging-Iterator', /.*/)
      .expect( /\[(\{.+\},)+\{.+\}\]/ )
      .expect(200)
      .end (err, res) ->
          if err then return done( err)
          expect( res.body.length).to.equal(5)
          iterator = res.get('x-hyphen-paging-iterator')
          done()
    it 'GET offers specifying maxResults=2 and passing iterator returns a list of 2 offers and an iterator', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults=2&iterator='+iterator)
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 'X-Hyphen-Paging-Iterator', /.*/)
      .expect( /\[(\{.+\},)+\{.+\}\]/ )
      .expect(200)
      .end (err, res) ->
          if err then return done( err)
          expect( res.body.length).to.equal(2)
          results = res.body
          iterator = res.get('x-hyphen-paging-iterator')
          done()
    it 'GET offers specifying maxResults=2 and passing iterator returns a list of 2 different offers and an iterator', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults=2&iterator='+iterator)
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 'X-Hyphen-Paging-Iterator', /.*/)
      .expect( /\[(\{.+\},)+\{.+\}\]/ )
      .expect(200)
      .end (err, res) ->
          if err then return done( err)
          expect( res.body.length).to.equal(2)
          expect( res.body).to.not.deep.equal( results)
          results = res.body
          done()
    it 'GET offers specifying maxResults=2 and passing same iterator returns same list of 2 offers', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults=2&iterator='+iterator)
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 'X-Hyphen-Paging-Iterator', /.*/)
      .expect( /\[(\{.+\},)+\{.+\}\]/ )
      .expect(200)
      .end (err, res) ->
          if err then return done( err)
          expect( res.body.length).to.equal(2)
          expect( res.body).to.deep.equal( results)
          iterator = res.get('x-hyphen-paging-iterator')
          done()
    it 'Repeated call re-passing the iterator will eventually return iterator === "end"', (done) ->
      recurse = ->
        request
        .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults=5&iterator='+iterator)
        .set( 'Authorization', user.$.authSecret )
        .set( 'Accept', 'application/json')
        .expect( 'Content-Type', /json/)
        .expect( 'X-Hyphen-Paging-Iterator', /.*/)
        .expect( /\[(\{.+\},)*\{.+\}\]/ )
        .expect(200)
        .end (err, res) ->
            if err then return done( err)
            expect( res.body.length).to.be.lessThan(11)
            expect( res.body).to.not.deep.equal( results)
            iterator = res.get('x-hyphen-paging-iterator')
            if iterator != 'end'
              recurse()
            else
              done()
      recurse()
    it 'GET again with iterator == "end" returns empty set', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults=5&iterator='+iterator)
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 'X-Hyphen-Paging-Iterator', /end/)
      .expect( [])
      .expect(200)
      .end (err, res) ->
          if err then return done( err)
          done()
    it 'GET nearby offers with malformed iterator returns 410', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults==5&iterator=badIterator')
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect(410, done)
    it 'GET nearby offers with an old iterator returns 410', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=' + lon + '&latitude=' + lat + '&maxResults=5&iterator='+oldIterator)
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .end (err, res) ->
          if err then return done( err)
          done()