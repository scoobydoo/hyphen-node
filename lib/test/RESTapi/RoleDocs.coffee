# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')

describe 'RoleDocs document', ->
  newUser = {}
  describe 'Lifecyle', ->
    newRoles = {}
    newMerchant = {}
    before (done) -> new rest.DocSet().createUsersMerchants 1,1, (err, docs) -> newMerchant = docs.merchants[0][0]; newUser = docs.users[0]; done()

    it 'GET the ascociated RoleDoc for a valid user returns a valid doc', (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/roleDocs')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> newRoles = res.body; 0)
      .expect( /^\[\{.*"user":\{.+\}.*\}\]$/)
      .expect( /^\[\{.*"adminMerchants":\[\{.+\}\].*\}\]$/)
      .expect( /^\[\{.*"postMerchants":\[\].*\}\]$/)
      .expect( 200, done)
    it 'DELETE thw User', (done) ->
      request
      .del( '/1/user/' + newUser.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 200, done)
    it 'GET the ascociated RoleDoc for a deleted User returns 404', (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/roleDocs')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( (res) -> newRoles = res.body; 0)
      .expect( 404, done)

  describe 'Bad requests', ->
    it 'GET Roles with invalid userid returns 401', (done) ->
      request
      .get( '/1/user/' + 'badId' + '/roleDocs')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)

