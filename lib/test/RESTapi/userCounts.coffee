# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
moment = require('moment')
async = require('async')

describe 'UserCounts document', ->
  docs = new rest.DocSet()
  newUser = newOffer = ratingUser = null

  before (done) ->
    docs.createUsersMerchantsOffers 1,1,1, ->
      newOffer = docs.offers[0][0][0]
      newUser = docs.users[0]
      done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> ratingUser = docs.users[0]; done()
  before (done) ->
    async.timesSeries 11, (n, done) ->
      newRating = _(require('./docs/newRating1')).clone()
      newRating.userId = ratingUser.$.id
      newRating.parentId = null
      request
      .post( '/1/offer/' + newOffer.$.id + '/feedback/')
      .set( 'Authorization', ratingUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newRating)
      .expect( 200, done)
    , (e) ->
      done( e)
  describe 'Lifecyle', ->
    it 'GET user counts reports 11 unread comments and likes, and repScore has been credited', (done) ->
      request.get( '/1/user/' + newUser.$.id + '/counts' )
      .set( 'Accept', 'application/json; charset=utf-8')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 'Content-Type', /json/)
      .expect( 200)
      .end (err, res) ->
          if err then return done( err)
          expect( res.body.unreadComments).to.equal( 11);
          expect( res.body.unreadLikes).to.equal( 11);
          expect( res.body.repScore > 100).ok;
          done()
    it 'GET all Offers for the merchant include correct unread counts', (done) ->
      request
      .get( '/1/party/' + newOffer.partyId + '/offer/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect (res) ->
          expect( res.body[0]._.unreadComments).to.equal( 11)
          expect( res.body[0]._.unreadLikes).to.equal( 11)

      .expect( 200, done)

    it 'GET the feedback for a valid Offer returns a valid doc', (done) ->
      request
      .get( '/1/offer/' + newOffer.$.id + '/feedbackSummary')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 200, done)

    it 'GET all Offers for the merchant include unread counts of 0', (done) ->
      request
      .get( '/1/party/' + newOffer.partyId + '/offer/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect (res) ->
          expect( res.body[0]._.unreadComments).to.equal( 0)
          expect( res.body[0]._.unreadLikes).to.equal( 0)

      .expect( 200, done)