# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
moment = require('moment');
async = require('async');
rest = require('../lib/restApi')

describe 'FeedbackSummary document', ->
  offerPostingUser = {}
  ratingUser = {}
  newOffer = {}
  newRating = {}
  feedback = {}
  newRating = {}
  postedComments = []

  before (done) -> new rest.DocSet().createUsersMerchantsOffers 1,1,1,(err,docs) ->
    offerPostingUser = docs.users[0]
    newOffer = docs.offers[0][0][0]
    done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> ratingUser = docs.users[0]; done()
  before (done) ->
    async.timesSeries 30, (n, done) ->
      newRating = _(require('./docs/newRating1')).clone()
      newRating.userId = ratingUser.$.id
      newRating.parentId = if(_.random(1) && n) then postedComments[ _.random( n-1)] else null
      request
      .post( '/1/offer/' + newOffer.$.id + '/feedback/')
      .set( 'Authorization', ratingUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newRating)
      .expect 200, (e, r) ->
          if(e) then done( e)
          postedComments.push( r.body.$.id)
          done( e, r.body)
    , (e) ->
      postedComments.push( null)
      done( e)

  describe 'Lifecyle', ->

    it 'GET the FeedabckSummary for a valid Offer returns a valid doc', (done) ->
      request
      .get( '/1/offer/' + newOffer.$.id + '/feedbackSummary')
      .set( 'Authorization', offerPostingUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect (res) ->
        feedback = res.body
        expect(feedback.usefulComments).is.an('array')
        for id in postedComments
          _(_(feedback.usefulComments).where({ parentId: id})).reduce (previous, comment) ->
            expect( previous.createdAt < comment.createdAt).to.be.true
            return comment
          , { createdAt: "2000-12-31T23:59:59.999Z"}

      .expect( 200, done)

  describe 'Bad requests', ->
    it 'GET Feedback with invalid offerId returns 404', (done) ->
      request
      .get( '/1/offer/' + 'badId' + '/feedabckSummary')
      .set( 'Authorization', ratingUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 404, done)

