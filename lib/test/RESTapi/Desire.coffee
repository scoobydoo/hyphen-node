# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
moment = require('moment')

goodDesireDoc = {
  title: {"es-21": "My Desire"},
  categories: {"en-US":{dining:null}},
  tags: {"es-21":{indian:null}},
  from: moment().utc().startOf('minute'),
  "vicinities": {
    "Home": {
      "latitude": 41.5189413,
      "longitude": -83.80382839999998
    }
  }
}

badDoc = _(goodDesireDoc).omit('tags')

describe 'Desire document', ->
  docs = new rest.DocSet()
  newUser = newDesire = newDesireETag = updatedDesire = updatedDesireETag = null

  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()

  describe 'Lifecyle', ->
    it 'POST a new Desire for a User returns a valid doc', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/desire/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> newDesire = res.body; 0)
      .send( goodDesireDoc)
      .expect( 200, done)
    it 'GET all Desires for the user returns only the one offer', (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/desire/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( [newDesire])
      .expect( 200)
      .end (err, res) ->
        newDesireETag = res.body[0].$.eTag
        done(err)
    it 'PUT it back unchanged returns Desire with updated modification date', (done) ->
      request
      .put( '/1/user/' + newUser.$.id + '/desire/' + newDesire.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', newDesireETag)
      .expect( 'Content-Type', /json/)
      .send( _(newDesire).omit('$','_'))
      .expect( 200)
      .end (err, res) ->
        updatedDesire = res.body
        updatedDesireETag = res.headers['etag']
        expect( updatedDesire.$.modified).to.be.greaterThan( newDesire.$.modified)
        expect( newDesireETag).to.not.equal( updatedDesireETag)
        done( err)
    it 'GET it again and it is unchanged', (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/desire/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( [updatedDesire])
      .expect( 200, done)
    it 'PUT again with old If-Match value returns 412', (done) ->
      request
      .put( '/1/user/' + newUser.$.id + '/desire/' + newDesire.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', newDesireETag)
      .send( _(newDesire).omit('$','_'))
      .expect( 412, done)

  describe 'Bad requests', ->

    it 'POST an invalid new doc', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/desire/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( badDoc)
      .expect( 400, done)
    it 'PUT invalid desireId with bad doc returns 400', (done) ->
      request
      .put( '/1/user/' + newUser.$.id + '/desire/' + "badId")
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', updatedDesireETag)
      .send( badDoc)
      .expect( 400, done)
    it 'PUT valid desireId with bad doc', (done) ->
      request
      .put( '/1/user/' + newUser.$.id + '/desire/' + newDesire.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', updatedDesireETag)
      .send( badDoc)
      .expect( 400, done)
