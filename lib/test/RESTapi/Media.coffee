# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { runNetworkedTests}, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
https = require('https')
http = require('http')

fs = require( 'fs')
image = require( 'imagemagick-native')
url = require('url')

imageParser = (res, callback) ->
  res.setEncoding('binary')
  res.data = ''
  res.on 'data', (chunk) ->
    res.data += chunk;
  res.on 'end', ->
    newImage = image.identify( { srcData: new Buffer(res.data, 'binary')})
    newImage.data = res.data
    callback( null, newImage)

describe 'Media', ->
  newUser = {}
  file = imageFileDetails = gerUrlReq = newMediaId = imageMimeType = eTag = uploadUrl = uploadHeaders = null
  before (done) -> new rest.DocSet().createUsers 2, (err, res) -> newUser = res.users[0]; done()

  describe 'Send image uri in body', ->
    if runNetworkedTests
      it 'POST a uri of media returns 200', (done) ->
        req = request
        .post( '/1/party/' + newUser.$.id + '/media')
        .set( 'Authorization', newUser.$.authSecret )
        .set( 'Accept', 'application/json')
        .expect( 'Content-Type', /json/)
        .expect( /"id":".+"/)
        .expect( 200)
        req.send( {uri: "http://media-cdn.tripadvisor.com/media/photo-s/01/c4/de/85/araz-restaurant.jpg"})
        req.end (err, res) ->
          if(err) then throw err
          done()
    else
      it 'POST a uri of media returns 200'

  for imageFile in ['32x32_1036B.jpg','3264x2448_523KB.jpg', '576x432_64KB.jpeg',
                    '324x594_512KB.png', '360x360_36KB.png', '654x494_24KB.png']
    do (file, imageFile, imageFileDetails, gerUrlReq, newMediaId, imageMimeType, eTag, uploadUrl, uploadHeaders) ->
      before ->
        file = fs.readFileSync('lib/test/RESTapi/docs/' + imageFile, 'binary')
        gerUrlReq = {
          length: file.length,
          MD5: require('crypto').createHash('md5').update(file).digest("base64"),
          mediaInfo: {type:'image'}
        }
        imageFileDetails = image.identify( { srcData: new Buffer( file, 'binary')})
        gerUrlReq.mediaInfo.x = imageFileDetails.width
        gerUrlReq.mediaInfo.y = imageFileDetails.height
        imageFileDetails.data = file
        if ( imageFileDetails.format == 'PNG')
          gerUrlReq.mediaInfo.subtype = 'png'
          imageMimeType = 'image/png'
        if ( imageFileDetails.format == 'JPEG')
          gerUrlReq.mediaInfo.subtype = 'jpeg'
          imageMimeType = 'image/jpeg'
        newMediaId = ""

      describe 'New lifecycle for ' + imageFile, ->
        it 'Get temporary upload url for a file', (done) ->
          req = request
          .post( '/1/party/' + newUser.$.id + '/media/getUploadUrl')
          .set( 'Authorization', newUser.$.authSecret )
          .expect( 'Content-Type', /json/)
          .expect( /"id":".+"/)
          .expect( 200)
          .send( gerUrlReq)
          req.end (err, res) ->
            if(err) then throw err
            uploadUrl = res.body.putUrl
            uploadHeaders = res.body.putHeaders
            newMediaId = res.body.mediaInfo.id
            done()

        it 'Upload to the temp url', (done) ->
          if !uploadUrl?
            return done()
          uri = url.parse( uploadUrl)
          uri.method = 'PUT'
          httpReq = (if uri.protocol == 'http:' then http else https).request uri, (res) ->
            expect( res.statusCode).to.equal( 200)
            res.on 'end', ->
              done()
            res.on 'error', (e) ->
              done(e)
            res.on 'data', ->
          _(uploadHeaders).each (value, header) ->
            httpReq.setHeader( header, value)
          httpReq.setHeader( 'Content-Length', file.length)
          httpReq.end( file, 'binary')

        it 'GET the media matches the original file', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .expect( 'Content-Type', imageMimeType)
          .expect( 'ETag', /.+/ )
          .expect( 200)
          .parse( imageParser)
          .end (err, res) ->
              if( err) then throw( err)
              expect( res.body).to.deep.equal( imageFileDetails)
              eTag = res.headers['etag']
              done( err)

        it 'GET again with If-None-Match header set to previous ETag returns 304', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .set('If-None-Match', eTag)
          .expect( 'ETag', eTag )
          .expect( 304, done)

        it 'GET again with If-None-Match header set to random ETag fetches original file', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .set('If-None-Match', 'random eTag string')
          .expect( 'Content-Type', imageMimeType)
          .expect( 'ETag', eTag )
          .expect( 200)
          .parse( imageParser)
          .end (err, res) ->
              if( err) then throw( err)
              expect( res.body).to.deep.equal( imageFileDetails)
              done( err)

      describe 'Old lifecycle for ' + imageFile, ->
        it 'POST new media for a valid user returns the mediaId', (done) ->
          req = request
          .post( '/1/party/' + newUser.$.id + '/media')
          .set( 'Authorization', newUser.$.authSecret )
          .set('Content-Type', imageMimeType)
          .set('Content-Length', file.length)
          .expect( 'Content-Type', /json/)
          .expect( /"id":".+"/)
          .expect( 200)
          req.write( file, 'binary')
          req.end (err, res) ->
            if(err) then throw err
            newMediaId = res.body.id
            done()

        it 'GET the media matches the original file', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .expect( 'Content-Type', imageMimeType)
          .expect( 'ETag', /.+/ )
          .expect( 200)
          .parse( imageParser)
          .end (err, res) ->
              if( err) then throw( err)
              expect( res.body).to.deep.equal( imageFileDetails)
              eTag = res.headers['etag']
              done( err)

        it 'GET again with If-None-Match header set to previous ETag returns 304', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .set('If-None-Match', eTag)
          .expect( 'ETag', eTag )
          .expect( 304, done)

        it 'GET again with If-None-Match header set to random ETag fetches original file', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .set('If-None-Match', 'random eTag string')
          .expect( 'Content-Type', imageMimeType)
          .expect( 'ETag', eTag )
          .expect( 200)
          .parse( imageParser)
          .end (err, res) ->
              if( err) then throw( err)
              expect( res.body).to.deep.equal( imageFileDetails)
              done( err)

      describe 'Fetch resized images for ' + imageFile, ->
        img1 = img2 = null
        it 'POST new media for a valid user to get a new mediaId', (done) ->
          req = request
          .post( '/1/party/' + newUser.$.id + '/media/getUploadUrl')
          .set( 'Authorization', newUser.$.authSecret )
          .expect( 'Content-Type', /json/)
          .expect( /"id":".+"/)
          .expect( 200)
          .send( gerUrlReq)
          req.end (err, res) ->
            if(err) then throw err
            newMediaId = res.body.mediaInfo.id
            if !res.body.putUrl?
              return done()
            uri = url.parse( res.body.putUrl)
            uri.method = 'PUT'
            req = (if uri.protocol == 'http:' then http else https).request uri, (res) ->
              expect( res.statusCode).to.equal( 200)
              res.on 'end', ->
                done()
              res.on 'error', (e) ->
                done(e)
              res.on 'data', ->
            _(res.body.putHeaders).each (value, header) ->
              req.setHeader( header, value)
            req.end( file, 'binary')

        it 'GET the media at 300*300, cropped, has requested dimensions', (done) ->
          req = request
          .get( '/1/media/' + newMediaId + '?x=300&y=300&resize=aspectfill')
          .expect( 'Content-Type', imageMimeType)
          .expect( 'ETag', /.+/ )
          .expect( 200)
          .parse( imageParser)
          .end (err, res) ->
              if( err) then throw( err)
              img1 = res.body
              img1.data = img1.data.replace( /[\s\S]{4}tEXtdate:(?:create|modify)[\s\S]{26}[\s\S]{4}/g, '')
              eTag = res.headers['etag']
              expect( img1).to.not.deep.equal( imageFileDetails)
              expect( img1.height).to.be.equal( 300)
              expect( img1.width).to.be.equal( 300)
              done()

        it 'GET again with If-None-Match header set to previous ETag returns 304', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .set('If-None-Match', eTag)
          .expect( 'ETag', eTag )
          .expect( 304, done)

        it 'GET the media at 300*300, shrunk, has requested dimensions and is different to the cropped', (done) ->
          req = request
          .get( '/1/media/' + newMediaId + '?x=300&y=300&resize=fill')
          .expect( 'Content-Type', imageMimeType)
          .expect( 'ETag', /.+/ )
          .expect( 200)
          .parse( imageParser)
          .end (err, res) ->
              if( err) then throw( err)
              img2 = res.body
              img2.data = img2.data.replace( /[\s\S]{4}tEXtdate:(?:create|modify)[\s\S]{26}[\s\S]{4}/g, '')
              eTag = res.headers['etag']
              expect( img2).to.not.deep.equal( imageFileDetails)
              if( imageFileDetails.width != imageFileDetails.height)
                expect( img2).to.not.deep.equal( img1)
#              else This doesn't seem to hold for png's any more.
#                expect( img2).to.deep.equal( img1)
              expect( img2.height).to.be.equal( 300)
              expect( img2.width).to.be.equal( 300)
              done()

        it 'GET again with If-None-Match header set to previous ETag returns 304', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .set('If-None-Match', eTag)
          .expect( 'ETag', eTag )
          .expect( 304, done)

        it 'GET the media at 300*300, shrunk, fixed aspect, has requested dimensions and is different again', (done) ->
          req = request
          .get( '/1/media/' + newMediaId + '?x=300&y=300&resize=aspectfit')
          .expect( 'Content-Type', imageMimeType)
          .expect( 'ETag', /.+/ )
          .expect( 200)
          .parse( imageParser)
          .end (err, res) ->
              if( err) then throw( err)
              img3 = res.body
              img3.data = img3.data.replace( /[\s\S]{4}tEXtdate:(?:create|modify)[\s\S]{26}[\s\S]{4}/g, '')
              eTag = res.headers['etag']
              expect( img3).to.not.deep.equal( imageFileDetails)
              if( imageFileDetails.width != imageFileDetails.height)
                expect( img3).to.not.deep.equal( img2)
                expect( img3).to.not.deep.equal( img1)
              else
                expect( img3).to.deep.equal( img2)
#                expect( img3).to.deep.equal( img1) This doesn't seem to hold???
              expect( img3.height / img3.width).to.be.closeTo( imageFileDetails.height / imageFileDetails.width, 0.05)
              if( img3.height > img3.width)
                expect(img3.height).to.be.equal(300)
              else
                expect(img3.width).to.be.equal(300)
              done()

        it 'GET again with If-None-Match header set to previous ETag returns 304', (done) ->
          req = request
          .get( '/1/media/' + newMediaId)
          .set('If-None-Match', eTag)
          .expect( 'ETag', eTag )
          .expect( 304, done)

  describe 'Bad requests', ->
    it 'GET the media with valid partyid and invalid mediaid returns 404 ', (done) ->
      req = request
      .get( '/1/media/' + 'badId')
      .expect( 404,  done)
    it 'GET the media with invalid partyid and invalid mediaid returns 404 ', (done) ->
      req = request
      .get( '/1/media/' + 'badId')
      .expect( 404, done)
    it 'GET the media with invalid partyid and valid mediaid returns 404 ', (done) ->
      req = request
      .get( '/1/media/' + newMediaId)
      .expect( 404, done)

  describe 'Oversize posts', ->
    bigFile = {}

    before ->
      bigFile = fs.readFileSync('lib/test/RESTapi/docs/2048x1484_1,1MB.jpg', 'binary')

    it 'POST a uri to an oversized image with no content length returns 413'

    it 'POST a file with no Content-Length header returns 411'# Have to not use superAgent as it stes the length automatically

    it 'POST an oversized file with a fake low Content-Length header results in a socket hang up', (done) ->
      # TODO This seems to cause a double callback - a 'write ECONNRESET' followed by a 'read ECONNRESET'
      req = request
      .post( '/1/party/' + newUser.$.id + '/media')
      .set( 'Authorization', newUser.$.authSecret )
      .set('Content-Type', 'image/jpeg')
      .set('Content-Length', 33*1000)
      req.write( bigFile, 'binary')
      req.end (err, res) ->
        expect( res?.status == 400 || res?.status == 415 || err?.message == 'write EPIPE' || err?.message == 'socket hang up' || err?.message == 'write ECONNRESET' || err?.message == 'read ECONNRESET').to.be.true;
        done()

    it 'POST a file with a (fake) very high Content-Length header returns 413', (done) ->
      req = request
      .post( '/1/party/' + newUser.$.id + '/media')
      .set( 'Authorization', newUser.$.authSecret )
      .set('Content-Type', 'image/jpeg')
      .set('Content-Length', 20*1000*1000)
      req.write( bigFile, 'binary')
      req.end (err, res) ->
        expect( res?.status == 413 || err?.message == 'write EPIPE' || err?.message == 'socket hang up' || err?.message == 'write ECONNRESET' || err?.message == 'read ECONNRESET').to.be.true;
        done()