# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')

describe 'Tag and categroy searches', ->
  newUser = null
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()

  it 'Get a basic list of hard coded categories', (done) ->
    input = {}
    request
    .search( '/1/user/' + newUser.$.id + '/tags?fetch=categories')
    .set( 'Accept', 'application/json')
    .set( 'Authorization', newUser.$.authSecret )
    .expect( 'Content-Type', /json/)
    .expect( /"categories":\{(.+:.+,)*.+:.+\}/)
    .send( input)
    .expect( 200)
    .end (err, res) ->
      if err then return done( err)
      done()

  it 'Get some tags for a hard coded category', (done) ->
    input = {
      categories: {
        "en-US": {dining : null}
      }
    }
    request
    .search( '/1/user/' + newUser.$.id + '/tags?fetch=tags')
    .set( 'Accept', 'application/json')
    .set( 'Authorization', newUser.$.authSecret )
    .expect( 'Content-Type', /json/)
    .expect( /"tags":\{(.+:.+,)*.+:.+\}/)
    .send( input)
    .expect( 200)
    .end (err, res) ->
      if err then return done( err)
      done()

  it 'Get some more tags for a hard coded category, result includes original category and tags', (done) ->
    input = {
      categories: {
        "en-US": {shopping : null}
      },
      tags: {
        "en-US": {newTag : null}
      }
    }
    request
    .search( '/1/user/' + newUser.$.id + '/tags?fetch=tags')
    .set( 'Accept', 'application/json')
    .set( 'Authorization', newUser.$.authSecret )
    .expect( 'Content-Type', /json/)
    .expect( /"tags":\{(.+:.+,)*.+:.+\}/)
    .expect( /"tags":\{.*newTag.*\}/)
    .expect( /"categories":\{.*shopping.*\}/)
    .send( input)
    .expect( 200)
    .end (err, res) ->
        if err then return done( err)
        done()

  it 'Get some more tags for a new category, result includes original categories and tags', (done) ->
    input = {
      categories: {
        "en-US": {
          dining : null,
          newCategory : null}
      },
      tags: {
        "en-US": {newTag : null}
      }
    }
    request
    .search( '/1/user/' + newUser.$.id + '/tags?fetch=tags')
    .set( 'Accept', 'application/json')
    .set( 'Authorization', newUser.$.authSecret )
    .expect( 'Content-Type', /json/)
    .expect( /"tags":\{(.+:.+,)*.+:.+\}/)
    .expect( /"tags":\{.*newTag.*\}/)
    .expect( /"categories":\{.*dining.*\}/)
    .expect( /"categories":\{.*newCategory.*\}/)
    .send( input)
    .expect( 200)
    .end (err, res) ->
      if err then return done( err)
      done()

  it 'Get categories and tags for a new category, result includes original category and tags', (done) ->
    input = {
      categories: {
        "en-US": {newCategory : null}
      },
      tags: {
        "en-US": {newTag : null}
      }
    }
    request
    .search( '/1/user/' + newUser.$.id + '/tags')
    .set( 'Accept', 'application/json')
    .set( 'Authorization', newUser.$.authSecret )
    .expect( 'Content-Type', /json/)
    .expect( /"tags":\{(.+:.+,)*.+:.+\}/)
    .expect( /"tags":\{.*newTag.*\}/)
    .expect( /"categories":\{.*newCategory.*\}/)
    .send( input)
    .expect( 200)
    .end (err, res) ->
        if err then return done( err)
        done()