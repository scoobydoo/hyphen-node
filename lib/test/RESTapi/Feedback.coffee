# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
moment = require('moment');
async = require('async');
rest = require('../lib/restApi')

describe 'Feedback document', ->
  newUser = {}
  newUser2 = {}
  newOffer = {}
  newRating = {}

  before (done) -> new rest.DocSet().createUsersMerchantsOffers 1,1,1,(err,docs) -> newOffer = docs.offers[0][0][0]; done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser2 = docs.users[0]; done()

  describe 'Lifecyle', ->
    it 'Post a Feedback from a valid User returns 200', (done) ->
      postedComments = []
      async.timesSeries 5, (n, done) ->
        newRating = _(require('./docs/newRating1')).clone()
        newRating.userId = newUser.$.id
        newRating.parentId = if(_.random(1) && n) then postedComments[ _.random( n-1)] else null
        request
        .post( '/1/offer/' + newOffer.$.id + '/feedback/')
        .set( 'Authorization', newUser.$.authSecret )
        .set( 'Accept', 'application/json')
        .send( newRating)
        .expect 200, (e, r) ->
          if(e) then done( e)
          postedComments.push( r.body.$.id)
          done( e, r.body)
      , (e, array) ->
        done( e)

  describe 'Bad requests', ->
    it.skip 'POST a valid feedback with invalid offerId returns 400', (done) ->
      request
      .post( '/1/offer/' + 'badId' + '/feedback/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newRating)
      .expect( 400, done)
    it 'POST a invalid Feedback doc with invalid OfferId returns 400', (done) ->
      request
      .post( '/1/offer/' + 'badId' + '/feedback/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 400, done)
    it 'POST a valid feedback with different userId, with valid offerId returns 401', (done) ->
      newRating.userId = newUser2.$.id
      request
      .post( '/1/offer/' + newOffer.$.id + '/feedback/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newRating)
      .expect( 401, done)
