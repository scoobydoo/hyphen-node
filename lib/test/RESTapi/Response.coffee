# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
moment = require('moment');
rest = require('../lib/restApi')


describe 'Response document', ->
  newUser = {}
  newOffer = {}
  newResponse = {}
  newResponse = require('./docs/response1')

  before (done) -> new rest.DocSet().createUsersMerchantsOffers 1,1,1,(err,docs) -> newOffer = docs.offers[0][0][0]; done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()

  describe 'Lifecyle', ->
    it 'POST responds with a valid JSON doc', (done) ->
      newResponse.offerId = newOffer.$.id
      newResponse.time = moment.utc();
      request
      .post( '/1/user/' + newUser.$.id + '/response/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newResponse)
      .expect( 200, done)

  describe 'Bad requests', ->
    it 'POST with invalid userId returns 401', (done) ->
      request
      .post( '/1/user/' + 'badId' + '/response/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newResponse)
      .expect( 401, done)
    it 'POST with invalid document and valid userid returns 400', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/response/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 400, done)

