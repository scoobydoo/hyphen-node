// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
exports.config = require( '@oakus/hyphen-libs/config').load('lib/test/config').load('./config').config();
exports.expect = require('chai').expect;
if( exports.config.restApiRemoteUrl) {
    exports.request = require('supertest')( exports.config.restApiRemoteUrl);
}
else {
    var app = require('../../app.js');
    exports.request = (require('supertest')( app));
    require( '@oakus/hyphen-libs/config').load('lib/test/config'); // reload the test config to override any app config settings
    before( function(done) {
        require( '@oakus/hyphen-libs/startupMonitor').on('success', function(){ done();});
    });
}
require('../lib/restApi').setRequest( exports.request);
exports.async = require('async');
exports._ = require( 'underscore');
require('http').globalAgent.maxSockets = exports.config.globalAgentMaxSockets; // Sets limits explicitly in async async tests.
