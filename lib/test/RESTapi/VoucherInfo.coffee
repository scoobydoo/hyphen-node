# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
moment = require('moment');
rest = require('../lib/restApi')

describe 'VoucherInfo docuemnt', ->
  newUser = {}
  newOffer = {}
  newOffer2 = {}
  newClaim = {}
  newClaim2 = {}
  newClaim = _(require('./docs/newClaim1')).clone()
  newClaim2 = _(require('./docs/newClaim1')).clone()

  before (done) -> new rest.DocSet().createUsersMerchantsOffers 1,1,2,(err,docs) ->
    newOffer = docs.offers[0][0][0];
    newOffer2 = docs.offers[0][0][1];
    done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()

  before (done) ->
    newClaim.offerId = newOffer.$.id
    newClaim.instanceDisplayFrom = newOffer.displayFrom
    newClaim.instanceSite = _(newOffer.sites).keys()[0]
    request
    .post( '/1/user/' + newUser.$.id + '/voucher/claim')
    .set( 'Authorization', newUser.$.authSecret )
    .set( 'Accept', 'application/json')
    .send( newClaim)
    .expect( 200, done)
  before (done) ->
    newClaim2.offerId = newOffer2.$.id
    newClaim2.instanceDisplayFrom = newOffer2.displayFrom
    newClaim2.instanceSite = _(newOffer2.sites).keys()[0]
    request
    .post( '/1/user/' + newUser.$.id + '/voucher/claim')
    .set( 'Authorization', newUser.$.authSecret )
    .set( 'Accept', 'application/json')
    .send( newClaim2)
    .expect( 200, done)

  describe 'Lifecyle', ->
    it 'GET all vouchers for a valid user', (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/vouchers')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect 200, (err, res) ->
        if err then done( err)
        vInfo = res.body
        expect( vInfo.length).to.equal( 2)
        done()
    it 'GET all vouchers for a valid user for a valid offer', (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/vouchers?offer=' + newOffer.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect 200, (err, res) ->
          if err then done( err)
          vInfo = res.body
          expect( vInfo.length).to.equal( 1)
          expect( vInfo[0].offer.$.id).to.equal( newOffer.$.id)
          done()
  describe 'Bad requests', ->
    it 'GET all vouchers for an invalid user returns 401', (done) ->
      request
      .get( '/1/user/' + 'badUserId' + '/vouchers')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    it 'GET all vouchers for a valid user with invalid offer id returns empty set', (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/vouchers?offer=' + 'basOfferIf')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( [])
      .expect( 200, done)
