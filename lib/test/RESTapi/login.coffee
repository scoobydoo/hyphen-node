# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')

describe 'Login opeartion', ->
  newUser = {}
  login1 = {
    deviceToken: "ADeviceToken",
    os: "Android",
    osVersion: "4.4.4",
    app: "Hyphen",
    appVersion: "0.5.0"
  }
  login2 = {
    deviceToken: "ASecondDeviceToken",
    os: "Android",
    osVersion: "4.4.4",
    app: "Hyphen",
    appVersion:"0.5.0"
  }
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()

  describe 'Lifecyle', ->
    it 'Post a login from a valid User returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/login/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( login1)
      .expect( 200, done)
    it 'Post a second device login from a valid User returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/login/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( login2)
      .expect( 200, done)
  describe 'Bad requests', ->
    it 'POST a valid login with invalid userId returns 401', (done) ->
      request
      .post( '/1/user/' + 'badId' + '/login/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( login1)
      .expect( 401, done)
    it 'POST a invalid login doc with invalid userId returns 401', (done) ->
      request
      .post( '/1/user/' + 'badId' + '/login/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 401, done)
    it 'POST a invalid login with valid userId returns 400', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/login/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 400, done)
