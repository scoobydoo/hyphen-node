# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { runNetworkedTests}, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')

describe 'Parallel async tests', ->
  this.timeout( 20 * 60 * 1000)

  docs0 = new rest.DocSet({ parallelLimit: Infinity, useS3SampleMediaResources: false})
  docs1 = new rest.DocSet({ parallelLimit: Infinity, useS3SampleMediaResources: true})
  docs2 = new rest.DocSet({ parallelLimit: Infinity, useS3SampleMediaResources: false})
  docs3 = new rest.DocSet({ parallelLimit: Infinity, useS3SampleMediaResources: true})

  it '6 users * 6 offers, image in body', (done) ->
    docs0.createUsersOffers 6,6, done

  if( runNetworkedTests)
    it '6 users * 6 offers, image from web', (done) ->
      docs1.createUsersOffers 6,6, done
  else
    it '5 users * 5 offers, image from web'

  it '6 users * 6 merchants * 6 offers, image in body', (done) ->
    docs2.createUsersMerchantsOffers 6,6,6, done

  if( runNetworkedTests)
    it '5 users * 5 merchants * 5 offers, image from web', (done) ->
      docs3.createUsersMerchantsOffers 5,5,5, done
  else
    it '5 users * 5 merchants * 5 offers, image from web'
