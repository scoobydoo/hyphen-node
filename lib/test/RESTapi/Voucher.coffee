# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
moment = require('moment');
rest = require('../lib/restApi')

describe 'Voucher actions', ->
  newUser = {}
  newOffer = {}
  newClaim = {}
  newRedemption = {}
  newClaim = _(require('./docs/newClaim1')).clone()
  newRedemption = _(require('./docs/newRedemption1')).clone()

  before (done) -> new rest.DocSet().createUsersMerchantsOffers 1,1,1,(err,docs) -> newOffer = docs.offers[0][0][0]; done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()

  describe 'Lifecyle', ->
    it 'POST to claim vouchers with valid doc', (done) ->
      newClaim.offerId = newOffer.$.id
      newClaim.instanceDisplayFrom = newOffer.displayFrom
      newClaim.instanceSite = _(newOffer.sites).keys()[0]
      request
      .post( '/1/user/' + newUser.$.id + '/voucher/claim')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( /"count":2/)
      .send( newClaim)
      .expect( 200, done)
    it 'POST to redeem vouchers with valid doc', (done) ->
      newRedemption.offerId = newOffer.$.id
      newRedemption.instanceDisplayFrom = newOffer.displayFrom
      newRedemption.instanceSite = _(newOffer.sites).keys()[0]
      newRedemption.time = moment.utc();
      request
      .post( '/1/user/' + newUser.$.id + '/voucher/redeem')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newRedemption)
      .expect( 200, done)
    it 'POST to claim 100 vouchers returns 409', (done) ->
      newClaim.count = 100
      request
      .post( '/1/user/' + newUser.$.id + '/voucher/claim')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( /"totalUnclaimed":8/)
      .expect( /"count":0/)
      .send( newClaim)
      .expect( 409, done)
  describe 'Bad requests', ->
    it 'POST a claim with invalid userId returns 401', (done) ->
      request
      .post( '/1/user/' + 'badId' + '/voucher/claim')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    it 'POST a claim with invalid document and valid userid returns 400', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/voucher/claim')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {bad:"doc"})
      .expect( 400, done)
    it 'POST a claim with valid document and invalid userId returns 401', (done) ->
      request
      .post( '/1/user/' + 'badUser' + '/voucher/claim')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newClaim)
      .expect( 401, done)

    it 'POST a redemption with invalid userId returns 401', (done) ->
      request
      .post( '/1/user/' + 'badId' + '/voucher/redeem')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    it 'POST a redemption with invalid document and valid userid returns 400', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/voucher/redeem')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {bad:"doc"})
      .expect( 400, done)
    it 'POST a redemptionwith valid document and invalid userId returns 401', (done) ->
      request
      .post( '/1/user/' + 'badUser' + '/voucher/redeem')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newRedemption)
      .expect( 401, done)
