# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, async, request, config: { mobileMagicSendSmsPrefixes}, config: { nexmo}, config: { identityMagicValidationCode}, config: { mobileMagicDontSendSms}, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
moment = require('moment')

realPhoneNumber = -> {
  "scope": "mobile",
  "identifier": mobileMagicSendSmsPrefixes['Thomas']  + _.random( 1000)
}
postIdentity = -> {
  "scope": "mobile",
  "identifier": mobileMagicDontSendSms + _.random( 1000000)
}
postIdentity2 = -> {
  "scope": "email",
  "identifier": "snoopy" + _.random( 1000000) + "@dog.com"
}

describe 'Identity document', ->
  docSet = newUser1 = newUser2 = newUser3 = unverifiedUser = verifiedUser = realNumUser = newMerchant = unverifiedUserIdentity = verifiedUserIdentity = userIdentities = null
  parties = user = newIdentity = []

  before (done) ->
    docSet = new rest.DocSet().createUsers 6, (err, docs) ->
      user = docs.users
      unverifiedUser = user[0]
      verifiedUser = user[1] # verifiedUser must be posted before newUser1 for Existing User test
      newUser1 = user[2]
      newUser2 = user[3]
      newUser3 = user[4]
      realNumUser = user[5]
      parties[0] = newUser2
      done()

  describe 'Lifecyle', ->

    for i in [0..0] # Only support user identities for now
      do (i) ->
        doc = postIdentity()
        it 'POST a new phone number for a '+(if i == 0 then 'user' else 'merchant'), (done) ->
          request
          .post( '/1/user/' + parties[i].$.id + '/identity/')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .expect( 'Content-Type', /json/)
          .expect( (res) -> newIdentity = res.body; 0)
          .send( doc)
          .expect( 200, done)
        if i == 0 then it "GET user doc should report 1 un-validated phone number", (done) ->
          request
          .get( '/1/user/' + parties[i].$.id)
          .set( 'Accept', 'application/json')
          .set( 'Authorization', newUser2.$.authSecret )
          .expect( 'Content-Type', /json/)
          .expect( /"mobileCount":1/)
          .expect( /"validatedMobileCount":0/)
          .expect( 200, done)
        it 'POST it again immeadiately returns 429', (done) ->
          request
          .post( '/1/user/' + parties[i].$.id + '/identity/')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .send( doc)
          .expect( 429, done)
        it 'POST to validate it using incorrect code returns 403', (done) ->
          request
          .post( '/1/user/' + parties[i].$.id + '/identity/scope/' + newIdentity.scope + '/identifier/' + newIdentity.identifier + '/validate')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .send( {code: identityMagicValidationCode+'1234'})
          .expect( 403, done)
        it 'POST to validate it using the magic code', (done) ->
          request
          .post( '/1/user/' + parties[i].$.id + '/identity/scope/' + newIdentity.scope + '/identifier/' + newIdentity.identifier + '/validate')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .expect( 'Content-Type', /json/)
          .expect( (res) -> parties[i] = res.body[0].user; 0)
          .send( {code: identityMagicValidationCode})
          .expect( 200, done)
        if i == 0 then it "GET user doc should report 1 validated phone number", (done) ->
          request
          .get( '/1/user/' + parties[i].$.id)
          .set( 'Accept', 'application/json')
          .set( 'Authorization', newUser2.$.authSecret )
          .expect( 'Content-Type', /json/)
          .expect( parties[i])
          .expect( /"mobileCount":1/)
          .expect( /"validatedMobileCount":1/)
          .expect( 200, done)
        it 'GET it again and it is unchanged', (done) ->
          request
          .get( '/1/user/' + parties[i].$.id + '/identity/')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .expect( 'Content-Type', /json/)
          .expect( (res) -> userIdentities = res.body; 0)
          .expect( 200, done)
        it 'POST to validate it again using incorrect code returns 403', (done) ->
          request
          .post( '/1/user/' + parties[i].$.id + '/identity/scope/' + newIdentity.scope + '/identifier/' + newIdentity.identifier + '/validate')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .send( {code: identityMagicValidationCode+'1234'})
          .expect( 403, done)
        it 'GET it again and it is unchanged', (done) ->
          request
          .get( '/1/user/' + parties[i].$.id + '/identity/')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .expect( 'Content-Type', /json/)
          .expect( userIdentities)
          .expect( 200, done)
        it 'POST to validate it again using the magic code', (done) ->
          request
          .post( '/1/user/' + parties[i].$.id + '/identity/scope/' + newIdentity.scope + '/identifier/' + newIdentity.identifier + '/validate')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .expect( 'Content-Type', /json/)
          .send( {code: identityMagicValidationCode})
          .expect( 200)
          .end (err, res) ->
            expect( res.body[0].user).to.deep.equal( parties[i])
            done()
        it 'DELETE it returns 200', (done) ->
          request
          .del( '/1/user/' + parties[i].$.id + '/identity/scope/' + newIdentity.scope + '/identifier/' + newIdentity.identifier)
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .expect( 200, done)
        it 'GET it again returns empty set', (done) ->
          request
          .get( '/1/user/' + parties[i].$.id + '/identity/')
          .set( 'Authorization', newUser2.$.authSecret )
          .set( 'Accept', 'application/json')
          .expect( 'Content-Type', /json/)
          .expect( [])
          .expect( 200, done)

  describe 'verify an existing user', (done) ->
    identityDoc = postIdentity()

    before (done) ->
      request
      .post( '/1/user/' + verifiedUser.$.id + '/identity/')
      .set( 'Authorization', verifiedUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> verifiedUserIdentity = res.body; 0)
      .send( identityDoc)
      .expect( 200, done)
    before (done) ->
      request
      .post( '/1/user/' + verifiedUser.$.id + '/identity/scope/' + verifiedUserIdentity.scope + '/identifier/' + verifiedUserIdentity.identifier + '/validate')
      .set( 'Authorization', verifiedUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .send( {code: identityMagicValidationCode})
      .expect( (res) -> verifiedUser = res.body[0].user; 0)
      .expect( 200, done)
    it 'POST an existing phone number for a new user', (done) ->
      request
      .post( '/1/user/' + newUser1.$.id + '/identity/')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> newIdentity = res.body; 0)
      .send( identityDoc)
      .expect( 200, done)
    it "GET user doc should report 1 un-validated phone number", (done) ->
      request
      .get( '/1/user/' + newUser1.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newUser1.$.authSecret )
      .expect( 'Content-Type', /json/)
      .expect( /"mobileCount":1/)
      .expect( /"validatedMobileCount":0/)
      .expect( 200, done)
    it 'POST it again immeadiately returns 429', (done) ->
      request
      .post( '/1/user/' + newUser1.$.id + '/identity/')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( identityDoc)
      .expect( 429, done)
    it 'POST to validate it using incorrect code returns 403', (done) ->
      request
      .post( '/1/user/' + newUser1.$.id + '/identity/scope/' + identityDoc.scope + '/identifier/' + identityDoc.identifier + '/validate')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {code: identityMagicValidationCode+'1234'})
      .expect( 403, done)
    it 'POST to validate it again using the magic code returns the pre-existing verified user', (done) ->
      request
      .post( '/1/user/' + newUser1.$.id + '/identity/scope/' + identityDoc.scope + '/identifier/' + identityDoc.identifier + '/validate')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .send( {code: identityMagicValidationCode})
      .expect( 200)
      .end (err, res) ->
          expect( res.body[0].user).to.deep.equal( verifiedUser)
          done()
    it "GET user doc should report 1 validated phone number", (done) ->
      request
      .get( '/1/user/' + newUser1.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newUser1.$.authSecret )
      .expect( 'Content-Type', /json/)
      .expect( /"mobileCount":1/)
      .expect( /"validatedMobileCount":1/)
      .expect( 200, done)
    it 'GET identities returns 200', (done) ->
      request
      .get( '/1/user/' + newUser1.$.id + '/identity/')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> userIdentities = res.body; 0)
      .expect( 200, done)
    it 'POST to validate it again using incorrect code returns 403', (done) ->
      request
      .post( '/1/user/' + newUser1.$.id + '/identity/scope/' + identityDoc.scope + '/identifier/' + identityDoc.identifier + '/validate')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {code: identityMagicValidationCode+'1234'})
      .expect( 403, done)
    it 'GET identies again and it is unchanged', (done) ->
      request
      .get( '/1/user/' + newUser1.$.id + '/identity/')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( userIdentities)
      .expect( 200, done)
    it 'POST to validate it again using the magic code returns the pre-existing verified user', (done) ->
      request
      .post( '/1/user/' + newUser1.$.id + '/identity/scope/' + identityDoc.scope + '/identifier/' + identityDoc.identifier + '/validate')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .send( {code: identityMagicValidationCode})
      .expect( 200)
      .end (err, res) ->
          expect( res.body[0].user).to.deep.equal( verifiedUser)
          done()
    it 'DELETE it returns 200', (done) ->
      request
      .del( '/1/user/' + newUser1.$.id + '/identity/scope/' + identityDoc.scope + '/identifier/' + identityDoc.identifier)
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 200, done)
    it 'GET it again returns empty set', (done) ->
      request
      .get( '/1/user/' + newUser1.$.id + '/identity/')
      .set( 'Authorization', newUser1.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( [])
      .expect( 200, done)


  ###
  before (done) ->
    request
    .post( '/1/user/' + docSet.merchants[0][0].$.id + '/identity/')
    .set( 'Authorization', docSet.users[0].$.authSecret )
    .set( 'Accept', 'application/json')
    .expect( 'Content-Type', /json/)
    .expect( (res) -> newMerchantIdentity = res.body; 0)
    .send( postIdentity2())
    .expect( 200, done)
  ###
  before (done) ->
    request
    .post( '/1/user/' + unverifiedUser.$.id + '/identity/')
    .set( 'Authorization', unverifiedUser.$.authSecret )
    .set( 'Accept', 'application/json')
    .expect( 'Content-Type', /json/)
    .expect( (res) -> unverifiedUserIdentity = res.body; 0)
    .send( postIdentity())
    .expect( 200, done)

  describe 'Unauthorised requests', ->
    it "POST a new phone number for a different User, returns 401", (done) ->
      request
      .post( '/1/user/' + unverifiedUser.$.id + '/identity/')
      .set( 'Authorization', newUser2.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( postIdentity())
      .expect( 401, done)
    it "GET identities for a different User, returns 401", (done) ->
      request
      .get( '/1/user/' + unverifiedUser.$.id + '/identity/')
      .set( 'Authorization', newUser2.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    it "POST validation for a different User, returns 401", (done) ->
      request
      .post( '/1/user/' + unverifiedUser.$.id + '/identity/scope/' + unverifiedUserIdentity.scope + '/identifier/' + unverifiedUserIdentity.identifier + '/validate')
      .set( 'Authorization', newUser2.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {code: identityMagicValidationCode})
      .expect( 401, done)
    it "DELETE identity for a different User, returns 401", (done) ->
      request
      .del( '/1/user/' + unverifiedUser.$.id + '/identity/scope/' + unverifiedUserIdentity.scope + '/identifier/' + unverifiedUserIdentity.identifier)
      .set( 'Authorization', newUser2.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    ###
    it "POST a new phone number for a merchant that the User isn't admin for, returns 401", (done) ->
            request
            .post( '/1/party/' + docSet.merchants[1][0] + '/identity/')
            .set( 'Authorization', docSet.users[0].$.authSecret )
            .set( 'Accept', 'application/json')
            .send( postIdentity())
            .expect( 401, done)
    it "GET identities for a merchant that the User isn't admin for, returns 401", (done) ->
      request
      .get( '/1/user/' + docSet.merchants[0][1].$.id + '/identity/')
      .set( 'Authorization', docSet.users[1].$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    it "PUT identity for a merchant that the User isn't admin for, returns 401", (done) ->
      request
      .post( '/1/party/' + docSet.merchants[0][0].$.id + '/identity/scope/' + newMerchantIdentity.scope + '/identifier/' + newMerchantIdentity.identifier + '/validate')
      .set( 'Authorization', docSet.users[1].$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {code: identityMagicValidationCode})
      .expect( 401, done)

    it "DELETE identity for a merchant that the User isn't admin for, returns 401", (done) ->
      request
      .del( '/1/party/' + newMerchantIdentity.$.id + '/identity/scope/' + newMerchantIdentity.scope + '/identifier/' + newMerchantIdentity.identifier)
      .set( 'Authorization', docSet.users[1].$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
###

  describe 'Bad requests', ->
    dummyDoc = {
      "id": "badId",
      "bad": "value"
    }
    newUserIdentity = postIdentity()
    it 'POST an invalid new doc returns 400', (done) ->
      request
      .post( '/1/user/' + newUser3.$.id + '/identity/')
      .set( 'Authorization', newUser3.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .send( dummyDoc)
      .expect( 400, done)
    it 'GET with invalid userid returns 401', (done) ->
      request
      .get( '/1/user/' + 'badPartyId' + '/identity/')
      .set( 'Authorization', newUser3.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
    it 'POST a validation with bad id but good doc returns 401', (done) ->
      request
      .post( '/1/user/' + 'badId' + '/identity/scope/' + newUserIdentity.scope + '/identifier/' + newUserIdentity.identifier + '/validate')
      .set( 'Authorization', newUser3.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {code: identityMagicValidationCode})
      .expect( 401, done)
    it 'POST a validation with good id but bad doc returns 400', (done) ->
      request
      .post( '/1/user/' + newUser3.$.id + '/identity/scope/' + newUserIdentity.scope + '/identifier/' + newUserIdentity.identifier + '/validate')
      .set( 'Authorization', newUser3.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {rubish: identityMagicValidationCode})
      .expect( 400, done)
    it "DELETE with invalid id, returns 401", (done) ->
      request
      .del( '/1/user/' + 'badId' + '/identity/scope/' + newUserIdentity.scope + '/identifier/' + newUserIdentity.identifier)
      .set( 'Authorization', newUser3.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)

  describe.skip 'Real sms and voice call', ->

    doc = realPhoneNumber()
    it 'POST a new phone number for a user', (done) ->
      request
      .post( '/1/user/' + realNumUser.$.id + '/identity/')
      .set( 'Authorization', realNumUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> doc = res.body; 0)
      .send( doc)
      .expect( 200, done)
    it 'Immeadiately POST again returns 429 due to the rate limit', (done) ->
      request
      .post( '/1/user/' + realNumUser.$.id + '/identity/')
      .set( 'Authorization', realNumUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( doc)
      .expect( 429, done)
    it 'Wait time specified in response', (done) ->
      setTimeout( done, moment( doc.resendCodeAt).diff( moment()))
    it 'POST again to resend with, returns 200 (different code sent by sms)', (done) ->
      request
      .post( '/1/user/' + realNumUser.$.id + '/identity/')
      .set( 'Authorization', realNumUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> doc = res.body; 0)
      .send( doc)
      .expect( 200, done)
    it 'Wait time specified in response', (done) ->
      setTimeout( done, moment( doc.resendCodeAt).diff( moment()))
    it 'POST resend with method-voice, returns 200 (different code sent by voice call)', (done) ->
      request
      .post( '/1/user/' + realNumUser.$.id + '/identity/?method=voice')
      .set( 'Authorization', realNumUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> doc = res.body; 0)
      .send( doc)
      .expect( 200, done)
    it 'DELETE it returns 200', (done) ->
      request
      .del( '/1/user/' + realNumUser.$.id + '/identity/scope/' + doc.scope + '/identifier/' + doc.identifier)
      .set( 'Authorization', realNumUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 200, done)

