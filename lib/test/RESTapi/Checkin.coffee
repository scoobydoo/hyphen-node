# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
moment = require( 'moment')

describe.skip 'Checkin operation', ->
  newUser = {}
  newCheckin = require('./docs/newCheckin')

  before (done) -> rest.user rest.newUser(), (err, result) -> newUser = result; done()

  describe 'Lifecyle', ->
    newCheckin.time = moment.utc()
    it 'POST a checkin for valid User returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/checkin/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newCheckin)
      .expect( 200, done)

  describe 'Bad requests', ->
    it 'POST a valid checkin for invalid User returns 404', (done) ->
      request
      .get( '/1/user/' + 'badId' + '/checkin/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newCheckin)
      .expect( 404, done)
    it 'POST a invalid checkin for invalid User returns 404', (done) ->
      request
      .get( '/1/user/' + 'badId' + '/checkin/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 404, done)
    it 'POST a invalid checkin for valid User returns 400', (done) ->
      request
      .get( '/1/user/' + 'badId' + '/checkin/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 404, done)

