# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, async, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')

describe 'Merchant document', ->
  newUser = newDoc = updatedUserDoc = newDocEtag = updatedDocEtag = null

  describe 'Lifecyle', ->
    it 'POST some new Merchant docs', (done) ->
      new rest.DocSet().createUsersMerchants 2,2, done
    it 'POST responds with a valid JSON doc', (done) ->
      new rest.DocSet().createUsersMerchants 1,1, (err, docs) -> newDoc = docs.merchants[0][0]; newUser = docs.users[0]; done()
    it 'POST some new Merchant docs', (done) ->
      new rest.DocSet().createUsersMerchants 2,2, done
    it 'GET respond with the same json document', (done) ->
      request
      .get( '/1/merchant/' + newDoc.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( newDoc)
      .expect( 200)
      .end (err, res) ->
          if err then return done( err)
          newDocEtag = res.headers['etag']
          done()
    it 'PUT it back unchanged', (done) ->
      request
      .put( '/1/merchant/' + newDoc.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', newDocEtag)
      .expect( 'Content-Type', /json/)
      .send( _(newDoc).omit( '$', '_'))
      .expect (res) ->
        updatedUserDoc = res.body
        updatedDocEtag = res.headers['etag']
        expect( updatedUserDoc.$.modified).to.be.greaterThan( newDoc.$.modified)
        expect( newDocEtag).to.not.equal( updatedDocEtag)

      .expect( 200, done)
    it 'GET it again and it is unchanged', (done) ->
      request
      .get( '/1/merchant/' + newDoc.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( updatedUserDoc)
      .expect( 'ETag', updatedDocEtag)
      .expect( 200, done)
    it 'GET it again with if-none-match set returns 304', (done) ->
      request
      .get( '/1/merchant/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'If-None-Match', updatedDocEtag)
      .expect( 'ETag', updatedDocEtag)
      .expect( 304, done)
    it 'PUT the original again with original etag returns 412', (done) ->
      request
      .put( '/1/merchant/' + newDoc.$.id)
      .set( 'Accept', 'application/json')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'If-Match', newDocEtag)
      .send( _(newDoc).omit( '$', '_'))
      .expect( updatedUserDoc)
      .expect( 412, done)

  describe 'Bad requests', ->
    dummyDoc = {
      "$": {"id": "badId"},
      "bad": "value"
    }

    user = merchant = merchant2 = null
    before (done) -> new rest.DocSet().createUsersMerchants 1,1, (err, res) ->
      user = res.users[0]
      merchant = res.merchants[0][0]
      merchant2 = _(newDoc).omit(['$','id','_'])
      done()

    it 'POST an invalid new doc', (done) ->
      request
      .post( '/1/merchant/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( dummyDoc)
      .expect( 400, done)
    it 'GET with invalid id', (done) ->
      request
      .get( '/1/merchant/' + dummyDoc.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 404, done)
    it 'PUT invalid id with bad doc', (done) ->
      request
      .put( '/1/merchant/' + dummyDoc.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', newDocEtag)
      .send( _(dummyDoc).omit( '$', '_'))
      .expect( 400, done)
    it 'PUT valid id with bad doc', (done) ->
      request
      .put( '/1/merchant/' + newDoc.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( _(dummyDoc).omit( '$', '_'))
      .expect( 400, done)
    it 'PUT invalid id with good doc (but same bad id as route)', (done) ->
      newDoc.$.id = dummyDoc.$.id = 'aaaaaaaaaaaaaaaaaaaa'
      request
      .put( '/1/merchant/' + dummyDoc.$.id)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', newDocEtag)
      .send( _(newDoc).omit( '$', '_'))
      .expect( 401, done)
    it 'POST with same name as existing merchant returns 409', (done) ->
      request
      .post( '/1/merchant/')
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .send(merchant2)
      .expect( 409, done)
    it 'POST with same name as existing user returns 409', (done) ->
      merchant2.name = user.name
      request
      .post( '/1/merchant/')
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .send(merchant2)
      .expect( 409, done)