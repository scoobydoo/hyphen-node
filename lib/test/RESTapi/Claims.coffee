# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
moment = require('moment');
rest = require('../lib/restApi')

describe 'Claims document', ->
  login1 = {
    deviceToken: "APA91bF3CuJeDrcuYdQimbujkLzgtdrankOg4YQjqM0RVSZV49HFdmZYIo5d",
    os: "Android",
    osVersion: "4.4.4",
    app: "Hyphen",
    appVersion: "0.5.0"
  }
  newUser = {}
  autherisedUser = {}
  newOffer = {}
  newRedemption = {}
  newClaim = require('./docs/newClaim1')
  newRedemption = require('./docs/newRedemption1')
  before (done) -> new rest.DocSet().createUsersMerchantsOffers 1,1,1,(err,docs) ->
    newOffer = docs.offers[0][0][0]
    autherisedUser = docs.users[0]
    done()
  before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> newUser = docs.users[0]; done()

  describe 'Lifecyle', ->
    it 'Post a login from a valid User returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/login/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( login1)
      .expect( 200, done)

    it 'POST to claim vouchers with valid doc', (done) ->
      newClaim.offerId = newOffer.$.id
      newClaim.instanceDisplayFrom = newOffer.displayFrom
      newClaim.instanceSite = _(newOffer.sites).keys()[0]
      newRedemption.offerId = newOffer.$.id
      newRedemption.instanceDisplayFrom = newOffer.displayFrom
      newRedemption.instanceSite = _(newOffer.sites).keys()[0]
      newRedemption.time = moment.utc();
      request
      .post( '/1/user/' + newUser.$.id + '/voucher/claim')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newClaim)
      .expect( 200, done)
    it 'POST to redeem vouchers with valid doc', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/voucher/redeem')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( newRedemption)
      .expect( 200, done)
    it 'GET the ascociated Claims doc', (done) ->
      request
      .get( '/1/offer/' + newOffer.$.id + '/claims')
      .set( 'Authorization', autherisedUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( 200)
      .end (err, res) ->
        expect( res.body[newClaim.instanceSite][newClaim.instanceDisplayFrom].numberOfClaims).to.equal(1)
        expect( res.body[newClaim.instanceSite][newClaim.instanceDisplayFrom].totalClaimed).to.equal(2)
        done(err)
    it 'POST another other by the merchant', (done) ->
      newOffer.displayFrom = moment.utc().toISOString();
      request
      .post( '/1/offer/')
      .set( 'Authorization', autherisedUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .send( _(newOffer).omit( '_', '$'))
      .expect( 200, done)
    it 'Wait', (done) ->
      this.timeout( 31 * 1000)
      setTimeout( done, 1000 * 30)

  describe 'Bad requests', ->
    it 'GET Claims with invalid offerId', (done) ->
      request
      .get( '/1/offer/' + "badId" + "/claims")
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 401, done)
