# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
moment = require('moment')

describe 'Offer document', ->
  docs = new rest.DocSet()
  newOffer = newOfferETag = updatedOffer = updatedOfferETag = null


  describe 'Lifecyle', ->
    it 'POST a new Offer returns a valid doc', (done) ->
      docs.createUsersMerchantsOffers 1,1,1, ->
        newOffer = docs.offers[0][0][0]
        newOfferETag = docs.offersHeaders[0][0][0]['etag']
        done()
    it 'POST some new Offer docs for another Merchant', (done) ->
      new rest.DocSet().createUsersMerchantsOffers 2,2,2, done
    it 'GET all Offers for the merchant returns only the one offer', (done) ->
      request
      .get( '/1/party/' + newOffer.partyId + '/offer/')
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect (res) ->
        reply = _(res.body[0]).omit('_')
        reply.$.eTag = newOffer.$.eTag #eTag can change as the unread comment and like counts can change
        expect( reply).to.deep.equal( newOffer)

      .expect( 200, done)
    it 'PUT it back unchanged returns Offer with updated modification date', (done) ->
      request
      .put( '/1/offer/' + newOffer.$.id)
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', newOfferETag)
      .expect( 'Content-Type', /json/)
      .expect (res) ->
        updatedOffer = res.body
        updatedOfferETag = res.headers['etag']
        expect( updatedOffer.$.modified).to.be.greaterThan( newOffer.$.modified)
        expect( newOfferETag).to.not.equal( updatedOfferETag)

      .send( _(newOffer).omit( '$', '_'))
      .expect( 200, done)
    it 'GET it again and it is unchanged', (done) ->
      request
      .get( '/1/party/' + newOffer.partyId + '/offer/')
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect (res) ->
          reply = _(res.body[0]).omit('_')
          reply.$.eTag = updatedOffer.$.eTag #eTag can change as the comment and like counts can change
          expect( reply).to.deep.equal( updatedOffer)

      .expect( 200, done)
    it 'PUT it back unchanged with If-Match set returns Offer with updated modification date', (done) ->
      request
      .put( '/1/offer/' + newOffer.$.id)
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', updatedOfferETag)
      .expect( 'Content-Type', /json/)
      .expect (res) ->
          expect( updatedOffer.$.modified).to.be.greaterThan( newOffer.$.modified)
          expect( updatedOfferETag).to.not.equal( res.headers['etag'])
          updatedOffer = res.body

      .send( _(updatedOffer).omit( '$', '_'))
      .expect( 200, done)
    it 'PUT again with old If-Match value returns 412', (done) ->
      request
      .put( '/1/offer/' + newOffer.$.id)
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', updatedOfferETag)
      .send( _(newOffer).omit( '$', '_'))
      .expect( updatedOffer)
      .expect( 412, done)

  describe 'Bad requests', ->
    badDoc = { bad:"atribute"}

    it 'POST an invalid new doc', (done) ->
      request
      .post( '/1/offer/')
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .send( badDoc)
      .expect( 400, done)
    it 'PUT invalid offerId with bad doc returns 400', (done) ->
      request
      .put( '/1/offer/' + "badId")
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', updatedOfferETag)
      .send( _(badDoc).omit( '$', '_'))
      .expect( 400, done)
    it 'PUT valid offerId with bad doc', (done) ->
      request
      .put( '/1/offer/' + newOffer.$.id)
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', updatedOfferETag)
      .send( _(badDoc).omit( '$', '_'))
      .expect( 400, done)
    it 'PUT invalid offerId with good doc (but with same bad id as in route)', (done) ->
      id = newOffer.$.id = 'aaaaaaaaaaaaaaaaaaaa'
      request
      .put( '/1/offer/' + id)
      .set( 'Authorization', docs.users[0].$.authSecret )
      .set( 'Accept', 'application/json')
      .set( 'If-Match', updatedOfferETag)
      .send( _(newOffer).omit( '$', '_'))
      .expect( 401, done)

  describe 'Nearby Offers', ->
    user = {}
    nearbyOffersdocSet = new rest.DocSet( {
      latitudeMax: 42.002,
      latitudeMin: 42,
      longitudeMin: -123.002,
      longitudeMax: -123,
      offersFromDate: "now",
      offersRangeMins: 10,
      offerLengthMinMins: 5,
      offerDisplayLengthMinMins: 10
    })
    before (done) -> nearbyOffersdocSet.createUsersMerchantsOffers 2,1,5, done
    before (done) -> new rest.DocSet().createUsers 1, (err,docs) -> user = docs.users[0]; done()
    it 'GET offers for a user near a specified location returns a list of offers', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=-123.001&latitude=42.001')
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( /\[(\{.+\},)+\{.+\}\]/ )
      .end (err, res) ->
        done( err)
    it.skip 'GET offers for a user near berkeley location returns a list of offers', (done) ->
      request
      .get( '/1/offer/?user=' + user.$.id + '&longitude=-122.259&latitude=37.87')
      .set( 'Authorization', user.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( /\[(\{.+\},)+\{.+\}\]/ )
      .expect( 200, done)

