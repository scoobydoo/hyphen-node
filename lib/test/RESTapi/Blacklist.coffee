# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')
moment = require('moment')

lat = _.random( -89, 89)
lon = _.random( -179, 179)
offerParams = {
  latitudeMax: lat + 0.0001,
  latitudeMin: lat - 0.0001,
  longitudeMin: lon-0.0001,
  longitudeMax: lon + 0.0001,
  offersFromDate: "now",
  offersRangeMins: 5,
  offerLengthMinMins: 5,
  offerDisplayLengthMinMins: 5
}

describe 'Blacklist document', ->
  newUser = {}

  blockedMerchantDocs = new rest.DocSet(offerParams)
  blockedUserDocs = new rest.DocSet(offerParams)
  before (done) -> new rest.DocSet().createUsers 1, (err, res) -> newUser = res.users[0]; done()
  before (done) -> new rest.DocSet(offerParams).createUsersMerchantsOffers 2,1,2, done
  before (done) -> blockedMerchantDocs.createUsersMerchantsOffers 1,1,3, done
  before (done) -> blockedUserDocs.createUsersOffers 1,3, done

  describe 'Lifecyle', ->
    it 'POST to add a new block to a blacklist returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/add')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 200, done)
    it 'POST same block again returns 409', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/add')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 409, done)
    it 'POST to remove the block from the blacklist returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/remove')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 200, done)
    it 'POST to remove the block again from the blacklist returns 404', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/remove')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 404, done)

  describe "Blocking another user's posts", ->
    it 'POST to add a new block to a blacklist returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedUserDocs.users[0].$.id + '/add')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 200, done)
    it "Get blacklist offers for the user returns an offer from the blocked user", (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/blacklist/offer/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( _(blockedUserDocs.offers[0]).max (o) -> o.$.created)
      .expect( 200, done)
    it "Get list of offers near location of a blocked offer doesn't include the blocked offer", (done) ->
      request
      .get( '/1/offer/?user=' + newUser.$.id + '&longitude='+lon+'&latitude='+lat)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( /\[(\{.+\},)*\{.+\}\]/ )
      .expect( 200)
      .end (err, res) ->
        if(err) then return done( err)
        expect( _(res.body).all (o1) -> _(blockedUserDocs.offers[0]).all (o2) -> o1.$.id != o2.$.id).to.be.true
        done()
    it 'POST to remove the block from the blacklist returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedUserDocs.users[0].$.id + '/remove')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 200, done)
    it "Get list of offers now includes the blocked offer", (done) ->
      request
      .get( '/1/offer/?user=' + newUser.$.id + '&longitude='+lon+'&latitude='+lat)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( /\[(\{.+\},)*\{.+\}\]/ )
      .expect( 200)
      .end (err, res) ->
          if(err) then return done( err)
          expect( _(blockedUserDocs.offers[0]).all (o1) -> _(res.body).any (o2) -> o1.$.id == o2.$.id).to.be.true
          done()

  describe "Blocking a merchant's posts", ->
    it 'POST to add a new block to a blacklist returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/add')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 200, done)
    it "Get blacklist offers for the user returns an offer from the blocked user", (done) ->
      request
      .get( '/1/user/' + newUser.$.id + '/blacklist/offer/')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( _(blockedMerchantDocs.offers[0][0]).max (o) -> o.$.created)
      .expect( 200, done)
    it "Get list of offers near location of a blocked offer doesn't include the blocked offer", (done) ->
      request
      .get( '/1/offer/?user=' + newUser.$.id + '&longitude='+lon+'&latitude='+lat)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( /\[(\{.+\},)*\{.+\}\]/ )
      .expect( 200)
      .end (err, res) ->
          if(err) then return done( err)
          expect( _(res.body).all (o1) -> _(blockedMerchantDocs.offers[0][0]).all (o2) -> o1.$.id != o2.$.id).to.be.true
          done()
    it 'POST to remove the block from the blacklist returns 200', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/remove')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 200, done)
    it "Get list of offers now includes the blocked offer", (done) ->
      request
      .get( '/1/offer/?user=' + newUser.$.id + '&longitude='+lon+'&latitude='+lat)
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( /\[(\{.+\},)*\{.+\}\]/ )
      .expect( 200)
      .end (err, res) ->
          if(err) then return done( err)
          expect( _(blockedMerchantDocs.offers[0][0]).all (o1) -> _(res.body).any (o2) -> o1.$.id == o2.$.id).to.be.true
          done()

  describe 'Bad requests', ->
    newDoc = {}

    it 'POST a block with invalid userid returns 401', (done) ->
      request
      .post( '/1/user/' + 'badUserId' + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/add')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 401, done)

    it 'POST a block with valid userid returns and invalid merchantid returns 404', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + 'badMerchantId' + '/add')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 404, done)

    it 'POST a remove with invalid userid returns 401', (done) ->
      request
      .post( '/1/user/' + 'badUserId' + '/blacklist/party/' + blockedMerchantDocs.merchants[0][0].$.id + '/remove')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 401, done)

    it 'POST a remove with valid userid returns and invalid merchantid returns 404', (done) ->
      request
      .post( '/1/user/' + newUser.$.id + '/blacklist/party/' + 'badMerchantId2' +'/remove')
      .set( 'Authorization', newUser.$.authSecret )
      .expect( 404, done)
