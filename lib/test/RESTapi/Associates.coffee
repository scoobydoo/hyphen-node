# Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
{expect, _, request, config: { anonUserAuthSecret}} = require('./testEnv')
rest = require('../lib/restApi')

describe.skip 'Associates document', ->
  newUser = {}
  newMerchant = {}
  before (done) -> rest.user rest.newUser(), (err, result) -> newUser = result; done()
  before (done) -> rest.merchant newUser.$.authSecret, rest.newMerchant(), (err, result) -> newMerchant = result; done()

  describe 'Lifecyle', ->
    newAssociates = {}
    updatedAssociates = {}

    it 'GET the Merchant ascociated Associates doc', (done) ->
      request
      .get( '/1/1/merchant/' + newMerchant.$.id + '/associates')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> newAssociates = res.body; 0)
      .expect( 200, done)
    it 'PUT it back unchanged', (done) ->
      request
      .put( '/1/1/merchant/' + newMerchant.$.id + '/associates')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( (res) -> updatedAssociates = res.body; 0)
      .send( newAssociates)
      .expect( 200, done)
    it 'GET it again and it is unchanged', (done) ->
      request
      .get( '/1/1/merchant/' + newMerchant.$.id + '/associates')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 'Content-Type', /json/)
      .expect( updatedAssociates)
      .expect( 200, done)

  describe 'Bad requests', ->
    newDoc = {}

    it 'GET Associates with invalid userid', (done) ->
      request
      .get( '/1/1/merchant/' + 'badId' + 'associates')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .expect( 404, done)
    it 'PUT invalid userid with good doc', (done) ->
      request
      .put( '/1/1/merchant/' + 'badId' + 'associates')
      .set( 'Authorization', newUser.$.authSecret )
      .set( 'Accept', 'application/json')
      .send( {})
      .expect( 404, done)
