// Copyright (C) Oakus Inc - All Rights Reserved. Contact oakus@oakusinc.com
var config = require('@oakus/hyphen-libs/config').load('./config.json').config();

if( config.nodetime && !config.cluster)
    require('nodetime').profile( config.nodetime);

var _ = require('underscore');
var http = require('http');
var https = require('http');
var startupMonitor = require('@oakus/hyphen-libs/startupMonitor');
var clusterD = require( '@oakus/hyphen-libs/clusterD');

// Set up logging
var winston = require('winston');
require('winston-loggly'); // TODO we should put thr module name in the config
winston.remove(winston.transports.Console);
winston.setLevels( winston.config.npm.levels);
_(config.winstonTransports).each( function( opts, name) {
    winston.add( winston.transports[ name], _(_(opts).clone()).defaults( _(winston).pick('levels')));
});
var log = require('@oakus/hyphen-libs/glogger').set( winston.log);

// http configuration
http.globalAgent.maxSockets = https.globalAgent.maxSockets = config.globalAgentMaxSockets;

// Start the server (cluster)
clusterD.start( startServer, config);

function startServer( cb) {
// Express configuration
    var express = require('express');
    var app = express();
    module.exports = app;
    app.disable('etag');
    app.disable('x-powered-by');
    app.set('port', config.port);

// Express Middleware
    app.use( clusterD.middleware()); // Used with error middleware, see below.
    app.use( require( '@oakus/hyphen-libs/winston-fiddler').middleware({logger: winston.default, header:"X-Hyphen-Log-Level"}));
    app.use( require( 'compression')());

    // HTTP logging middleware according to config
    if( config.expressWinston) {
        var expressWinston = require( 'express-winston');
        expressWinston.responseWhitelist.push('_headers');
        app.use( expressWinston.logger( _({ winstonInstance: winston}).extend(config.expressWinston)));
        if( config.expressWinston.httpBodyLogging) {
            expressWinston.responseWhitelist.push('body');
            expressWinston.requestWhitelist.push('body');
            expressWinston.bodyBlacklist.push( 'authSecret');
        }
    }

// Error handler middleware
    app.use( clusterD.errorMiddleware({debug: 'development' == config.node_env})); // Should be last error handler

// Express REST routes
    require('./app_server/presenters/REST')(app);
// Express clients routes
    var models = require('./app_server/models');
    require('@oakus/hyphen-clients')(app, models);
// Express dev clients routes
    require('./app_server/presenters/devWeb')(app);


// Start the http server
    startupMonitor.task( 'createServer', '*', function(cb) {
        http.createServer(app).listen(app.get('port'), function(){
            log.info('Hyphen-node server listening on port', app.get('port'));
            cb();
        });
    });

// Start data loader agent
   if( config.testAgentConfig)
       startupMonitor.on('success', function () {
           require('./test/agent/app')( app);
       });

// Start up push notification
    clusterD.run( function() {
        require('./app_server/presenters/pushNotifications');
    });

    startupMonitor.on('success', cb);
}

