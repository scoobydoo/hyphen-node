hyphen-node - a node.js powered location based marketing platform
=================================================================
Copyright © Oakus Inc - All Rights Reserved. If you want to make use of anything here, please contact oakus@oakusinc.com


Contents
--------

1. Local Development
2. Deployment
3. Technical Stack Overview
4. Documentation


1 Local Development
-------------------

### 0. Prerequisites

The stack is verified with following versions but there is no known issues with higher versions.

| tool/package  | Version | Install suggestions             |
|---------------|--------:|---------------------------------|
| git           |   2.3.8 | http://git-scm.com/downloads    |
| node.js       |    12.2 | https://nodejs.org/en/download/ |
| npm           |   2.7.4 | Comes with node installation    |
| imagemagick   |   6.8.9 | `brew install imagemagick` `yum install ImageMagick-c++ ImageMagick-c++-devel` |
| postgres libs |   6.8.9 | `brew install postgres` `yum install postgresql93-devel` |

### 1. Local services

For local development the config is set to use local installations of PostgreSQL and redis.  Check that the settings in `/config.json` at `overrides.localDev.postrgesql.superUser` and `overrides.localDev.redis` align with your installation.  Or change the configuration to point to a remote service.
   
| tool/package | Version | Install suggestions                          |
|--------------|--------:|----------------------------------------------|
| redis        |  2.8.17 | `brew install redis`                         |
| postgres     |  2.3.10 | OS X: http://postgresapp.com or use Homebrew |   
| postGIS      |   2.1.8 | OS X: http://postgresapp.com or use Homebrew |   
   
### 2. Environment variables

    export AWS_ACCESS_KEY_ID=<YOU_KEY_HERE>
    export AWS_SECRET_ACCESS_KEY=<YOU_SECRET_KEY_HERE>
    export PARAM1=localDev
    export NODE_ENV=development
    
`PARAM1` selects the configuration from the `config.json` file.  You need to explicitly set `NODE_ENV` (to ensure a proper setting in production environments.)
    
### 3. Install dependant module, run the tests, start the server

`cd` to the repo root and use the standard npm commands.

    npm install
    npm test
    npm start

### 5. Load some test data

The agent will continuously load random sample data into the system.  By default it generates offers for anytime in the coming week, so you may need to leave it running a few minutes until you get hits for the current time.

    node lib/test/agent/index.js &


2 Deployment
------------

Just `git push` to the master repository and a build and test cycle will be triggered on shippable.com.  On success the new version is deployed to the 'ebDev' Elastic Beanstalk environment.  Promotion to the Staging and Production environments is covered in 'Deploying' document.


3 Technical Stack Overview
--------------------------

The code in this repo implements this technology stack:

- SQL: PostgreSQL with PostGIS
- NoSQL: Redis with Lua, DynamoDB
- Middle tier: Node.js and Express.js implementing a stateless RESTful JSON document api.
- Prototype iOS client: React Native app.  Javascript code shared with the web client.  Swift.
- Prototype Web client: HTML5 Single Page Application in Javascript using React and Reflux.  Server side rendering (Isomorphic Javascript) for super fast page loading.
- Cloud hosting: AWS (Relational Database Service (RDS), Elastic Beanstalk (EB), Elastic Load Balancer (ELB), s3, dynamoDB, redislabs, CloudFront, Route53).  Auto-scaled for performance, redundancy, high-availability and cost optimisation.
- Continuous integration and deployment: Git, Bitbucket, Shippable.com (travis)
- Performance optimisation: pgadmin3, nodetime. Chrome Developer Tools
- Test tools:  Mocha, Chai, Istanbul
- Other modules/ tools/ services: Imagemagick, JSON Schema, browserify, npm, Github, Bitbucket, Grunt, Loggly, Nexmo

Not included in this repository:

- Android client: native app in Java
- SQL Db definition file


4 Documentation
---------------

- [Conceptual Model](https://docs.google.com/document/d/1BVcqLBMxpc6OzvNdj2_kwxBIUTSGOKNZptHkVjo73U0/edit?usp=sharing)
- [Rest API](https://docs.google.com/document/d/144CjlnGw1Fmr4G-fGBP8ZOWgvk_zjP4dGIYOHOUY3ik/edit?usp=sharing)
- JSON document schemas are in the directory `lib/app_server/presenters/REST/jsonSchemas`.  Common definitions are in `definitions.json`, otherwise there is one file per document type.


Copyright © Oakus Inc - All Rights Reserved. If you want to make use of anything here, please contact oakus@oakusinc.com
